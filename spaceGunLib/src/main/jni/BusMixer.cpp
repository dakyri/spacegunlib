/*
 * BusMixer.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: dak
 */


#include "BusMixer.h"

#include <android/log.h>

BusMixer::BusMixer(int bufsize)
{
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "create bufsize %d", bufsize);
	currentBufSize = 0;
	buffer = NULL;
	setBufsize(bufsize);
}

BusMixer::~BusMixer()
{
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "delete bus mixer");
	for (int i=0; i<bus.size(); i++) {
		Bus *b = bus[i];
		if (b != NULL) {
			delete b;
		}
	}
	delete [] buffer;
}

Bus *
BusMixer::addBus(Bus *b)
{
	if (b == NULL) return NULL;
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "adding bus %d/%d", b->id, bus.size());
	b->setBufsize(currentBufSize);
	short id = b->id;
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "add bus %d/%d", id, bus.size());
	if (id < bus.size()) {
		bus[id] = b;
		busBuf[id] = b->getBuffer();
	} else {
		while (id > bus.size()) {
			bus.push_back(NULL);
			busBuf.push_back(NULL);
		}
		bus.push_back(b);
		busBuf.push_back(b->getBuffer());
	}
	if (id > 0 && bus[id-1] != NULL) {
		b->setSideChainSrc(busBuf[id-1]);
		bus[id-1]->setSideChainDst(busBuf[id]);
	}
	if (id < bus.size()-1 && bus[id+1] != NULL) {
		b->setSideChainSrc(busBuf[id+1]);
		bus[id+1]->setSideChainDst(busBuf[id]);
	}
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "bus is added ok %d/%d", id, bus.size());
	return b;
}

Bus *
BusMixer::getBus(int i)
{
	return bus[i];
}

int
BusMixer::busCount()
{
	return bus.size();
}

void
BusMixer::setBufsize(int bufsize)
{
	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "set bufsize %d", bufsize);
	currentBufSize = bufsize;
	if (buffer != NULL) {
		delete [] buffer;
	}
	buffer = new float[bufsize];
	for (int i=0; i<bus.size(); i++) {
		Bus *b = bus[i];
		if (b != NULL) {
			b->setBufsize(currentBufSize);
			busBuf[i] = b->getBuffer();
		}
	}
}


void
BusMixer::setTempo(float t)
{
//	TimeSource::setTempo(t);
//	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "set tempo %g", t);
	for (int i=0; i<bus.size(); i++) {
		Bus *b = bus[i];
		if (b != NULL) {
			b->setTempo(t);
		}
	}
}

void
BusMixer::mix(int nActiveBus, short *outBuffer, int nFrame, short nOutChannels)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "mix 1 %d %d %d", nFrame,  busBuf.size(), nActiveBus);
	for (int i=0; i<nActiveBus; i++) {
		Bus *b = bus[i];
		if (b != NULL) {
			b->applyFX(busBuf[i], nFrame);
		}
	}
	int ns = nFrame*nOutChannels;
	memcpy(buffer, busBuf[0], ns*sizeof(float));

	for (int i=1; i<nActiveBus; i++) {
		float *fb = busBuf[i];
		if (fb != NULL) {
			for (int j=0; j<ns;j++) {
				buffer[j] += fb[j];
			}
		}
	}

	for(int i=0; i < ns; i++) {
		float b=buffer[i];
		outBuffer[i] = (short)(SHRT_MAX*(b<-1.0f?-1.0f:(b>1.0f?1.0f:b)));
	}
}

void
BusMixer::zeroBuffers(int nFrame)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "BusMixer", "zeroing %d %d", busBuf.size(), bus.size());
	for (int i=0; i<busBuf.size(); i++) {
		float*fb = busBuf[i];
		if (fb != NULL) {
			memset(fb, 0, currentBufSize*sizeof(float));
		}
	}
	memset(buffer, 0, currentBufSize*sizeof(float));

}



