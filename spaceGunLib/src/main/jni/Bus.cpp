/*
 * Bus.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#include "Bus.h"
#include "com_mayaswell_audio_Control.h"
#include "com_mayaswell_audio_FX.h"

Bus::Bus(short _id, int maxLfo, int maxLfoTgt, int maxFX, int maxFXparam)
	: FXChain(_id, maxFX, maxFXparam, *this, false)
{
	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "Bus(%d.%d)", id, _id);
	buffer = NULL;
	for (int i=0; i<maxLfo; i++) {
		lfo.push_back(LFO(maxLfoTgt, *this));
	}
	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "Bus(%d.%d) done", id, _id);
}

Bus::~Bus()
{
	if (buffer != NULL) delete [] buffer;
}

float *
Bus::getBuffer()
{
	return buffer;
}

void
Bus::setTempo(float t)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "set tempo %g", t);
	TimeSource::setTempo(t);
	for (int i=0; i<nActiveLFO; i++) {
		lfo[i].resetRate();
	}
}

void
Bus::setBufsize(int bufsize)
{
	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "set bufsize %d", bufsize );
	if (buffer != NULL) {
		delete [] buffer;
	}
	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "set bufsize done delete");
	buffer = new float[bufsize];
	bufferSize = bufsize;

	for (int i=0; i<fx.size(); i++) {
		__android_log_print(ANDROID_LOG_DEBUG, "Bus", "set bufsize doing fx %d", i);
		fx[i].setBufsize(bufsize);
	}
}

bool
Bus::setLFOTarget(int which, int whichn, int cc, float amt)
{
	__android_log_print(ANDROID_LOG_DEBUG, "Bus", "set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= lfo[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING) {
			return false;
		}
	}
	int was = lfo[which].target[whichn].target;

	lfo[which].target[whichn].target = cc;
	lfo[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	/*
	if (was == com_mayaswell_spacegun_Controllable_CControl_FLT_CUTOFF) {
		if (hasLfo(com_mayaswell_spacegun_Controllable_CControl_FLT_CUTOFF)<0 && hasEnv(com_mayaswell_spacegun_Controllable_CControl_FLT_CUTOFF)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_Controllable_CControl_FLT_ENVMOD) {
		// nb env mod can affect cutofff
		if (hasLfo(com_mayaswell_spacegun_Controllable_CControl_FLT_ENVMOD)<0 && hasEnv(com_mayaswell_spacegun_Controllable_CControl_FLT_ENVMOD)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_Controllable_CControl_FLT_RESONANCE) {
		if (hasLfo(com_mayaswell_spacegun_Controllable_CControl_FLT_RESONANCE)<0 && hasEnv(com_mayaswell_spacegun_Controllable_CControl_FLT_RESONANCE)<0) {
			lFilter.setResonance(filterResonance);
			rFilter = lFilter;
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
		if (pw == 0) {
			if (hasLfo(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}*/
	return true;
}

unsigned int
Bus::mxn(float *dta, int offset, int nFrame, float gain)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadBus", "mxn() in %x %d %d %g", dta, offset, nFrame, gain);
	int nb = nFrame * 2;
	if (nb > bufferSize-offset) nb = bufferSize-offset;
	float *b = buffer+offset;
	for (int i=0; i<nb; i++, b++, dta++) {
		*b += *dta * gain;
	}
	return nFrame;
}
