/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_mayaswell_spacegun_CControl */

#ifndef _Included_com_mayaswell_spacegun_CControl
#define _Included_com_mayaswell_spacegun_CControl
#ifdef __cplusplus
extern "C" {
#endif
#undef com_mayaswell_spacegun_CControl_GAIN
#define com_mayaswell_spacegun_CControl_GAIN 1L
#undef com_mayaswell_spacegun_CControl_PAN
#define com_mayaswell_spacegun_CControl_PAN 2L
#undef com_mayaswell_spacegun_CControl_LOOP_START
#define com_mayaswell_spacegun_CControl_LOOP_START 3L
#undef com_mayaswell_spacegun_CControl_LOOP_LENGTH
#define com_mayaswell_spacegun_CControl_LOOP_LENGTH 4L
#undef com_mayaswell_spacegun_CControl_FLT_CUTOFF
#define com_mayaswell_spacegun_CControl_FLT_CUTOFF 5L
#undef com_mayaswell_spacegun_CControl_FLT_ENVMOD
#define com_mayaswell_spacegun_CControl_FLT_ENVMOD 6L
#undef com_mayaswell_spacegun_CControl_FLT_RESONANCE
#define com_mayaswell_spacegun_CControl_FLT_RESONANCE 7L
#undef com_mayaswell_spacegun_CControl_TUNE
#define com_mayaswell_spacegun_CControl_TUNE 8L
#undef com_mayaswell_spacegun_CControl_SEND_BASE
#define com_mayaswell_spacegun_CControl_SEND_BASE 20L
#undef com_mayaswell_spacegun_CControl_ENV_BASE
#define com_mayaswell_spacegun_CControl_ENV_BASE 40L
#ifdef __cplusplus
}
#endif
#endif
