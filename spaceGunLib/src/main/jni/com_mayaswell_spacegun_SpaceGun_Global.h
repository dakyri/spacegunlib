/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_mayaswell_spacegun_SpaceGun_Global */

#ifndef _Included_com_mayaswell_spacegun_SpaceGun_Global
#define _Included_com_mayaswell_spacegun_SpaceGun_Global
#ifdef __cplusplus
extern "C" {
#endif
#undef com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD
#define com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_LFO_TGT
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_LFO_TGT 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD
#define com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD 2L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_ENV_TGT
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_ENV_TGT 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_STPR_PER_PAD
#define com_mayaswell_spacegun_SpaceGun_Global_STPR_PER_PAD 2L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_STPR_TGT
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_STPR_TGT 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_BUS
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_BUS 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_BUS_FX
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_BUS_FX 4L
#undef com_mayaswell_spacegun_SpaceGun_Global_MAX_FX_PARAM
#define com_mayaswell_spacegun_SpaceGun_Global_MAX_FX_PARAM 6L
#ifdef __cplusplus
}
#endif
#endif
