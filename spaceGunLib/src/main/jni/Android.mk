LOCAL_PATH:= $(call my-dir)

CORE_AUDIO_BASE:=../../MayaswellCore/jni/audio
CORE_AUDIO_OBJ:=../../MayaswellCore/obj/local/armeabi

include $(CLEAR_VARS)
LOCAL_MODULE            := audio
LOCAL_SRC_FILES         := $(CORE_AUDIO_OBJ)/libaudio.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := cpad
LOCAL_CFLAGS    := -Werror -D__GXX_EXPERIMENTAL_CXX0X__
LOCAL_CPPFLAGS  := -std=c++11
LOCAL_SRC_FILES := cpad.cpp CPadSample.cpp BusMixer.cpp Bus.cpp
LOCAL_LDLIBS    := -llog 
APP_STL := c++_static
LOCAL_STATIC_LIBRARIES := audio
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/include $(LOCAL_PATH)/$(CORE_AUDIO_BASE)/include

include $(BUILD_SHARED_LIBRARY)