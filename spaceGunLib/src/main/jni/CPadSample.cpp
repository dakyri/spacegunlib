/*
 * CPadSample.cpp
 *
 *  Created on: Oct 2, 2013
 *      Author: dak
 */
#include <stddef.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sstream>

#include <android/log.h>

#include "Signal.h"
#include "CPadSample.h"

/********************************************************
 * Main CPad
 ********************************************************/
CPadSample::CPadSample(short _id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt)
{
	id = _id;
	buffer = nullptr;
	controlFrame = 0;
	setBufsize(4096);
	leftCycleBuffer = new float[framesPerControlCycle];
	rightCycleBuffer = new float[framesPerControlCycle];

	gain = 1;
	pan = 0;
	filterType = com_mayaswell_audio_Controllable_Filter_UNITY;
	filterFrequency = 0.5;
	filterResonance = 0;
	filterEnvMod = 0;

	realTune = tune = 0;
	psi = 1;

	for (int i=0; i<maxEnv; i++) {
		env.push_back(Envelope(maxEnvTgt, *this));
	}
	for (int i=0; i<maxLfo; i++) {
		lfo.push_back(LFO(maxLfoTgt, *this));
	}

	nActiveEnv = 0;
	nActiveLFO = 0;
	nActiveSend = 0;

	lastFraction = 0;
	nextDataFrame = 0;

	lfoOffset = std::vector<float>(lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD), 0); // highest number target for a cpad
	envOffset = std::vector<float>(lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD), 1);

	setControlRate(framesPerControlCycle/signalSampleRate);
}

CPadSample::~CPadSample()
{
	if (buffer != NULL) {
		delete [] buffer;
	}
	delete [] leftCycleBuffer;
	delete [] rightCycleBuffer;
}

void
CPadSample::setBufsize(int bufsize)
{
	bufferSize = bufsize;
	if (buffer != NULL) {
		delete [] buffer;
	}
	buffer = new float[bufsize];
}

/********************************************************
 * hooks for jni bridge
 ********************************************************/

void
CPadSample::setSampleInfo(long ntf, short nc)
{
	nTotalFrames = ntf;
	nChannels = nc;
}

void
CPadSample::setGain(float g)
{
	gain = g;
}

void
CPadSample::setPan(float p)
{
	pan = p;
}

void
CPadSample::setTune(float t, float e, float l)
{
	float rt = (t/12)+e+l;
	tune = t;
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set tune %g mods %g %g psi %g\n", t, e, l, psi);
	if (realTune != rt) {
		realTune = rt;
		psi = exp2_tune_psi[rt];//exp2(t/12);
//s		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set tune rt %g psi %g\n", rt, psi);
	}
}

bool
CPadSample::setNSend(int n)
{
	nActiveSend = n;
	return true;
}

int
CPadSample::countNActiveSend(int lastAdded)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "count send %d %d size %d", lastAdded, nActiveSend, send.size());
	if (lastAdded >= nActiveSend) {
		if (send[lastAdded].gain > 0 && !send[lastAdded].mute) {
			return lastAdded+1;
		}
	} else {
		int na = 0;
		for (int j=0; j<nActiveSend; j++) {
			if (send[j].gain > 0 && !send[j].mute) {
				na = j+1;
			}
		}
		return na;
	}
	return nActiveSend;
}

bool
CPadSample::setSend(Bus *b, float g, bool m)
{
	int busid = b->id;
	while (busid >= send.size()) {
		send.push_back(Send());
	}
	send[busid].bus = b;
	send[busid].gain = g;
	send[busid].mute = m;
	nActiveSend = countNActiveSend(busid);
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send %g %d done on %d bus %d, n %d", g, m, busid, id, nActiveSend);
	return true;
}

bool
CPadSample::setSendGain(Bus *b, float g)
{
	int id = b->id;
	while (id >= send.size()) {
		send.push_back(Send());
	}
	send[id].bus = b;
	send[id].gain = g;
	nActiveSend = countNActiveSend(id);

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send gain %g done on %d, n %d", g, id, nActiveSend);
	return true;
}

bool
CPadSample::setSendMute(Bus *b, bool m)
{
	int id = b->id;
	while (id >= send.size()) {
		send.push_back(Send());
	}
	send[id].bus = b;
	send[id].mute = m;
	nActiveSend = countNActiveSend(id);
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send gain %d done on %d, n %d", m, id, nActiveSend);
	return true;

}

void
CPadSample::setFltType(int t)
{
	if (t != filterType) {
		filterType = t;
		lFilter.setType(t);
		lFilter.reset();
		setFilterSweepBounds(filterFrequency, filterEnvMod);
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set filter %d freq %g %g sweep %g %g", filterType, filterFrequency, filterEnvMod, filterSweepMin, filterSweepMax);
		lFilter.setFrequency(filterSweepMax);
		lFilter.setResonance(filterResonance);
		rFilter = lFilter;
	}
}

void
CPadSample::setFltFrequency(float f)
{
	if (f != filterFrequency) {
		filterFrequency = f;
		setFilterSweepBounds(filterFrequency, filterEnvMod);
		lFilter.setFrequency(filterSweepMax);
		rFilter = lFilter;
	}
}


void
CPadSample::setFltEnvMod(float f)
{
	if (f != filterEnvMod) {
		filterEnvMod = f;
		setFilterSweepBounds(filterFrequency, filterEnvMod);
	}
}

void
CPadSample::setFltResonance(float r)
{
	if (r != filterResonance) {
		filterResonance = r;
		lFilter.setResonance(r);
		rFilter = lFilter;
	}
}
void
CPadSample::setFilter(int t, float f, float r, float e)
{
	filterType = t;
	filterResonance = r;
	filterFrequency = f;
	filterEnvMod = e;
	setFilterSweepBounds(f, e);
	lFilter.set(filterType, filterSweepMax, filterResonance);
	rFilter = lFilter;

}

void
CPadSample::setTempo(float t)
{
	TimeSource::setTempo(t);
	for (int i=0; i<nActiveLFO; i++) {
		lfo[i].resetRate();
	}
}

bool
CPadSample::setEnvelopeTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set env target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= env.size()) {
		return false;
	}
	if (whichn >= env[which].nActiveSends || whichn < 0 || whichn >= env[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING)
			return false;
	}

	int was = env[which].target[whichn].target;

	env[which].target[whichn].target = cc;
	env[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set env target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_spacegun_CControl_FLT_CUTOFF) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
		/* nb env mod can affect cutofff */
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0) {
			lFilter.setResonance(filterResonance);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_TUNE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_TUNE)<0 && hasEnv(com_mayaswell_spacegun_CControl_TUNE)<0) {
			setTune(tune);
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set env target %d %d to %d %g was %d %d %d", which, whichn, cc, amt, was, lw, pw);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}

bool
CPadSample::setLFOTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= lfo[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING) {
			return false;
		}
	}
	int was = lfo[which].target[whichn].target;

	lfo[which].target[whichn].target = cc;
	lfo[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_spacegun_CControl_FLT_CUTOFF) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
		/* nb env mod can affect cutofff */
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			lFilter.setFrequency(filterSweepMax);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0) {
			lFilter.setResonance(filterResonance);
			rFilter = lFilter;
		}
	} else if (was == com_mayaswell_spacegun_CControl_TUNE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_TUNE)<0 && hasEnv(com_mayaswell_spacegun_CControl_TUNE)<0) {
			setTune(tune);
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}

void
CPadSample::fire(bool doReset)
{
	controlFrame = 0;
	lFilter.reset();
	rFilter.reset();
	if (doReset) {
		for (int i=0; i<lfo.size(); i++) {
			lfo[i].reset();
		}
		for (int i=0; i<env.size(); i++) {
			env[i].reset();
		}
	}
}

void
CPadSample::stop()
{
}

void
CPadSample::reloop()
{
	for (int i=0; i<lfo.size(); i++) {
		if (lfo[i].resetMode == com_mayaswell_audio_Modulator_RESET_ONLOOP) lfo[i].reset();
	}
	for (int i=0; i<env.size(); i++) {
		if (env[i].resetMode == com_mayaswell_audio_Modulator_RESET_ONLOOP) env[i].reset();
	}
}

void
CPadSample::resetModulators(int modt)
{
	for (int i=0; i<lfo.size(); i++) {
		if (lfo[i].resetMode == modt) lfo[i].reset();
	}
	for (int i=0; i<env.size(); i++) {
		if (env[i].resetMode == modt) env[i].reset();
	}
}

void
CPadSample::resetModulators()
{
	for (int i=0; i<lfo.size(); i++) {
		lfo[i].reset();
	}
	for (int i=0; i<env.size(); i++) {
		env[i].reset();
	}
}

/********************************************************
 * main generator hook
 ********************************************************/
inline void
CPadSample::calculateControlsStereo(float &l, float &r)
{
	float p;
	float g;

	bool hasFreqMod = false;
	bool hasFreqEnv = false;
	bool hasResMod = false;
	bool hasResEnv = false;
	bool hasPanEnv = false;
	bool hasGainEnv = false;
	hasTuneMod = false;
	std::vector<bool> hasLFORateMod(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD, false);
	std::vector<bool> hasLFORateEnv(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD, false);
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "calculateControlsStereo %d %d", nActiveLFO, nActiveEnv);

	for (int i=0; i<lfoOffset.size(); i++) {
		lfoOffset[i] = 0;
	}

	for (int i=0; i<nActiveLFO; i++) {
		LFO &lf = lfo[i];

		float a = lf();
		for (int j=0; j<lf.nActiveSends; j++) {
			ModulationTarget&mt = lf.target[j];
			if (mt.target > 0 && mt.target <lfoOffset.size()) {
				float am = a*(mt.amount*envOffset[lfoTargetDepthId(i,j)]);
//				__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo %d %g", mt.target, am);
				if (mt.target == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
					hasResMod = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_FLT_CUTOFF
						|| mt.target == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
					hasFreqMod = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_TUNE) {
					hasTuneMod = true;
				} else if (isLFOControl(mt.target)) {
					int l = lfo4Id(mt.target);
					int p = lfoParam4Id(mt.target);
					if (p == 0) hasLFORateMod[l] = true;
//				} else if (isEnvControl(mt.target)) {
				}
				lfoOffset[mt.target] += am;

			}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done lfo %d %d", nActiveLFO, nActiveEnv);

	for (int i=0; i<envOffset.size(); i++) {
		envOffset[i] = 1;
	}
	for (int i=0; i<nActiveEnv; i++) {
		Envelope & e = env[i];
		float a = e();
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "envelope %d %g %d", i, a, e.nActiveSends);
		for (int j=0; j<e.nActiveSends; j++) {
			ModulationTarget&mt = e.target[j];
			if (mt.target > 0 && mt.target <envOffset.size()) {
				float mta = mt.amount+lfoOffset[envTargetDepthId(i,j)];
				envOffset[mt.target] = (mt.amount < 0)?((a-1)*mta):(a*mta);
				if (mt.target == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
					hasResEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_PAN) {
					hasPanEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_GAIN) {
					hasGainEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_FLT_CUTOFF
						|| mt.target == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
					hasFreqEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_TUNE) {
					envOffset[mt.target] -= mt.amount;
					hasTuneMod = true;
				} else if (isLFOControl(mt.target)) {
					int l = lfo4Id(mt.target);
					int p = lfoParam4Id(mt.target);
					if (p == 0) hasLFORateEnv[l] = true;
//				} else if (isEnvControl(mt.target)) {
				}
			}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done env %d %d", nActiveLFO, nActiveEnv);

	if (hasFreqMod) { // apply lfo and recalculate the sweepMin and sweepMax
		setFilterSweepBounds(
				filterFrequency+lfoOffset[com_mayaswell_spacegun_CControl_FLT_CUTOFF],
				filterEnvMod+lfoOffset[com_mayaswell_spacegun_CControl_FLT_ENVMOD]);
	}
	if (hasFreqEnv) { // apply envelope [sweepMin, sweepMax] and set frequency
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "envelope %g %g %g", filterSweepMin, filterSweepMax, filterSweepMin+(filterSweepMax-filterSweepMin)*envOffset[com_mayaswell_spacegun_CControl_FLT_CUTOFF]);
		lFilter.setFrequency(filterSweepMin+(filterSweepMax-filterSweepMin)*envOffset[com_mayaswell_spacegun_CControl_FLT_CUTOFF]);
		rFilter = lFilter;
	} else if (hasFreqMod) {
		lFilter.setFrequency(filterSweepMax);
		rFilter = lFilter;
	}
	if (hasResEnv) { // apply envelope and lfo and set resonancef
		lFilter.setResonance(filterResonance*envOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]+lfoOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]);
		rFilter = lFilter;
	} else if (hasResMod) { // apply envelope and lfo and set resonance
		lFilter.setResonance(filterResonance+lfoOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]);
		rFilter = lFilter;
	}
	if (hasTuneMod) {
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "tune envelope %g %g %g", tune, envOffset[com_mayaswell_spacegun_CControl_TUNE], lfoOffset[com_mayaswell_spacegun_CControl_TUNE]);
		setTune(tune, envOffset[com_mayaswell_spacegun_CControl_TUNE], lfoOffset[com_mayaswell_spacegun_CControl_TUNE]);
	}

	for (int i=0; i<nActiveLFO; i++) {
		if (hasLFORateMod[i] || hasLFORateEnv[i]) {
			float mod = 0;
			if (hasLFORateEnv[i]) {
				mod = LFO::logRmin * (1-envOffset[lfoRateId(i)]);
			}
			mod += lfoOffset[lfoRateId(i)];
			float emod = pow2_6[mod];//pow(2, mod);

//			__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo mod %g %g %g", mod, emod, pow(2, mod));
			lfo[i].modulate(emod);
		}
	}

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "adjusted filter %d %d", nActiveLFO, nActiveEnv);

	if (hasPanEnv) {
		p = pan*envOffset[com_mayaswell_spacegun_CControl_PAN] + lfoOffset[com_mayaswell_spacegun_CControl_PAN];
	} else {
		p = pan + lfoOffset[com_mayaswell_spacegun_CControl_PAN];
	}
	if (hasGainEnv) {
		g = gain*envOffset[com_mayaswell_spacegun_CControl_GAIN] + lfoOffset[com_mayaswell_spacegun_CControl_GAIN];
	} else {
		g = gain + lfoOffset[com_mayaswell_spacegun_CControl_GAIN];
	}
	if (p < -1) p = -1; else if (p > 1) p = 1;
	if (g < 0) g = 0; else if (g > 1) g = 1;
	r = rAmp = (g)*(1+p)/2;
	l = lAmp = (g)*(1-p)/2;
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "adjusted parameters %g %g %g %g %g %g", lAmp, rAmp, p, g, pan, gain);
}

/**
 * I'm not expecting this to get called ... most of the lf modulation stuff will work ... fill this sometime later for completeness,
 * but ignore until then
 * todo
 */
inline void
CPadSample::calculateControlsMono(float &l)
{
	l = lAmp = gain;
}

inline int
CPadSample::playChunkReplacing2(
		float *buff, int nIterFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, short direction)
{
	int nIterDataFrames = ((currentDataFrame + nIterFrames > nTotalFrames)?(nTotalFrames-currentDataFrame):nIterFrames);
	if (nIterDataFrames < 0) nIterDataFrames = 0;
	int coff = 0;
	if (chunkLen == 0) {
		coff = 0;
		nIterDataFrames = 0;
	} else if (nIterDataFrames > 0) {
		coff = currentDataFrame-chunkStart;
		if (coff < 0) {
			coff = 0;
		}
		if (coff + nIterDataFrames > chunkLen) {
			nIterDataFrames = chunkLen - coff;
			nIterFrames = nIterDataFrames;
		}
		coff = coff*nChannels;
	}

	float *lSig = leftCycleBuffer;
	float *rSig = rightCycleBuffer;
	unsigned int buffBnd;
	unsigned int buffI = 0;
	unsigned int chunkI = coff;
	unsigned int nCycleFrames;
	unsigned int nControlChunkFrames;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "pcr %d %d %d %d", nIterFrames, nIterDataFrames, nIterFrames-nIterDataFrames, coff);
	if (nOutChannels == 2) {
		float crAmp=rAmp;
		float clAmp=lAmp;
		buffBnd = buffI+2*nIterDataFrames;
		nCycleFrames = nIterDataFrames;
		if (nChannels == 2) {

			do {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				for (unsigned int i=0; i<nControlChunkFrames; i++) {
					lSig[i] = chunkData[chunkI++];
					rSig[i] = chunkData[chunkI++];
				}
				lFilter.apply(lSig, nControlChunkFrames);
				rFilter.apply(rSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*rSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
			} while (buffI < buffBnd);

			nCycleFrames = nIterFrames-nIterDataFrames;
			buffBnd += 2*nCycleFrames;
			while (buffI < buffBnd) {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(rSig, 0, nControlChunkFrames*sizeof(float));
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
				lFilter.apply(lSig, nControlChunkFrames);
				rFilter.apply(rSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*rSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
			}
		} else if (nChannels == 1) {
			do {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
//				__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "out bufi %d ccframes %d controlframe %d ncycleframe %d coff %d chunki %d", buffI, nControlChunkFrames, controlFrame, nCycleFrames, coff, chunkI);
				for (unsigned int i=0; i<nControlChunkFrames; i++) {
					lSig[i] = chunkData[chunkI++];
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0; // -= framesPerControlCycle ... if it's > we have a mess where control cycle is skipped. so line above, nControlChunkFrames = .... etc
				}
				nCycleFrames -= nControlChunkFrames;
			} while (buffI < buffBnd);
			nCycleFrames = nIterFrames-nIterDataFrames;
//			__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "out ncycle frame %d buffi %d buffbnd %d coff %d", nCycleFrames, buffI, buffBnd, coff);
			buffBnd += 2*nCycleFrames;
			while (buffI < buffBnd) {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
			}
		}
	} else if (nOutChannels == 1) { // dull, dull, dull
		float clAmp = lAmp;
		buffBnd = buffI+nIterDataFrames;
		if (nChannels == 2) {
			do {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				for (unsigned int i=0; i<nControlChunkFrames; i++) {
					lSig[i] = (chunkData[chunkI++]+chunkData[chunkI++])/2;
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
			} while (buffI < buffBnd);
		} else if (nChannels == 1) {
			do {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				for (unsigned int i=0; i<nControlChunkFrames; i++) {
					lSig[i] = chunkData[chunkI++];
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
			} while (buffI < buffBnd);
		}
		nCycleFrames = nIterFrames-nIterDataFrames;
		buffBnd += nCycleFrames;
		while (buffI < buffBnd) {
			if (controlFrame == 0) {
				calculateControlsMono(clAmp);
			}
			nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
			memset(lSig, 0, nControlChunkFrames*sizeof(float));
			lFilter.apply(lSig, nControlChunkFrames);
			for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
				buff[buffI] = clAmp*lSig[i];
			}
			if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
				controlFrame = 0;
			}
			nCycleFrames -= nControlChunkFrames;
		}
	} else {
		// we would be exceedingly ambitious for a phone ;D
	}
	return nIterFrames;
}

/**
 *
 *
 * @param buff output buffer
 * @param nRequestedFrames optimal number of frames to play.
 * @param chunkData data etc for the chunk
 * @param chunkStartFrame
 * @param chunkNFrames
 * @param nextBufferStartL start of 'next' buffer
 * @param nextBufferStartR
 * @param direction >= 0 for forwards, else backwards
 */
inline int
CPadSample::playChunkReplacing(
		float *buff, int nRequestedFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStartFrame, int chunkNFrames, float nextBufferStartL, float nextBufferStartR, short direction)
{
	int currentBufferOffset = 0;
	int nDataFramesAvailable = 0;
	if (chunkNFrames > 0) {
		currentBufferOffset = currentDataFrame-chunkStartFrame; // currentBufferOffest initially in frames
		if (currentBufferOffset < 0) {
			currentBufferOffset = 0;
		}
		nDataFramesAvailable = (direction >= 1)?(chunkNFrames - currentBufferOffset):(currentBufferOffset+1);
		if (nDataFramesAvailable < 0) {
			nDataFramesAvailable = 0;
		}
		currentBufferOffset = currentBufferOffset*nChannels; // currentBufferOffest now an index into buffer
	}

	float *lSig = leftCycleBuffer;
	float *rSig = rightCycleBuffer;
	unsigned int buffI = 0;
	unsigned int chunkI = currentBufferOffset;
	unsigned int nCycleFrames;
	unsigned int nControlChunkFrames;
	unsigned int nOutputFrames = 0;
	float lRaw1 = 0;
	float rRaw1 = 0;
	float lRaw2 = 0;
	float rRaw2 = 0;
	long lframe=-1;
	long frame=-1;
	long nextBufferStartFrame = (direction >= 0)?(chunkStartFrame+chunkNFrames):(chunkStartFrame-1);

	bool pitchShifting = (tune != 0 || hasTuneMod);
	double phs=0;
	if (currentDataFrame != nextDataFrame) {
		phs = currentDataFrame;
	} else {
		phs = currentDataFrame+lastFraction;
	}

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "pcr bufsize %d nreqframes %d ndataframes %d %d %d chunk %d %d ", bufferSize, nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
	if (nOutChannels == 2) {
		float crAmp=rAmp;
		float clAmp=lAmp;
		if (nChannels == 2) {
			nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, stereo sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
							rSig[i] = chunkData[chunkI++];
						}
					} else { // backwards, un pitch shifted, stereo sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
							rSig[i] = chunkData[chunkI--];
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, stereo sample data into a stereo out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI+2];
										rRaw2 = chunkData[chunkI+3];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							rSig[i] = interpolate(rRaw1, rRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, stereo sample data into a stereo out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI-2];
										rRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							rSig[i] = interpolate(rRaw1, rRaw2, frac);
							phs += rpsi;
						}
					}
				}
				lFilter.apply(lSig, nControlChunkFrames);
				rFilter.apply(rSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*rSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}

			if ((!pitchShifting?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0) {
				nCycleFrames = nRequestedFrames-nOutputFrames;
				while (nCycleFrames > 0) {
					if (controlFrame == 0) {
						calculateControlsStereo(clAmp, crAmp);
					}
					nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
					memset(rSig, 0, nControlChunkFrames*sizeof(float));
					memset(lSig, 0, nControlChunkFrames*sizeof(float));
					lFilter.apply(lSig, nControlChunkFrames);
					rFilter.apply(rSig, nControlChunkFrames);
					for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
						buff[buffI] = clAmp*lSig[i];
						buff[buffI+1] = crAmp*rSig[i];
					}
					if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
						controlFrame = 0;
					}
					nCycleFrames -= nControlChunkFrames;
					nOutputFrames += nControlChunkFrames;
					float rpsi = (direction>=0)?psi:-psi;
					phs += rpsi*nControlChunkFrames;
				}
			}
		} else if (nChannels == 1) {
			nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
			while (nCycleFrames > 0) {
//				__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "cycle %d %d %d",  currentmixer->busCount(),controlFrame, nCycleFrames);
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, mono sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
						}
					} else  {  // backwards, un pitch shifted, mono sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, single channel sample data into a stereo out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI+1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, single channel sample data into a stereo out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					}
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0; // -= framesPerControlCycle ... if it's > we have a mess where control cycle is skipped. so line above, nControlChunkFrames = .... etc
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
			if (((!pitchShifting)?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0) {
				nCycleFrames = nRequestedFrames-nOutputFrames;
				while (nCycleFrames > 0) {
					if (controlFrame == 0) {
						calculateControlsStereo(clAmp, crAmp);
					}
					nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
					memset(lSig, 0, nControlChunkFrames*sizeof(float));
					lFilter.apply(lSig, nControlChunkFrames);
					for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
						buff[buffI] = clAmp*lSig[i];
						buff[buffI+1] = crAmp*lSig[i];
					}
					if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
						controlFrame = 0;
					}
					nCycleFrames -= nControlChunkFrames;
					nOutputFrames += nControlChunkFrames;
					float rpsi = (direction>=0)?psi:-psi;
					phs += rpsi*nControlChunkFrames;
				}
			}
		}
	} else if (nOutChannels == 1) { // mono output ... an edge case we hope not to see
		float clAmp = lAmp;
		nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
		if (nChannels == 2) {
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, stereo sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = (chunkData[chunkI++]+chunkData[chunkI++])/2;
						}
					} else {  // backwards, un pitch shifted, stereo sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = (chunkData[chunkI--]+chunkData[chunkI--])/2;
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, stereo sample data into a mono out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI+2];
										rRaw2 = chunkData[chunkI+3];
									}
								}
							}
							lSig[i] = (interpolate(lRaw1, lRaw2, frac)+interpolate(rRaw1, rRaw2, frac))/2;
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, stereo sample data into a mono out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI-2];
										rRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = (interpolate(lRaw1, lRaw2, frac)+interpolate(rRaw1, rRaw2, frac))/2;
							phs += rpsi;
						}
					}
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		} else if (nChannels == 1) { // this is near identical to the mono -> stereo case except for calculateControlsMono
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, single channel sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
						}
					} else { // backwards, un pitch shifted, single channel sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					float rpsi = (direction>=0)?psi:-psi;
					if (direction >= 0) { // forwards, pitch shifted, single channel sample data into a mono out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI+1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, single channel sample data into a mono out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					}
				}
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		}
		if (((!pitchShifting)?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0) {
			nCycleFrames = nRequestedFrames-nOutputFrames;
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
				float rpsi = (direction>=0)?psi:-psi;
				phs += psi*nControlChunkFrames;
			}
		}
	} else {
		// we would be exceedingly ambitious for a phone ;D
	}
	if (!pitchShifting) {
		nextDataFrame = currentDataFrame + (direction >= 0?nOutputFrames:-nOutputFrames);
		lastFraction = 0;
	} else {
		nextDataFrame = floor(phs);
		lastFraction = phs-nextDataFrame;
	}
	return nOutputFrames;
}

int
CPadSample::playChunk(
		float *buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, short direction)
{
	return playChunkReplacing2(buff + buffInd, nIterFrames, nOutChannels, currentDataFrame, chunkData, chunkStart, chunkLen, direction);
}

int
CPadSample::playChunkCumulative(
		float *buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, short direction)
{
	nIterFrames = playChunkReplacing2(buffer, nIterFrames, nOutChannels, currentDataFrame, chunkData, chunkStart, chunkLen, direction);
	buff += buffInd;
	for (int i=0; i<nOutChannels*nIterFrames; i++) {
		buff[i] += buffer[i];
	}
	return nIterFrames;
}

int
CPadSample::playChunkMixer1(
		BusMixer *mixer, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, short direction)
{
	nIterFrames = playChunkReplacing2(buffer, nIterFrames, nOutChannels, currentDataFrame, chunkData, chunkStart, chunkLen, direction);

	if (send.size() == 0) {
		mixer->getBus(0)->mxn(buffer, buffInd, nIterFrames, 1);
	} else {
		for (int i=0; i<nActiveSend; i++) {
			Send &s = send[i];
			if (s.bus != NULL && s.gain > 0 && !s.mute) {
				s.bus->mxn(buffer, buffInd, nIterFrames, s.gain);
			}
		}
	}
	return nIterFrames;
}

int
CPadSample::playChunkMixer(
		BusMixer *mixer, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "playChunkMixer() %d busses chunk %d %d %x", mixer->busCount(), chunkStart, (int)chunkLen, (unsigned)mixer);
	nIterFrames = playChunkReplacing(
			buffer, nIterFrames, nOutChannels, currentDataFrame, chunkData, chunkStart, chunkLen,
			nextBufferStartL, nextBufferStartR, direction);

	if (send.size() == 0) {
		mixer->getBus(0)->mxn(buffer, buffInd, nIterFrames, 1);
	} else {
		for (int i=0; i<nActiveSend; i++) {
			Send &s = send[i];
			if (s.bus != NULL && s.gain > 0 && !s.mute) {
				s.bus->mxn(buffer, buffInd, nIterFrames, s.gain);
			}
		}
	}
	return nIterFrames;
}


