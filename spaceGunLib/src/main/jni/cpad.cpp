/*
 * cpad.cpp
 *
 *  Created on: Oct 2, 2013
 *      Author: dak
 */
#include <stdlib.h>
#include "com_mayaswell_audio_Controllable.h"
#include "com_mayaswell_spacegun_PadSample.h"
#include "com_mayaswell_spacegun_BusMixer.h"
#include "com_mayaswell_spacegun_Bus.h"

#include "CPadSample.h"
#include "BusMixer.h"
#include "Bus.h"

const float A_E	= 2.0;
const float FALLOFF	= 200.0;
const float pow2Range = 6;
const float pad2Octs = (com_mayaswell_audio_Controllable_TUNE_RANGE_SEMITONES/12);

FunctionTable 		pow2Wavetable(wavetableLength);
FunctionTable 		exp_1_FalloffWavetable(wavetableLength);
FunctionTable 		logFalloffWavetable(wavetableLength);
FunctionTable 		exp2TuneWavetable(wavetableLength);

BoundedLookup		pow2_6(&pow2Wavetable, -pow2Range, pow2Range);
BoundedLookup		exp2_tune_psi(&exp2TuneWavetable, -pad2Octs, pad2Octs);

/*
emap = new BoundedLooKup(exp_1_FalloffWavetable,-FALLOFF, FALLOFF);
amap = new BoundedLooKup(logFalloffWavetable,0,200);

ampEnvelope	. DecayTime((signalSampleRate/maxFramesPerChunk)*2.1);
ampEnvelope . Endpoints(1,0);
ampEnvelope . DecayAlpha(-7);

vcf.envelope.DecayTime((signalSampleRate/maxFramesPerChunk)*2.1); // DecayTime takes #env updates ... so 2.1s decay time

inline void BassLineFilter::DecayRate(float decay)
{
	if (decayRate != decay) {
		decayRate = decay;
		float freqAlpha = -2.5 - (1.-decay)*9;
		envelope.DecayAlpha(freqAlpha * -freqAlpha);
	}
}

inline ExpEnvelope&
ExpEnvelope::DecayAlpha(float alpha)
{
	this->d_alpha=alpha;
	//onemEAlpha=1.-exp(d_alpha);
	onemEAlpha=(*emap)[d_alpha];
	return *this;
}
if (segTime < decayTime) {
	theEnv =  yStart + yEndmStart*
					(((*emap)[(segTime/(float)decayTime)*d_alpha])/onemEAlpha);
}
*/

/*******************************************************
 * CPadSample entry points
 *******************************************************/

JNIEXPORT jlong JNICALL Java_com_mayaswell_spacegun_PadSample_allocCPad(JNIEnv *env, jclass obj, jint id, jint nEnv, jint nEnvTgt, jint nLfo, jint nLfoTgt)
{
	return (jlong)(new CPadSample(id, nEnv, nEnvTgt, nLfo, nLfoTgt));
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_deleteCPad(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return;
	delete ((CPadSample *)padSamplePointer);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadBufsize(JNIEnv *env, jclass obj, jlong padSamplePointer, jint bufsize)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setBufsize(bufsize);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_globalCPadInit(JNIEnv *env, jclass obj)
{
	int i;
	for(i=0;i<=wavetableLength;i++) {
		sineWavetable[i] = sin(2*M_PI*((float)i/(float)wavetableLength));
		negSawWavetable[i] = ((float)(wavetableLength-2*i)/(float)wavetableLength);
		posSawWavetable[i] = ((float)(2*i-wavetableLength)/(float)wavetableLength);
		negExpWavetable[i] =
			(2*exp(A_E* ((float)(wavetableLength-2*i)/(float)wavetableLength))/exp(A_E)) - 1;
		posExpWavetable[i] =
			(2*exp(A_E* ((float)(2*i-wavetableLength)/(float)wavetableLength))/exp(A_E)) - 1;
		squareWavetable[i] = (i<(wavetableLength/2))?1:-1;

		pow2Wavetable[i] = pow(2, pow2Range*((float)(2*i-wavetableLength))/wavetableLength);
		exp2TuneWavetable[i] = pow(2, pad2Octs*((float)(2*i-wavetableLength))/wavetableLength);

		exp_1_FalloffWavetable[i]=
			1. - exp(((float)(i-(wavetableLength/2.))/(wavetableLength/2.))*FALLOFF);
		logFalloffWavetable[i]=log((((float)i/wavetableLength)*FALLOFF+1));
	}
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_globalCPadCleanup(JNIEnv *env, jclass obj)
{

}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadSampleInfo(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong ntf, jshort nc)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setSampleInfo(ntf, nc);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadGain(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat g)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setGain(g);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadPan(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat p)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setPan(p);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadTune(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat p)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setTune(p);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadNSend(JNIEnv *env, jclass obj, jlong padSamplePointer, jint n)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setNSend(n);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadSend(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong busPointer, jfloat gain, jboolean mute)
{
	if (padSamplePointer == 0 || busPointer == 0) {
		return false;
	}
	return ((CPadSample *)padSamplePointer)->setSend((Bus *)busPointer, gain, mute);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadSendGain(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong busPointer, jfloat gain)
{
	if (padSamplePointer == 0 || busPointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setSendGain((Bus *)busPointer, gain);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadSendMute(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong busPointer, jboolean mute)
{
	if (padSamplePointer == 0 || busPointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setSendMute((Bus *)busPointer, mute);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadFltType(JNIEnv *env, jclass obj, jlong padSamplePointer, jint t)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setFltType(t);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadFltFrequency(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat f)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setFltFrequency(f);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadFltEnvMod(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat f)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setFltEnvMod(f);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadFltResonance(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat r)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setFltResonance(r);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadFlt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint t, jfloat f, jfloat r, jfloat e)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setFilter(t, f, r, e);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadTempo(JNIEnv *env, jclass obj, jlong padSamplePointer, jfloat t)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->setTempo(t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnv(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jbyte reset, jboolean lock,
		jfloat attackT, jfloat decayT, jfloat sustainT, jfloat sustainL, jfloat releaseT)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelope(which, reset, lock, attackT, decayT, sustainT, sustainL, releaseT);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadNEnv(JNIEnv *env, jclass obj, jlong padSamplePointer, jint n)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setNEnvelope(n);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadNEnvTgt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint ntgt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setNEnvelopeTarget(which, ntgt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvTgt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint whicht, jint tgt, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeTarget(which, whicht, tgt, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvTgtAmt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint whicht, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeAmount(which, whicht, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvReset(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jbyte rst)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeReset(which, rst);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvLock(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jboolean lck)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeLock(which, lck);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvAtt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat t)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeAttack(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvDec(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat t)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeDecay(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvSus(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat t)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeSustain(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvSusLevel(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat l)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeSustainLevel(which, l);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadEnvRel(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat t)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setEnvelopeRelease(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFO(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which,
		jbyte wf, jbyte reset, jboolean lock, jfloat rate, jfloat phs)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFO(which, wf, reset, lock, rate, phs);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadNLFO(JNIEnv *env, jclass obj, jlong padSamplePointer, jint n)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setNLFO(n);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadNLFOTgt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint ntgt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setNLFOTarget(which, ntgt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOTgt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint whicht, jint tgt, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOTarget(which, whicht, tgt, amt);
}
JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOTgtAmt(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jint whicht, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOAmount(which, whicht, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOReset(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jbyte rst)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOReset(which, rst);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOLock(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jboolean lck)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOLock(which, lck);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOWave(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jbyte wf)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOWave(which, wf);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFORate(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFORate(which, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_PadSample_setCPadLFOPhase(JNIEnv *env, jclass obj, jlong padSamplePointer, jint which, jfloat amt)
{
	if (padSamplePointer == 0) return false;
	return ((CPadSample *)padSamplePointer)->setLFOPhase(which, amt);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_fireCPad(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->fire();
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_stopCPad(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->stop();
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_PadSample_reloopCPad(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return;
	((CPadSample *)padSamplePointer)->reloop();
}


JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_playChunkC(JNIEnv *env, jclass obj, jlong padSamplePointer,
		jfloatArray buff, jint buffInd, jint nIterFrames, jshort nOutChannels, jint currentDataFrame,
		jfloatArray chunkData, jint chunkStart, jint chunkLen)
{
	if (padSamplePointer == 0) return (jint)0;
	unsigned char isCopyBuff, isCopyChunk;

	jfloat *buffC = env->GetFloatArrayElements(buff, &isCopyBuff);
	if (buffC == NULL) return (jint)0;
	jsize buffLen = env->GetArrayLength(buff);

	jfloat *chunkDataC;
	if (chunkData != NULL) {
		chunkDataC = env->GetFloatArrayElements(chunkData, &isCopyChunk);
		if (chunkDataC == NULL) return (jint)0;
	} else {
		chunkDataC = NULL;
		chunkLen = 0;
		chunkStart = 0;
	}

	nIterFrames = ((CPadSample *)padSamplePointer)->playChunk(
													buffC, buffInd, nIterFrames, nOutChannels, currentDataFrame,
													chunkDataC, chunkStart, chunkLen, 1);

	env->ReleaseFloatArrayElements(buff, buffC, 0);
	if (chunkDataC != NULL) env->ReleaseFloatArrayElements(chunkData, chunkDataC, 0);

	return (jint)nIterFrames;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_playChunkCumulativeC(JNIEnv *env, jclass obj, jlong padSamplePointer,
		jfloatArray buff, jint buffInd, jint nIterFrames, jshort nOutChannels, jint currentDataFrame,
		jfloatArray chunkData, jint chunkStart, jint chunkLen)
{
	if (padSamplePointer == 0) return (jint)0;
	unsigned char isCopyBuff, isCopyChunk;

	jfloat *buffC = env->GetFloatArrayElements(buff, &isCopyBuff);
	if (buffC == NULL) return (jint)0;
	jsize buffLen = env->GetArrayLength(buff);

	jfloat *chunkDataC;
	if (chunkData != NULL) {
		chunkDataC = env->GetFloatArrayElements(chunkData, &isCopyChunk);
		if (chunkDataC == NULL) return (jint)0;
	} else {
		chunkDataC = NULL;
		chunkLen = 0;
		chunkStart = 0;
	}

	nIterFrames = ((CPadSample *)padSamplePointer)->playChunkCumulative(
													buffC, buffInd, nIterFrames, nOutChannels, currentDataFrame,
													chunkDataC, chunkStart, chunkLen, 1);

	env->ReleaseFloatArrayElements(buff, buffC, 0);
	if (chunkDataC != NULL) env->ReleaseFloatArrayElements(chunkData, chunkDataC, 0);

	return (jint)nIterFrames;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_playChunkMixer1(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong busMixerPointer,
		jint buffInd, jint nIterFrames, jshort nOutChannels, jint currentDataFrame, jfloatArray chunkData, jint chunkStart, jint chunkLen, jint direction)
{
	if (padSamplePointer == 0 || busMixerPointer == 0) return (jint)0;
	unsigned char isCopyChunk;

	jfloat *chunkDataC;
	if (chunkData != NULL) {
		chunkDataC = env->GetFloatArrayElements(chunkData, &isCopyChunk);
		if (chunkDataC == NULL) return (jint)0;
	} else {
		chunkDataC = NULL;
		chunkLen = 0;
		chunkStart = 0;
	}

	nIterFrames = ((CPadSample *)padSamplePointer)->playChunkMixer1(
			((BusMixer *)busMixerPointer), buffInd, nIterFrames, nOutChannels, currentDataFrame,
			chunkDataC, chunkStart, chunkLen, direction);

	if (chunkDataC != NULL) env->ReleaseFloatArrayElements(chunkData, chunkDataC, 0);
	return (jint)nIterFrames;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_playChunkMixer(JNIEnv *env, jclass obj, jlong padSamplePointer, jlong busMixerPointer,
		jint buffInd, jint nIterFrames, jshort nOutChannels, jint currentDataFrame, jfloatArray chunkData, jint chunkStart, jint chunkLen,
		jfloat nextL, jfloat nextR, jint direction)
{
	if (padSamplePointer == 0 || busMixerPointer == 0) return (jint)0;
	unsigned char isCopyChunk;

	jfloat *chunkDataC;
	if (chunkData != NULL) {
		chunkDataC = env->GetFloatArrayElements(chunkData, &isCopyChunk);
		if (chunkDataC == NULL) return (jint)0;
	} else {
		chunkDataC = NULL;
		chunkLen = 0;
		chunkStart = 0;
	}

	nIterFrames = ((CPadSample *)padSamplePointer)->playChunkMixer(
			((BusMixer *)busMixerPointer), buffInd, nIterFrames, nOutChannels, currentDataFrame,
			chunkDataC, chunkStart, chunkLen, nextL, nextR, direction);

	if (chunkDataC != NULL) env->ReleaseFloatArrayElements(chunkData, chunkDataC, 0);
	return (jint)nIterFrames;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_getNextDataFrame(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return (jint)0;
	return ((CPadSample *)padSamplePointer)->nextDataFrame;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_spacegun_PadSample_activeBusCount(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return (jint)0;
	return ((CPadSample *)padSamplePointer)->nActiveSend;
}


/*******************************************************
 * BusMixer entry points
 *******************************************************/

JNIEXPORT jlong JNICALL Java_com_mayaswell_spacegun_BusMixer_allocBmx(JNIEnv *env, jclass obj, jint bufsize)
{
	return (jlong)(new BusMixer(bufsize));
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_deleteBmx(JNIEnv *env, jclass obj, jlong busMixerPointer)
{
	if (busMixerPointer == 0) return;
	delete ((BusMixer *)busMixerPointer);

}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_setBmxBufsize(JNIEnv *env, jclass obj, jlong busMixerPointer, jint bufsize)
{
	if (busMixerPointer == 0) return;
	((BusMixer *)busMixerPointer)->setBufsize(bufsize);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_globalBmxInit(JNIEnv *env, jclass obj)
{
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_globalBmxCleanup(JNIEnv *env, jclass obj)
{

}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_setBmxTempo(JNIEnv *env, jclass obj, jlong busMixerPointer, jfloat tempo)
{
	if (busMixerPointer == 0) return;
	((BusMixer *)busMixerPointer)->setTempo(tempo);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_zeroBmxBuffers(JNIEnv *env, jclass obj, jlong busMixerPointer, jint nFramePerBuf)
{
	if (busMixerPointer == 0) return;
	((BusMixer *)busMixerPointer)->zeroBuffers(nFramePerBuf);
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_BusMixer_bmxMix(JNIEnv *env, jclass obj, jlong busMixerPointer, jint nActiveBus, jshortArray outBuffer, jint nFramePerBuf, jshort nOutChannels)
{
	if (busMixerPointer == 0) return;
	unsigned char isCopyBuf;

	jshort *outbufC;

	if (outBuffer != NULL) {
		outbufC = env->GetShortArrayElements(outBuffer, &isCopyBuf);
		if (outbufC == NULL) return;
	}
	((BusMixer *)busMixerPointer)->mix(nActiveBus, outbufC, nFramePerBuf, nOutChannels);
	if (outbufC != NULL) env->ReleaseShortArrayElements(outBuffer, outbufC, 0);
	return;
}

/*******************************************************
 * Bus entry points
 *******************************************************/

JNIEXPORT jlong JNICALL Java_com_mayaswell_spacegun_Bus_allocBus(JNIEnv *env, jclass obj, jlong busMixerPointer, jint id, jint nFX, jint nFXPar, jint nLfo, jint nLfoTgt)
{
	if (busMixerPointer == 0) return 0;
	Bus *b = (new Bus(id, nFX, nFXPar, nLfo, nLfoTgt));
	((BusMixer *)busMixerPointer)->addBus(b);
	return (jlong) b;
}

JNIEXPORT void JNICALL Java_com_mayaswell_spacegun_Bus_deleteBus(JNIEnv *env, jclass obj, jlong busPointer)
{
	if (busPointer == 0) return;
	delete ((Bus *)busPointer);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFO(JNIEnv *env, jclass obj, jlong busPointer, jint which,
		jbyte wf, jbyte reset, jboolean lock, jfloat rate, jfloat phs)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFO(which, wf, reset, lock, rate, phs);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusNLFO(JNIEnv *env, jclass obj, jlong busPointer, jint n)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setNLFO(n);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusNLFOTgt(JNIEnv *env, jclass obj, jlong busPointer, jint which, jint ntgt)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setNLFOTarget(which, ntgt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOTgt(JNIEnv *env, jclass obj, jlong busPointer, jint which, jint whicht, jint tgt, jfloat amt)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOTarget(which, whicht, tgt, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOTgtAmt(JNIEnv *env, jclass obj, jlong busPointer, jint which, jint whicht, jfloat amt)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOAmount(which, whicht, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOReset(JNIEnv *env, jclass obj, jlong busPointer, jint which, jbyte rst)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOReset(which, rst);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOLock(JNIEnv *env, jclass obj, jlong busPointer, jint which, jboolean lck)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOLock(which, lck);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOWave(JNIEnv *env, jclass obj, jlong busPointer, jint which, jbyte wf)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOWave(which, wf);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFORate(JNIEnv *env, jclass obj, jlong busPointer, jint which, jfloat amt)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFORate(which, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusLFOPhase(JNIEnv *env, jclass obj, jlong busPointer, jint which, jfloat amt)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setLFOPhase(which, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusFXType(JNIEnv *env, jclass obj, jlong busPointer, jint whichFX, jint type, jint nParam, jfloatArray paramsJ)
{
	if (busPointer == 0) return false;
	unsigned char isCopyBuf;
	jfloat *paramsC = NULL;

	if (paramsJ != NULL) {
		paramsC = env->GetFloatArrayElements(paramsJ, &isCopyBuf);
		if (paramsC == NULL) return false;
	}
	return ((Bus *)busPointer)->setFXType(whichFX, type, nParam, paramsC);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusFXEnable(JNIEnv *env, jclass obj, jlong busPointer, jint whichFX, jboolean en)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setFXEnable(whichFX, en);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusFXParam(JNIEnv *env, jclass obj, jlong busPointer, jint whichFX, jint whichParam, jfloat val)
{
	if (busPointer == 0) return false;
	return ((Bus *)busPointer)->setFXParam(whichFX, whichParam, val);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_spacegun_Bus_setBusFXParams(JNIEnv *env, jclass obj, jlong busPointer, jint whichFX, jint nParam, jfloatArray paramsJ)
{
	if (busPointer == 0) return false;
	unsigned char isCopyBuf;
	jfloat *paramsC;

	if (paramsJ != NULL) {
		paramsC = env->GetFloatArrayElements(paramsJ, &isCopyBuf);
		if (paramsC == NULL) return false;
	}
	return ((Bus *)busPointer)->setFXParams(whichFX, nParam, paramsC);
}
