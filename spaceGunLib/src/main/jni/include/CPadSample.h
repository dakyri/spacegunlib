/*
 * CPadSample.h
 *
 *  Created on: Oct 2, 2013
 *      Author: dak
 */

#ifndef CPADSAMPLE_H_
#define CPADSAMPLE_H_


#include <vector>

#include "Envelope.h"
#include "LFO.h"
#include "BusMixer.h"

#include "Filter.h"
#include "Send.h"

#include "com_mayaswell_spacegun_CControl.h"
#include "com_mayaswell_audio_Controllable.h"

#include "ControlDefs.h"


class CPadSample: public TimeSource, public LFOHost, public EnvelopeHost, public FilterHost
{
public:
	CPadSample(short _id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt);
	~CPadSample();

	void setBufsize(int bufsize);

	void setSampleInfo(long ntf, short nc);
	void setGain(float g);
	void setPan(float p);
	void setTune(float t, float e=0, float l=0);
	void setFltType(int t);
	void setFltFrequency(float f);
	void setFltResonance(float r);
	void setFltEnvMod(float e);
	void setFilter(int t, float f, float r, float e);
	void setTempo(float t);

	bool setLFOTarget(int which, int whichn, int cc, float amt);
	bool setEnvelopeTarget(int which, int whichn, int cc, float amt);

	bool setNSend(int n);
	bool setSend(Bus *b, float g, bool m);
	bool setSendGain(Bus *b, float g);
	bool setSendMute(Bus *b, bool m);

	void fire(bool doReset=true);
	void stop();
	void reloop();

	void resetModulators(int mode);
	void resetModulators();

	int playChunk(
			float *buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, short direction);
	int playChunkCumulative(
			float *buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, short direction);
	int playChunkMixer1(
			BusMixer *mixer, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, short direction);
	int playChunkMixer(
			BusMixer *mixer, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction);
	short id;

	float gain;
	float pan;

	float tune;
	float realTune;
	float psi;
	bool hasTuneMod;

	float lastFraction;
	long nextDataFrame;

	long nTotalFrames;
	short nChannels;

protected:
	int playChunkReplacing(
			float *buff, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction);
	int playChunkReplacing2(
			float *buff, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, short direction);
	int playChunkReplacing1(
			float *buff, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen);
	int playChunkReplacing0(
			float *buff, int nIterFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen);
	void calculateControlsMono(float &l);
	void calculateControlsStereo(float &l, float &r);


	Filter lFilter;
	Filter rFilter;

	int countNActiveSend(int lastAdded);

	unsigned int controlFrame;
	float 		*buffer;
	float 		*leftCycleBuffer;
	float 		*rightCycleBuffer;
	int 		bufferSize;


	float				lAmp;
	float				rAmp;

	std::vector<float>	lfoOffset;
	std::vector<float>	envOffset;

	std::vector<Send>	send;
public:
	int					nActiveSend;

};

extern BoundedLookup pow2_6;
extern BoundedLookup exp2_tune_psi;
#endif /* CPADSAMPLE_H_ */
