/*
 * Send.h
 *
 *  Created on: Nov 10, 2015
 *      Author: dak
 */

#ifndef SEND_H_
#define SEND_H_

#include "Bus.h"

class Send
{
public:
	Send(Bus *b=NULL, float g=1, bool m=false)
	{
		gain = 1;
		bus = NULL;
		mute = false;
	}

	Bus		*bus;
	float	gain;
	bool	mute;
};


#endif /* SEND_H_ */
