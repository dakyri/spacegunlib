/*
 * Bus.h
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#ifndef BUS_H_
#define BUS_H_

#include <vector>
#include "FXChain.h"
#include "Effect.h"
#include "LFO.h"

class Bus: public FXChain, public TimeSource, public LFOHost
{
public:
	Bus(short _id, int maxLfo, int maxLfoTgt, int maxFX, int maxFXparam);
	virtual ~Bus();

	float *getBuffer();

	void setTempo(float t);

	bool setLFOTarget(int which, int whichn, int cc, float amt);
	void setBufsize(int bufsize);

	unsigned int mxn(float *buffer, int offset, int nFrame, float gain);

protected:
	std::vector<float>		lfoOffset;

	float *					buffer;
	int						bufferSize;
};


#endif /* BUS_H_ */
