/*
 * BusMixer.h
 *
 *  Created on: Nov 15, 2013
 *      Author: dak
 */

#ifndef BUSMIXER_H_
#define BUSMIXER_H_

#include <vector>

#include "Bus.h"

class BusMixer
{
public:
	BusMixer(int bufSize);
	~BusMixer();

	Bus *addBus(Bus *b);

	void mix(int nActiveBus, short *outbufC, int nFramePerBuf, short nOutChannels);
	void zeroBuffers(int nFramePerBuf);

	void setTempo(float t);
	void setBufsize(int bufsize);
	Bus *getBus(int i);
	int busCount();

protected:
	float 				*buffer;
	long				currentBufSize;

	std::vector<Bus*>	bus;
	std::vector<float*>	busBuf;
};


#endif /* BUSMIXER_H_ */
