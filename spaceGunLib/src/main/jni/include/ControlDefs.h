/*
 * ControlDefs.h
 *
 *  Created on: Oct 23, 2013
 *      Author: dak
 */

#ifndef CONTROLDEFS_H_
#define CONTROLDEFS_H_

#include "com_mayaswell_audio_Control.h"
#include "com_mayaswell_spacegun_CControl.h"
#include "com_mayaswell_spacegun_SpaceGun_Global.h"

		constexpr int busSendId(int i) { return (com_mayaswell_spacegun_CControl_SEND_BASE + i); }
		constexpr int bus4SendId(int id) { return (id - com_mayaswell_spacegun_CControl_SEND_BASE); }
// SpaceGun.ENV_PER_PAD *
//		attackT
//		decayT
//		sustainT
//		sustainL
//		releaseT
// 		+ SpaceGun.MAX_ENV_TGT * target
		constexpr int envParam4Id(int i) { return (i-com_mayaswell_spacegun_CControl_ENV_BASE)%(com_mayaswell_spacegun_SpaceGun_Global_MAX_ENV_TGT+5); }
		constexpr int env4Id(int i) { return (i-com_mayaswell_spacegun_CControl_ENV_BASE)/(com_mayaswell_spacegun_SpaceGun_Global_MAX_ENV_TGT+5); }
		constexpr int envIdBase(int i) { return com_mayaswell_spacegun_CControl_ENV_BASE+i*(com_mayaswell_spacegun_SpaceGun_Global_MAX_ENV_TGT+5); }
		constexpr int envAttackTimeId(int i) { return 0+envIdBase(i); }
		constexpr int envDecayTimeId(int i) { return 1+envIdBase(i); }
		constexpr int envSustainTimeId(int i) { return 2+envIdBase(i); }
		constexpr int envSustainLevelId(int i) { return 3+envIdBase(i); }
		constexpr int envReleaseTimeId(int i) { return 4+envIdBase(i); }
		constexpr int envTargetDepthId(int i, int j) { return j+5+envIdBase(i); }
// SpaceGun.LFO_PER_PAD *
//		rate
// 		+ SpaceGun.MAX_LFO_TGT * target
		constexpr int lfoParam4Id(int i) { return (i-envIdBase(com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD))%(com_mayaswell_spacegun_SpaceGun_Global_MAX_LFO_TGT+2); }
		constexpr int lfo4Id(int i) { return (i-envIdBase(com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD))/(com_mayaswell_spacegun_SpaceGun_Global_MAX_LFO_TGT+2); }
		constexpr int lfoIdBase(int i) { return envIdBase(com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD)+i*(com_mayaswell_spacegun_SpaceGun_Global_MAX_LFO_TGT+2); }
		constexpr int lfoRateId(int i) { return 0+lfoIdBase(i); }
		constexpr int lfoPhaseId(int i) { return 1+lfoIdBase(i); }
		constexpr int lfoTargetDepthId(int i, int j) { return j+2+lfoIdBase(i); }

		constexpr int fxIdBase(int i) { return lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD)+i*(com_mayaswell_spacegun_SpaceGun_Global_MAX_FX_PARAM+1); }
		constexpr int fxEnableId(int i) { return 0+fxIdBase(i); }
		constexpr int fxParamId(int i, int j) { return 1+j+fxIdBase(i); }
		constexpr int fxParam4Id(int i) { return ((i-lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD))%(com_mayaswell_spacegun_SpaceGun_Global_MAX_FX_PARAM+1)); }
		constexpr int fx4Id(int i) { return (i-lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD))/(com_mayaswell_spacegun_SpaceGun_Global_MAX_FX_PARAM+1); }

	constexpr bool isEnvControl(int typeCode) {
			return typeCode >= envIdBase(0) && typeCode < envIdBase(com_mayaswell_spacegun_SpaceGun_Global_ENV_PER_PAD);
		}
		constexpr bool isLFOControl(int typeCode) {
			return typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD);
		}

#endif /* CONTROLDEFS_H_ */
