package com.mayaswell.spacegun;

import android.os.Parcel;
import android.os.Parcelable;

public class Send implements Parcelable
{
	protected static Bus.Host busHost = null;

	protected Send(Parcel in) {
		gain = in.readFloat();
		mute = in.readByte() != 0;
		int busid = in.readInt();
		bus = (busHost != null? busHost.busFor(busid): null);
	}

	public static final Creator<Send> CREATOR = new Creator<Send>() {
		@Override
		public Send createFromParcel(Parcel in) {
			return new Send(in);
		}

		@Override
		public Send[] newArray(int size) {
			return new Send[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeFloat(gain);
		parcel.writeByte((byte) (mute ? 1 : 0));
		parcel.writeInt(bus.getBusId());
	}

	public interface Host {
		Send send4Bus(Bus b);
		int nActiveBus();
	}
	protected Bus bus = null;
	protected float gain = 1.0f;
	protected boolean mute = false;
	
	public Send() {
		this(null, 1.0f, false);
	}
	
	public Send(Bus b, float g) {
		this(b, g, false);
	}

	public Send(Bus b, float g, boolean m)
	{
		bus = b;
		gain = g;
		mute = m;
	}
	
	public float getGain()
	{
		return gain;
	}
	
	public boolean  getMute()
	{
		return mute;
	}
	
	public Bus getBus()
	{
		return bus;
	}
	
	public void setGain(float g)
	{
		gain = g;
	}
	
	public void setMute(boolean m)
	{
		mute = m;
	}
	
	public void setBus(Bus g)
	{
		bus = g;
	}
	
	public Send clone()
	{
		return new Send(bus, gain, mute);
	}

	public void set(float d, boolean b) {
		gain = d;
		mute = b;
	}
}

