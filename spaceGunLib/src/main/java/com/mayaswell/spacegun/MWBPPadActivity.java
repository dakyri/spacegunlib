package com.mayaswell.spacegun;

import android.widget.ArrayAdapter;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.PadMode;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.util.AbstractBank;
import com.mayaswell.util.AbstractPatch;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.widget.MenuData;

/**
 *  subclass of MWBPActivity that deals with PadSample and PadSampleState elements
 * @author dak
 *
 * @param <B>
 * @param <P>
 * @param <C>
 */
public abstract class MWBPPadActivity<B extends AbstractBank<P, C>, P extends AbstractPatch, C extends Control>
		extends MWBPActivity<B, P, C> implements SamplePlayer.Manager {

	public MWBPPadActivity(String fe) {
		super(fe);
	}

	public abstract PadSample getPadSample(int i); 
	public abstract int countPadSamples();
//	public abstract void startPadSample(PadSample p, boolean fromSync);
//	public abstract void stopPadSample(PadSample p);
	public abstract float getSampleRate();

	public abstract PadSample getSelectedPad();
	public abstract ArrayAdapter<MenuData<PadMode>> getPadModeAdapter();

	public abstract float getTempo();

	public abstract boolean getShowSampleTimeBeats();
	public abstract void setShowSampleTimeBeats(boolean b);

	public abstract BusMixer getBusMixer();

	public abstract PadSample padForId(int parseInt);
}
