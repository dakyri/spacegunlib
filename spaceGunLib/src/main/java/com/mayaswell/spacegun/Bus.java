package com.mayaswell.spacegun;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.ControlsAdapter.ControlsFilter;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;

import android.content.Context;
import android.util.Log;

public class Bus implements Controllable
{
	public interface Host {
		Bus busFor(int id);
	}

	private static native long allocBus(long bmxPointer, int id, int nFX, int nFXPar, int nLfo, int nLfoTgt);
	private static native void deleteBus(long busptr);
	
	private static native boolean setBusLFO(long ptr, int which, byte wf, byte reset, boolean lock, float rate, float phs);
	private static native boolean setBusNLFO(long ptr, int n);
	private static native boolean setBusNLFOTgt(long ptr, int i, int n);
	private static native boolean setBusLFOTgt(long ptr, int i, int j, int cc, float amt);
	private static native boolean setBusLFOTgtAmt(long ptr, int i, int j, float amt);
	private static native boolean setBusLFOReset(long ptr, int i, byte rst);
	private static native boolean setBusLFOLock(long ptr, int i, boolean lock);
	private static native boolean setBusLFOWave(long ptr, int i, byte wf);
	private static native boolean setBusLFORate(long ptr, int i, float r);
	private static native boolean setBusLFOPhase(long ptr, int i, float p);
	
	private static native boolean setBusFXType(long ptr, int xi, int fxId, int np, float [] p);
	private static native boolean setBusFXEnable(long ptr, int xi, boolean p);
	private static native boolean setBusFXParam(long ptr, int xi, int pi, float p);
	private static native boolean setBusFXParams(long ptr, int xi, int np, float [] p);

	private int busId = 0;
	public BusState state = null;
	protected ControlsAdapter interfaceTgtAdapter = null;
	protected ControlsAdapter lfoTgtAdapter = null;
//	protected ArrayList<FXState> fx = null;

	protected long busPointer = 0;
	protected int nFX;
	protected int nFXPar;
	protected int nLFO;
	protected int nLFOTgt;
	
	public Bus(int id, long bmxPointer, int nFX, int nFXPar, int nLFO, int nLFOTgt)
	{
		setBusId(id);
		state = new BusState(id);
		this.nFX = nFX;
		this.nFXPar = nFXPar;
		this.nLFO = nLFO;
		this.nLFOTgt = nLFOTgt;
//		fx = new ArrayList<FXState>();
		Log.d("create", "allocating the bus "+id);
		
		busPointer  = allocBus(bmxPointer, id, nFX, nFXPar, nLFO, nLFOTgt);
		
		Log.d("create", "allocating the bus is all ok!!!"+id);
	}

	public String toString()
	{
		return "Bus "+Integer.toString(getBusId()+1);
	}
	
	public BusState getState()
	{
		return state;
	}
	/**
	 * @param bs
	 */
	public void setState(BusState bs)
	{
		if (bs == null) {
			setNFX(0);
			setNLFO(0);
			setFXList(null);
		} else {
			state = bs;
			bs.id = getBusId();
			setFXList(bs.fx);
			setLFOList(bs.lfo);
		}
		if (interfaceTgtAdapter != null) interfaceTgtAdapter.refilter();
		if (lfoTgtAdapter != null) lfoTgtAdapter.refilter();

	}
	/*******************************
	 * Hooks for FX
	 *******************************/
	public boolean setNFX(int n)
	{
		return false;
	}
	
	public boolean setFXList(ArrayList<FXState> fxi)
	{
//		setCPadNLFO(cPadPointer, lfo.size());
		int i=0;
		if (fxi != null) {
			for (FXState f: fxi) {
				if (f.getFx() != null) {
					int j=0;
					float [] param = new float[f.getFx().params().size()];
					for (float pi: f.getParams()) {
						if (j > param.length) {
							break;
						}
						param[j++] = pi;
					}
					setBusFXType(busPointer, i, f.getFx().getId(), param.length, param);
					setBusFXEnable(busPointer, i, f.getEnable());
				} else {
					Log.d("bus", "fx list get fx null");
				}
				i++;
			}
		}
		for (; i<nFX; i++) {
			setBusFXType(busPointer, i, FX.UNITY, 0, null);
			setBusFXEnable(busPointer, i, false);
			state.setFXstate(i, FX.unityFX);
			state.setFXenable(i, i==0);
		}
		if (interfaceTgtAdapter != null) interfaceTgtAdapter.refilter();
		return true;
	}
	
	public boolean setFX(int xi, FX f)
	{
		if (xi<0 || state == null) {
			return false;
		}
		boolean b = state.setFXstate(xi, f);
		if (b) {
			float [] param = new float[f.params().size()];
			int i=0;
			for (FXParam v: f.params()) {
				param[i++] = v.getCenterValue();
			}
			setBusFXType(busPointer, xi, f.getId(), param.length, param);
			if (interfaceTgtAdapter != null) interfaceTgtAdapter.refilter();
		}
		return b;
	}
	
	public boolean setFXenable(int xi, boolean enable)
	{
		if (xi<0 || state == null) {
			return false;
		}
		boolean b = state.setFXenable(xi, enable);
		setBusFXEnable(busPointer, xi, enable);
		return b;
	}
	
	public boolean setFXparam(int xi, int yi, float p)
	{
		if (xi<0 || state == null) {
			return false;
		}
//		Log.d("bus", String.format("set fx %d %d %g", xi, yi, p));
		boolean b = state.setFXparam(xi, yi, p);
		setBusFXParam(busPointer, xi, yi, p);
		return b;
	}
	
	/**
	 * 
	 * @param n
	 * @return
	 */
	public boolean setNLFO(int n)
	{
		return setBusNLFO(busPointer, n);
	}

	public void setLFOList(ArrayList<LFO> lfo)
	{
		setBusNLFO(busPointer, lfo.size());
		int i=0;
		for (LFO l: lfo) {
			if (l.countValidTargets() > 0) {
				setBusLFO(busPointer, i, l.waveform.code(), l.trigger.code(), l.tempoLock, l.rate, l.phase);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null) {
						setBusLFOTgt(busPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setBusNLFOTgt(busPointer, i, j);
				i++;
			}
		}
	}
	
	public boolean setLFORate(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).rate = amt;
		return setBusLFORate(busPointer, xyci, amt);
	}

	public boolean setLFOPhase(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).phase = amt;
		return setBusLFOPhase(busPointer, xyci, amt);
	}

	public boolean setLFOLock(int xyci, boolean amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).tempoLock = amt;
		return setBusLFOLock(busPointer, xyci, amt);
	}

	public boolean setLFOReset(int xyci, ResetMode amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).trigger = amt;
		return setBusLFOReset(busPointer, xyci, amt.code());
	}

	public boolean setLFOWave(int xyci, LFWave amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).waveform = amt;
		return setBusLFOWave(busPointer, xyci, amt.code());
	}
	
	public boolean setNLFOTarget(int xyci, int n)
	{
		return setBusNLFOTgt(busPointer, xyci, n);
	}

	public boolean setLFOTarget(int xyci, int xycj, CControl target, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
			state.lfo.get(xyci).target.get(xycj).target = target;
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setBusLFOTgt(busPointer, xyci, xycj, target!=null?target.ccontrolId():CControl.NOTHING, amt);
	}

	public boolean setLFOTargetAmt(int xyci, int xycj, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setBusLFOTgtAmt(busPointer, xyci, xycj, amt);
	}
	
/***********************************************
 * Controllable implementation
 ***********************************************/

	ControlsFilter inUseModFxFilter = new ControlsFilter() {
		@Override
		public boolean isAvailable(Control c) {
			CControl cc = (CControl) c;
			if (cc.isLFOControl()) {
				/*
				if (state == null) return false;
				ArrayList<LFO> ll = state.lfo;
				int lfo = c.lfo4Id();
				int param = c.lfoParam4Id();
				if (lfo < ll.size()) {
					if (ll.get(lfo).target.size() == 0) return false;
					if (ll.get(lfo).target.size() == 1 && ll.get(lfo).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param == 1) {
						return false;
					} else if (param >= 2) {
						if (param-2 >= ll.get(lfo).target.size()) return false;
						if (ll.get(lfo).target.get(param-2).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}*/
				return false;
			} else if (cc.isFXControl()) {
				if (state == null) return false;
				int fxI = cc.fx4Id();
				int paramI = cc.fxParam4Id()-1; // enable is fx param 1 ... paramI is now [0-5] if it is a valid floating point param
				ArrayList<FXState> fx= state.fx;
				if (fx == null) return false;
				if (fxI < 0 || fxI >= fx.size()) {
					return false;
				}
				FXState f = fx.get(fxI);
				if (paramI < 0 || paramI >= f.countParams()) {
					return false;
				}
				return true;
			}
			return true;
		}
	};

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#setupControlsAdapter(android.content.Context, int, int)
	 */
	@Override
	public void setupControlsAdapter(Context c, int tvrid, int ddrid)
	{
		interfaceTgtAdapter = new ControlsAdapter(c, tvrid);
		interfaceTgtAdapter.setDropDownViewResource(ddrid);
		interfaceTgtAdapter.add(new CControl(CControl.NOTHING));
		/*
		for (int i=0; i<SpaceGun.Global.LFO_PER_PAD; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<SpaceGun.Global.MAX_LFO_TGT; j++) {
//				interfaceTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
*/
		for (int i=0; i<nFX; i++) {
			for (int j=0; j<nFXPar; j++) {
				interfaceTgtAdapter.add(new CControl(CControl.fxParamId(i, j)));
			}
		}

		interfaceTgtAdapter.setFilter(inUseModFxFilter);
		interfaceTgtAdapter.refilter();
		
		lfoTgtAdapter = new ControlsAdapter(c, tvrid);
		lfoTgtAdapter.setDropDownViewResource(ddrid);
		lfoTgtAdapter.add(new CControl(CControl.NOTHING));
		for (int i=0; i<nLFO; i++) {
			lfoTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<nLFOTgt; j++) {
//				lfoTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		lfoTgtAdapter.setFilter(inUseModFxFilter);
		lfoTgtAdapter.refilter();

	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#controllableId()
	 */
	@Override
	public int controllableId()
	{
		return getBusId();
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#controllableType()
	 */
	@Override
	public String controllableType()
	{
		return Controllable.BUS;
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#floatValue(com.mayaswell.spacegun.CControl)
	 */
	@Override
	public float floatValue(Control cc)
	{
		switch (cc.ccontrolId()) {
		/*
		case CControl.GAIN: return state.gain;
		case CControl.PAN: return state.pan;
		case CControl.LOOP_START: {
			return nTotalFrames == 0? 0: (((float)state.loopStart)/nTotalFrames);
		}
		case CControl.LOOP_LENGTH: {
			return nTotalFrames == 0? 0: (((float)state.loopLength)/nTotalFrames);
		}
		case CControl.FLT_CUTOFF: return state.filterFrequency;
		case CControl.FLT_ENVMOD: return state.filterEnvMod;
		case CControl.FLT_RESONANCE: return state.filterResonance;
		*/
		default: {
			if (cc.ccontrolId() >= CControl.fxIdBase(0) && cc.ccontrolId() < CControl.fxIdBase(nFX)) {
				int fx = CControl.fx4Id(cc.ccontrolId());
				int param = CControl.fxParam4Id(cc.ccontrolId());
				if (param == 0) { // enable
				} else {// param(pj-1);
					if (fx<state.fx.size() && state.fx.get(fx).getParams() != null && (param-1) < state.fx.get(fx).getParams().length) { 
						return state.fx.get(fx).param(param-1);
					}
				}
			} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(nLFO)) {
				int lfo = CControl.lfo4Id(cc.ccontrolId());
				int param = CControl.lfoParam4Id(cc.ccontrolId());
				if (param == 0) { // rate
					if (lfo<state.lfo.size()) { return state.lfo.get(lfo).rate; }
				} else if (param == 1) { // phase
					if (lfo<state.lfo.size()) { return state.lfo.get(lfo).phase; } 
				} else {// depth(param-2);
					if (lfo<state.lfo.size() && (param-2) < state.lfo.get(lfo).target.size()) { 
						return state.lfo.get(lfo).target.get(param-2).amount;
					}
				}
			}
		}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#setValue(com.mayaswell.spacegun.CControl, float)
	 */
	@Override
	public void setValue(Control cc, float v)
	{
		switch (cc.ccontrolId()) {
/*
		case CControl.GAIN: {
			setGain(v);
			break;
		}
		case CControl.PAN: {
			setPan(v);
			break;
		}
		case CControl.LOOP_START: {
			setLoopStart((long) (v * nTotalFrames));
			break;
		}
		case CControl.LOOP_LENGTH: {
			setLoopLength((long) (v * nTotalFrames));
			break;
		}
		case CControl.FLT_CUTOFF: {
			setFilterFrequency(v);
			break;
		}
		case CControl.FLT_ENVMOD: {
			setFilterEnvMod(v);
			break;
		}
		case CControl.FLT_RESONANCE: {
			setFilterResonance(v);
			break;
		}
*/
		default:
			if (cc.ccontrolId() >= CControl.fxIdBase(0) && cc.ccontrolId() < CControl.fxIdBase(nFX)) {
				int fxi = CControl.fx4Id(cc.ccontrolId());
				int param = CControl.fxParam4Id(cc.ccontrolId());
				if (param == 0) { // enable
				} else {// param(pj-1);
					ArrayList<FXState> fxa = state.fx;
					if (fxi<fxa.size() && fxa.get(fxi).getParams() != null && (param-1) < fxa.get(fxi).getParams().length) { 
						fxa.get(fxi).param(param-1, v);
						setFXparam(fxi, param-1, v);
					}
				}
			} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(nLFO)) {
				int lfo = CControl.lfo4Id(cc.ccontrolId());
				int param = CControl.lfoParam4Id(cc.ccontrolId());
				if (param == 0) { setLFORate(lfo, v); } // rate
				if (param == 1) { setLFOPhase(lfo, v); } // phase
				else { setLFOTargetAmt(lfo, param-2, v);} // depth(param-2);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.Controllable#abbrevName()
	 */
	@Override
	public String abbrevName()
	{
		return "B"+Integer.toString(getBusId());
	}
	
	@Override
	public ControlsAdapter getInterfaceTgtAdapter()
	{
		return interfaceTgtAdapter;
	}

	public ControlsAdapter getLFOTgtAdapter() {
		return lfoTgtAdapter;
	}
	public long getBusPointer() {
		return busPointer;
	}
	public int getId() {
		return getBusId();
	}
	public int getBusId() {
		return busId;
	}
	public void setBusId(int busId) {
		this.busId = busId;
	}

}
