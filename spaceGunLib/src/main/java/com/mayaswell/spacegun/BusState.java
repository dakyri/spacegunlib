package com.mayaswell.spacegun;

import java.util.ArrayList;

import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.LFO;

import android.util.Log;

public class BusState
{
	public int id = 0;
	public String name = "Bus";
	public ArrayList<LFO> lfo = new ArrayList<LFO>();
	public ArrayList<FXState> fx = new ArrayList<FXState>();
	public float gain = 1;
	public boolean enable = true;
	
	public BusState(int _id)
	{
		setId(_id, null);
	}
	
	public void add(LFO l)
	{
		if (lfo == null) lfo = new ArrayList<LFO>();
		lfo.add(l);
	}
	
	public void add(FXState l)
	{
		if (fx == null) fx = new ArrayList<FXState>();
		fx.add(l);
	}
	
	public BusState clone()
	{
		BusState b = new BusState(id);
		b.gain = gain;
		b.enable = enable;
		b.name = name;
		
		for (FXState f: fx) {
			b.fx.add(f.clone());
		}
		for (LFO l: lfo) {
			b.lfo.add(l.clone());
		}
		
		return b;
	}

	public boolean setFXstate(int xi, FX f)
	{
		Log.d("setFXstate", String.format("%d %s", xi, f!=null?f.toString():""));
		if (f == null) {
			return false;
		}
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		FXState fxs = fx.get(xi);
		if (fxs == null) {
			return false;
		}
		if (fxs.getFx().getId() == f.getId()) {
			return false;
		}
		fxs.setType(f);
		return true;
	}

	public boolean setFXenable(int xi, boolean enable)
	{
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		fx.get(xi).setEnable(enable);
		return true;
	}

	public boolean setFXparam(int xi, int yi, float p)
	{
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		FXState fss = fx.get(xi);
		if (fss.countParams() <= yi) {
			return false;
		}
		fss.param(yi, p);
		return true;
	}

	public void setId(int id2, String name2) {
		id = id2;
		if (name2 != null && !name2.equals("")) {
			name = name2;
		} else {
			name = "Bus "+Integer.toString(id+1);
		}
	}
}
