package com.mayaswell.spacegun;


import android.os.Parcel;
import android.os.Parcelable;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.PadMode;
import com.mayaswell.audio.Sample.Direction;

import java.util.ArrayList;
import java.util.List;

public class PadSampleState extends BusAudioNodeState implements Parcelable {
	public int syncMasterId = -1;

	public long loopStart = 0;
	public long loopLength = 0;
	public int loopCount = 1; // <= 0 infinte loop
	public Direction loopDirection = Direction.FORWARD;
	
	public int filterType = Controllable.Filter.UNITY;
	public float filterFrequency = 0.5f;
	public float filterEnvMod = 0;
	public float filterResonance = 0;
	
	public PadMode padMode = PadMode.NORMAL;
	public String path = "";
	
	public PadSampleState()
	{
	}

	public PadSampleState(Parcel in) {
		name = in.readString();
		gain = in.readFloat();
		pan = in.readFloat();
		tune = in.readFloat();
		loopStart = in.readLong();
		loopLength = in.readLong();
		loopCount = in.readInt();
		loopDirection = Direction.parse(in.readInt());
		filterType = in.readInt();
		filterFrequency = in.readFloat();
		filterEnvMod = in.readFloat();
		filterResonance = in.readFloat();
		path = in.readString();
		padMode = PadMode.parse(in.readString());
		syncMasterId = in.readInt();

		envelope = new ArrayList<Envelope>();
		in.readTypedList(envelope, Envelope.CREATOR);
		lfo = new ArrayList<LFO>();
		in.readTypedList(lfo, LFO.CREATOR);
		send = new ArrayList<Send>();
		in.readTypedList(send, Send.CREATOR);
	}

	public PadSampleState clone()
	{
		PadSampleState pbs = new PadSampleState();
		pbs.name = name;
		pbs.gain = gain;
		pbs.pan = pan;
		pbs.tune = tune;
		pbs.loopStart = loopStart;
		pbs.loopLength = loopLength;
		pbs.loopCount = loopCount;
		pbs.loopDirection = loopDirection;
		pbs.filterType = filterType;
		pbs.filterFrequency = filterFrequency;
		pbs.filterEnvMod = filterEnvMod;
		pbs.filterResonance = filterResonance;
		pbs.path = path;
		pbs.padMode = padMode;
		
		pbs.syncMasterId = syncMasterId;
		
		for (Envelope e: envelope) {
			pbs.envelope.add(e.clone());
		}
		for (LFO l: lfo) {
			pbs.lfo.add(l.clone());
		}
		for (Send l: send) {
			pbs.send.add(l.clone());
		}
		return pbs;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int i) {
		dest.writeString(name);
		dest.writeFloat(gain);
		dest.writeFloat(pan);
		dest.writeFloat(tune);
		dest.writeLong(loopStart);
		dest.writeLong(loopLength);
		dest.writeInt(loopCount);
		dest.writeInt(loopDirection.code());
		dest.writeInt(filterType);
		dest.writeFloat(filterFrequency);
		dest.writeFloat(filterEnvMod);
		dest.writeFloat(filterResonance);
		dest.writeString(path);
		dest.writeString(padMode.toString());
		dest.writeInt(syncMasterId);

		dest.writeTypedList(envelope);
		dest.writeTypedList(lfo);
		dest.writeTypedList(send);
	}

	public static Parcelable.Creator CREATOR = new Parcelable.Creator() {

		@Override
		public PadSampleState createFromParcel(Parcel in) {
			return new PadSampleState(in);
		}

		@Override
		public PadSampleState[] newArray(int i) {
			return new PadSampleState[i];
		}
	};
}
