package com.mayaswell.spacegun;

import java.util.ArrayList;

public class BusAudioNodeState extends AudioNodeState {

	public ArrayList<Send> send = new ArrayList<Send>();

	public BusAudioNodeState() {
		super();
	}

	public void addSend(Send l) {
		if (send == null) send = new ArrayList<Send>();
		send.add(l);
	}

	public float busSend(int busId) {
		for (Send b: send) {
			if (b.bus.getBusId() == busId) {
				return b.gain;
			}
		}
		return 0;
	}

	public boolean busMute(int busId) {
		for (Send b: send) {
			if (b.bus.getBusId() == busId) {
				return b.mute;
			}
		}
		return true;
	}

	public Bus lowestSend() {
		Bus bus = null;
		for (Send b: send) {
			if (bus == null || b.bus.getBusId() <= bus.getBusId()) {
				bus = b.bus;
			}
		}
		return bus;
	}

	public Send send4bus(int busId) {
		for (Send b: send) {
			if (b.bus.getBusId() == busId) {
				return b;
			}
		}
		return null;
	}

	public Send send4Bus(Bus bs) {
		for (Send b: send) {
			if (b.bus == bs) {
				return b;
			}
		}
		return null;
	}

}