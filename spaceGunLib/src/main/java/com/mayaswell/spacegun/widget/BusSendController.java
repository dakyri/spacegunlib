package com.mayaswell.spacegun.widget;

import java.util.ArrayList;

import com.mayaswell.spacegun.Bus;
import com.mayaswell.spacegun.BusState;
import com.mayaswell.spacegunlib.R;
import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.RotaryPotView;
import com.mayaswell.widget.TabWedgeIt.TabWedgeItListener;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class BusSendController extends BaseControl
{
	public interface BusSendListener
	{
		public void onBusSelected(Bus b);
		public void onGainChanged(Bus b, float amt);
		public void onEnableChanged(Bus b, boolean e);
	}

	private BusSendListener listener = null;
	private RotaryPotView pot = null;
	private EditText view = null;
	private boolean notifyOnTextChange = true;
	private Bus bus = null;
	private BusWedgeIt select = null;
	private ToggleButton mute = null;
	
	public BusSendController(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public BusSendController(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public BusSendController(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.sg_bus_send_ctrl, this, true);
		
		select  = (BusWedgeIt)findViewById (R.id.sgTabSelector);
		label = (TextView)findViewById (R.id.sgCtlLabel);
		pot = (RotaryPotView)findViewById (R.id.sgCtlPot);
		view  = (EditText)findViewById (R.id.sgCtlEditText);
		mute  = (ToggleButton)findViewById (R.id.sgCtlToggle);
		
		select.setListener(new TabWedgeItListener() {
			@Override
			public void onTabReselected(int ind, Object d) {
				Bus b = (Bus) d;
				if (b != null) {
					setBus(b);
					if (listener != null) {
						listener.onBusSelected(b);
					}
				}
			}

			@Override
			public void onTabSelected(int ind, Object d) {
				try {
					Bus b = (Bus) d;
					if (b != null) {
						setBus(b);
						if (listener != null) {
							listener.onBusSelected(b);
						}
					}
				} catch (Error e) {
					
				}
			}

			@Override
			public void onTabUnselected(int ind, Object d) {
			}
			
		});
		select.selectTab(0);
				
		pot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListener(v);
			}
		});

		view.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < pot.getMinVal()) v = pot.getMinVal();
						else if (v > pot.getMaxVal()) v = pot.getMaxVal();
						pot.setValue(v);
						if (notifyOnTextChange) {
							notifyListener(v);
						}
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		mute.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (listener != null) {
					listener.onEnableChanged(bus, isChecked);
				}
			}	
		});
	}
	
	public void setBusList(ArrayList<Bus> busses)
	{
		if (busses == null) return;
		int i=0;
		select.clear();
		for (Bus b: busses) {
			select.add(Integer.toString(i+1), i, null, b);
			i++;
		}
		if (busses.size() > 0) {
			setBus(busses.get(0));
		} else {
			Log.d("bus send", "no buses abailable");
		}
	}
	
	public void setSendGain(float v)
	{
		notifyOnTextChange  = false;
		pot.setValue(v);
		setView(v);
		notifyOnTextChange = true;
	}

	public void setsendEnable(boolean m) {
		mute.setChecked(m);
	}

	private void setView(float v)
	{
		view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}

	public void setListener(BusSendListener l )
	{
		listener  = l;
	}

	public void setValueViewWidth(int px)
	{
		LayoutParams params = (LayoutParams) view.getLayoutParams();
		params.width = px;
	}

	public void setLabelWidth(int px)
	{
		LayoutParams params = (LayoutParams) label.getLayoutParams();
		params.width = px;
	}

	public void setValueBounds(float min, float max)
	{
		pot.setValueBounds(min, max);
	}
	
	public void setCenterValue(float cv)
	{
		pot.setCenterValue(cv);
	}
	
	private void notifyListener(float sip)
	{
		if (listener != null && bus != null) {
			listener.onGainChanged(bus, sip);
		}
	}

	private void setBus(Bus b) {
		bus = b;
		BusState s = b.getState();
		if (s != null && s.name != null) {
			label.setText(s.name);
		} else {
			label.setText("");
		}
	}
	
	public Bus getSelectedBus()
	{
		return bus;
	}

	public boolean showingBus(int i) {
		return (bus == null && i== 0) || (bus.controllableId() == i);
	}

	public void setToBus(int i) {
		select.selectTab(i);
//		bus = select.getSelectedBus(0);
	}

	public boolean showingBus(Bus bs) {
		return bs == bus;
	}

	public void refreshDisplayedBusState() {
		if (listener != null) {
			listener.onBusSelected(bus);
		}
	}

}
