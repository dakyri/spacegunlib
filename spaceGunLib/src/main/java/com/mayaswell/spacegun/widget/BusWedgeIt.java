package com.mayaswell.spacegun.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mayaswell.spacegun.Bus;
import com.mayaswell.widget.TabWedgeIt;

public class BusWedgeIt extends TabWedgeIt
{
	public BusWedgeIt(Context context) {
		super(context);
	}
	public BusWedgeIt(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BusWedgeIt(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	/*
	protected void adjustLayout(int ind)
	{
		RelativeLayout.LayoutParams kozParams = null;
		int lmid = -1;
		int umid = -1;
		for (int i=0; i<tabs.size(); i++) {
			if (i%2 == 0) {
				umid = -1;
			}
			TabInfo sxi = tabs.get(i);
			int mid = sxi.button.getId();
			
			kozParams = new RelativeLayout.LayoutParams(buttonWidth, buttonHeight);
			if (lmid < 0) {
				kozParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, umid);				
			} else {
				kozParams.addRule(RelativeLayout.RIGHT_OF, lmid);				
			}
			if (umid < 0) {
				kozParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, umid);				
			} else {
				kozParams.addRule(RelativeLayout.BELOW, umid);				
			}
			sxi.button.setLayoutParams(kozParams);
			if (i%2 == 1) {
				lmid = mid;
			}
			umid = mid;
		}
	}
	*/
	protected void adjustButton(Button newTBut)
	{
		
	}
	
	public Bus getSelectedBus(int i) {
		for (TabInfo bi: tabs) {
			Log.d("bus wedge", "get selected bus"+i+", "+bi.button.isSelected());
			if (bi.button != null && bi.button.isSelected()) {
				i--;
				if (i<0) {
					return (Bus) bi.data;
				}
			}
		}
		return null;
	}

}
