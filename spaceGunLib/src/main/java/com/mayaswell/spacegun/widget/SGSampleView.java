package com.mayaswell.spacegun.widget;

import com.mayaswell.spacegun.PadSample;
import com.mayaswell.widget.SampleView;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class SGSampleView extends SampleView {
	protected boolean rightTouchFirst=false;
	protected float gridRate = 60.0f;
	
	public SGSampleView(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public SGSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public SGSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		setGridMetric(60, 4, 4);
	}
	
	@Override
	protected void onDraw (Canvas canvas)
	{
		if (currentPlayer != null) {
			if (gridWidth > 0 && showGrid) {
				drawGrid(canvas);
			}
			if (pxRs < svW && pxRe > 0) {
				if (pxRs < 0) pxRs = 0;
				if (pxRe > svW) pxRe = svW;
				canvas.drawRect(pxRs, 0, pxRe, svH-scrollerHeight, regionBrush);
			}

			if (sampleBitmap != null) {
				canvas.drawBitmap(sampleBitmap, sampleBitmapSrc, svRect, null);
			}
			if (crsX >= 0 && crsX <= svW) {
				canvas.drawLine(crsX, 0, crsX, svH, cursorBrush);
			}
		}
		scrollerBack.setBounds(scrollerBackRect);	
		scrollerBack.draw(canvas);
		
		scrollerKnob.setBounds(scrollerKnobRect);	
		scrollerKnob.draw(canvas);
	}
	
	@Override
	protected void onTouchDown(MotionEvent e) {
		float px0 = frame2px(regionStart);
		float px1 = frame2px(regionStart+regionLength);
		int x0 = (int) e.getX();
		if (x0 > px1-5) {
			touchOffset = 0;
			rightTouchFirst = true;
		} else {
			rightTouchFirst = false;
			if (x0 > px0 && x0 < px1-5) {
				touchOffset = (int) (x0-px0);
			} else if (x0 <= px0){
				touchOffset = 0;
			}
		}
	}
	
	@Override
	protected void onTouchSelecting(MotionEvent e) {
		long fr0=0;
		long fr1=0;
		boolean hasLength = false;
		boolean hasStart = false;
		if (rightTouchFirst) {
			fr1 = px2frame(e.getX());
			fr0 = regionStart;
			hasLength = true;
		} else {
			fr0 = px2frame(e.getX()-touchOffset);
			hasStart = true;
		}
		if (e.getPointerCount() > 1) {
			if (rightTouchFirst) {
				fr0 = px2frame(e.getX(1));
				hasStart = true;
			} else {
				fr1 = px2frame(e.getX(1));
				hasLength = true;
			}
		}
		if (hasStart) {
			if (fr0 < 0) fr0 = 0;
			if (fr0 != regionStart && listener != null) {
				listener.onRegionStartChanged(fr0);
			}
			setRegionStart(fr0);
		}
		if (hasLength) {
			if (fr1 > fr0) {
				if ((fr1-fr0) != regionLength && listener != null) {
					listener.onRegionLengthChanged(fr1-fr0);
				}
				setRegionLength(fr1-fr0);
			}
		}
		invalidate();
	}
	
	public void setGridMetric(float t, int maj, int min) {
		gridRate = t/60;
		gridMajorSubdivision = maj; // beats per bar
		gridMinorSubdivision = min; // subdivisions per beat
		setGridParameters();
	}
	
	@Override
	public void setGridParameters() {
		if (currentPlayer == null) {
			gridWidth = 0;
			return;
		}
		float secsDisplayed = currentPlayer.proportion2secs(scrollL);
		float beatsDisplayed = secsDisplayed * gridRate;
		float gridsDisplayed = beatsDisplayed * gridMinorSubdivision;
		gridWidth = svW / gridsDisplayed;		
	}
	

}
