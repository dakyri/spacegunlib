package com.mayaswell.spacegun.widget;

import com.mayaswell.audio.SampleFrame;
import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.widget.InfinIntControl;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;

public class SampleFrameControl extends InfinIntControl {
	
	MWBPPadActivity<?,?,?> mwbp;

	public SampleFrameControl(Context context) {
		super(context);
	}

	public SampleFrameControl(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SampleFrameControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setup(Context context, AttributeSet attrs)
	{
		super.setup(context, attrs);
		mwbp = (MWBPPadActivity<?, ?, ?>) context;
		view.setInputType(InputType.TYPE_CLASS_TEXT);
	}
	
	@Override
	protected long string2Long(String s)
	{
		if (mwbp.getShowSampleTimeBeats()) {
			float t = mwbp.getTempo();
			float v = Float.parseFloat(s);
			float f = (60*((float)mwbp.getSampleRate())*v)/t;
			return (long)f;
		}
		return SampleFrame.parseSampleFrame(s);
	}

	@Override
	protected String long2String(long v)
	{
		if (mwbp.getShowSampleTimeBeats()) {
			float t = mwbp.getTempo();
			float b = t * ((float)v) / (60*((float)mwbp.getSampleRate()));
			return Float.toString(b);
		}
		return SampleFrame.toString(v);
	}

	public void setBeatMode(boolean b)
	{
		setValue(getValue());
		if (b) {
			view.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
		} else {
			view.setInputType(InputType.TYPE_CLASS_TEXT);
		}
	}
}
