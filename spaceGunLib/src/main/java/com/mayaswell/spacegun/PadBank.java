package com.mayaswell.spacegun;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;

import com.mayaswell.audio.Controllable.Filter;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.PadMode;
import com.mayaswell.audio.Sample;
import com.mayaswell.util.AbstractBank;
import com.mayaswell.util.AbstractPatch;

public abstract class PadBank<T extends AbstractPatch, C extends Control> extends AbstractBank<T,C> {

	public PadBank() {
		super();
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected PadSampleState doPad(XmlPullParser xpp, MWBPPadActivity<?,?,?> a) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("pad")) return null;
		
		PadSampleState pss = new PadSampleState();
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				pss.name = v;
			} else if (n.equals("src")) {
				pss.path = v;
			} else if (n.equals("sync")) {
				pss.syncMasterId = Integer.parseInt(v);
			} else if (n.equals("mode")) {
				pss.padMode = PadMode.parse(v);
			} else if (n.equals("loopCount")) {
				pss.loopCount = Integer.parseInt(v);
			} else if (n.equals("gain")) {
				pss.gain = Float.parseFloat(v);
			} else if (n.equals("pan")) {
				pss.pan = Float.parseFloat(v);
			} else if (n.equals("tune")) {
				pss.tune = Float.parseFloat(v);
			} else if (n.equals("loopStart")) {
				pss.loopStart = Long.parseLong(v);
			} else if (n.equals("loopLength")) {
				pss.loopLength = Long.parseLong(v);
			} else if (n.equals("loopDirection")) {
				pss.loopDirection = Sample.Direction.parse(v);
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return pss;
		}
		int eventType = xpp.next();//xpp.getEventType();
		Filter fs = null;
		LFO lfo = null;
		Envelope env = null;
		Send send = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((fs=doFilter(xpp)) != null) {
					pss.filterType = fs.getId();
					pss.filterFrequency = fs.frequency;
					pss.filterEnvMod = fs.envMod;
					pss.filterResonance = fs.resonance;
				} else if ((lfo=doLFO(xpp)) != null) {
					pss.addLFO(lfo);
				} else if ((env=doEnvelope(xpp)) != null) {
					pss.addEnvelope(env);
				} else if ((send=doSend(xpp)) != null) {
					pss.addSend(send);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("pad")) { // tag consumed by doPatch()
					return pss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pad'");
	}

	private Send doSend(XmlPullParser xpp) {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("send")) return null;
		
		Send send = new Send();
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("busId")){
				int id = Integer.parseInt(v);
				Bus b = BusMixer.getBus(id);
				if (b == null) {
					Log.d("load bank", String.format("bus mixer can't find %d", id));
					return null;
				}
				send.bus = b;
			} else if (n.equals("gain")){
				send.gain = Float.parseFloat(v);
			} else if (n.equals("mute")){
				send.mute = Boolean.parseBoolean(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in lfo element, "+n);
			}
		}
		return send;
	}

	protected BusState doBus(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("bus")) return null;
		
		BusState bss = new BusState(0);
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				bss.name = v;
			} else if (n.equals("busId")) {
				bss.id = Integer.parseInt(v);
			} else if (n.equals("gain")) {
				bss.gain = Float.parseFloat(v);
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		bss.setId(bss.id, bss.name);
		if (xpp.isEmptyElementTag()) {
			return bss;
		}
		int eventType = xpp.next();//xpp.getEventType();
		FXState fs = null;
		LFO lfo = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((fs=doFX(xpp)) != null) {
					bss.add(fs);
				} else if ((lfo=doLFO(xpp)) != null) {
					bss.add(lfo);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("bus")) { // tag consumed by doPatch()
					return bss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'bus'");
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected Filter doFilter(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("filter")) return null;
		
		Filter f = new Filter();
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("type")){
				Filter mf = PadSample.filter4name(v);
				if (mf == null) {
					return null;
				}
				f.setType(mf);
			} else if (n.equals("frequency")){
				f.setFrequency(Float.parseFloat(v));
			} else if (n.equals("resonance")){
				f.setResonance(Float.parseFloat(v));
			} else if (n.equals("envMod")){
				f.setEnvMod(Float.parseFloat(v));
			} else {
//				throw new XmlPullParserException("Unknown attribute in filter element, "+n);
			}
		}
		
		if (xpp.isEmptyElementTag()) {
			return f;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("filter")) { // tag consumed in loop of doBank()
					return f;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'filter'");
	}

	protected void putBus(XmlSerializer xml, BusState b) throws IOException {
		if (b.fx.size() > 0 || b.lfo.size() > 0 || b.gain != 1.0) {
			Log.d("save", String.format("bus %s", b.name));
			xs(xml, "bus");
			la(xml, "busId", b.id);
			sa(xml, "name", b.name);
			fa(xml, "gain", b.gain);
			for (FXState f:b.fx) {
				putFX(xml, f);
			}
			for (LFO l:b.lfo) {
				putLFO(xml, l);
			}
			xe(xml, "bus");
		}
	}

	protected void putPad(XmlSerializer xml, PadSampleState pss) throws IOException {
		xs(xml, "pad");
		if (pss.name != null) sa(xml, "name", pss.name);
		if (pss.path != null) sa(xml, "src", pss.path);
		if (pss.syncMasterId >= 0) la(xml, "sync", pss.syncMasterId);
		sa(xml, "mode", pss.padMode.toString());
		la(xml, "loopCount", pss.loopCount);
		fa(xml, "gain", pss.gain);
		fa(xml, "pan", pss.pan);
		fa(xml, "tune", pss.tune);
		la(xml, "loopStart", pss.loopStart);
		la(xml, "loopLength", pss.loopLength);
		sa(xml, "loopDirection", pss.loopDirection.toString());
		if (pss.filterType != Filter.UNITY) {
			Filter f = PadSample.filter4type(pss.filterType);
			if (f != null) {
				xs(xml, "filter");
				sa(xml, "type", f.getSaveName());
				fa(xml, "frequency", pss.filterFrequency);
				fa(xml, "resonance", pss.filterResonance);
				fa(xml, "envMod", pss.filterEnvMod);
				xe(xml, "filter");
			}
		}
		for (Send psss: pss.send) {
			putSend(xml, psss);
		}
		for (LFO psslf: pss.lfo) {
			putLFO(xml, psslf);
		}
		for (Envelope psse: pss.envelope) {
			putEnv(xml, psse);
		}
		xe(xml, "pad");
	}

	private void putSend(XmlSerializer xml, Send psss)
			throws IllegalArgumentException, IllegalStateException, IOException {
				if (psss != null && psss.bus != null) {
					xs(xml, "send");
					la(xml, "busId", psss.bus.getBusId());
					fa(xml, "gain", psss.gain);
					ba(xml, "mute", psss.mute);
					xe(xml, "send");
				} else {
					Log.d("save", "null send or bus");
				}
			}

}