package com.mayaswell.spacegun;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Path;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleChunkInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.ControlsAdapter.ControlsFilter;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.audio.SamplePlayer;

import rx.Observable;
import rx.Subscriber;

public class PadSample extends SamplePlayer implements Controllable, Envelope.Host, LFO.Host, Send.Host  {
	public class Info  {
		public Info(PadSample p) {
			padSample = p;
			if (p != null) {
				name = p.toString();
			} else {
				name = "";
			}
		}
		public String toString() {
			return name;
		}
		public PadSample padSample;
		private String name;
		
	}

	public interface Listener {
		void playStarted(PadSample p);
		void playComplete(PadSample padSample);
		void setSampleData(PadSample padSample);
	}
	private Listener listener = null;
	public void setListener(Listener l) {
		listener = l;
	}

	public PadSampleState state = new PadSampleState();
	
	public int id = 0;
	protected String name = ""; // the fixed name, visible in lists of targets for controllers
	
	protected long cPadPointer = 0;

	private Manager manager = null;
	
	/*
	 * native hooks
	 *  static load library is here, though it is referenced also in BusMixer. PadSample global init should be the first
	 *  function called ... that will also force linkage of libcpad.so
	 */
	static {
		System.loadLibrary("cpad");
		Log.d("now", ">>>> System load");
	}

	public static native void globalCPadInit();
	public static native void globalCPadCleanup();

	private static native long allocCPad(int id, int nEnv, int nEnvTgt, int nLfo, int nLfoTgt);
	private static native void deleteCPad(long ptr);
	private static native void setCPadBufsize(long ptr, int bufsize);
	
	protected static native void setCPadSampleInfo(long ptr, long nframe, short nc);
	protected static native void setCPadGain(long ptr, float gain);
	protected static native void setCPadPan(long ptr, float pan);
	protected static native void setCPadTune(long ptr, float tune);
	
	private static native void setCPadFltType(long ptr, int t);
	private static native void setCPadFltFrequency(long ptr, float f);
	private static native void setCPadFltEnvMod(long ptr, float f);
	private static native void setCPadFltResonance(long ptr, float r);
	private static native void setCPadFlt(long ptr, int t, float f, float r, float e);
	
	private static native boolean setCPadNSend(long ptr, int n);
	private static native boolean setCPadSend(long ptr, long busp, float f, boolean m);
	private static native boolean setCPadSendGain(long ptr, long busp, float f);
	private static native boolean setCPadSendMute(long ptr, long busp, boolean m);
	
	private static native void setCPadTempo(long ptr, float tempo);
	private static native boolean setCPadEnv(long ptr, int which, byte reset, boolean lock, float attackT, float decayT, float sustainT, float sustainL, float releaseT);
	private static native boolean setCPadNEnv(long ptr, int n);
	private static native boolean setCPadNEnvTgt(long ptr, int i, int m);
	private static native boolean setCPadEnvTgt(long ptr, int i, int j, int cc, float amt);
	private static native boolean setCPadEnvTgtAmt(long ptr, int i, int j, float amt);
	private static native boolean setCPadEnvReset(long ptr, int i, byte rst);
	private static native boolean setCPadEnvLock(long ptr, int i, boolean lock);
	private static native boolean setCPadEnvAtt(long ptr, int i, float t);
	private static native boolean setCPadEnvDec(long ptr, int i, float t);
	private static native boolean setCPadEnvSus(long ptr, int i, float t);
	private static native boolean setCPadEnvSusLevel(long ptr, int i, float l);
	private static native boolean setCPadEnvRel(long ptr, int i, float t);
	private static native boolean setCPadLFO(long ptr, int which, byte wf, byte reset, boolean lock, float rate, float phs);
	private static native boolean setCPadNLFO(long ptr, int n);
	private static native boolean setCPadNLFOTgt(long ptr, int i, int n);
	private static native boolean setCPadLFOTgt(long ptr, int i, int j, int cc, float amt);
	private static native boolean setCPadLFOTgtAmt(long ptr, int i, int j, float amt);
	private static native boolean setCPadLFOReset(long ptr, int i, byte rst);
	private static native boolean setCPadLFOLock(long ptr, int i, boolean lock);
	private static native boolean setCPadLFOWave(long ptr, int i, byte wf);
	private static native boolean setCPadLFORate(long ptr, int i, float r);
	private static native boolean setCPadLFOPhase(long ptr, int i, float p);
	
	protected static native void fireCPad(long ptr);
	protected static native void stopCPad(long ptr);
	protected static native void reloopCPad(long ptr);
	protected static native int playChunkC(long ptr,
			float []buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen);
	protected static native int playChunkCumulativeC(long ptr,
			float []buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen);
	protected static native int playChunkMixer1(long ptr, long mxPtr,
			int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen, int direction);
	protected static native int playChunkMixer(long ptr, long mxPtr,
			int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen, float nextL, float nextR, int direction);
	
	protected static native int getNextDataFrame(long ptr);
	protected static native int activeBusCount(long ptr);
	
	/**
	 * @param name 
	 * 
	 */
	public PadSample(Manager a, String name, int id)
	{
		super();
		this.name = name;
		this.id = id;
		manager = a;
		cPadPointer = allocCPad(id, CControl.maxEnv, CControl.maxEnvTgt, CControl.maxLFO, CControl.maxLFOTgt);
	}

	public void cleanup() {
		if (cPadPointer != 0) {
			deleteCPad(cPadPointer);
			cPadPointer = 0;
		}
	}
	
	/**
	 * @param frame
	 * @return
	 */
	public boolean seekFrame(long frame)
	{
		return true;
	}
	
	/**
	 * @return
	 */
	public PadSampleState getState()
	{
		return state;
	}
	
	public long getFrameCount()
	{
		return nTotalFrames;
	}

	public long getRegionStart()
	{
		return state != null? state.loopStart:0;
	}
	
	public long getRegionLength()
	{
		return state != null? state.loopLength:0;
	}
	
	/**
	 * @return current sample position as a fraction between 0 and 1
	 */
	public float getCurrentPosition()
	{
//		Log.d("position", "getting current position "+currentLoopFrame+", "+(state!=null?state.loopLength:"<>"));
		if (!isPlayable()) {
			return 0;
		}
		if (state == null) {
			return 0;
		}
		if (state.loopLength == 0) {
			return 1;
		}
		if (currentLoopFrame < 0) {
			return 0;
		}
		if (currentLoopFrame > state.loopLength) {
			return 1;
		}
		return ((float)currentLoopFrame)/state.loopLength;
	}

	
	/**
	 * the old way of doing things
	 * @param buff
	 * @param nFrames
	 * @param nOutChannels
	 */
	public int play(float []buff, int nFrames, short nOutChannels)
	{
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
			return 0;
		}
		if (!isPlaying()) {
			return 0;
		}
		
		int nFramesOut = 0;
		int ll = ((int)state.loopLength);
		if (ll < 2) ll = 2;
		int sf = ((int)state.loopStart);
		int buffInd = 0;
		while (nFramesOut < nFrames) {
			int nFramesRemaining = nFrames-nFramesOut;
			int nIterFrames = ( (currentLoopFrame + nFramesRemaining > ll)?(ll - currentLoopFrame): nFramesRemaining);
			if (nIterFrames > 0) {
				int currentDataFrame = currentLoopFrame + sf;
				int scid = 0;
				int scistart = 0;
				int scilength = 0;
				float [] scidata = null;
				SampleChunkInfo sci = csf.getChunkFor(currentDataFrame);
				if (sci != null) {
					scid = sci.id;
					scilength = sci.nFrames;
					scidata = sci.data;
					scistart = sci.startFrame;
				}
				nIterFrames = playChunkJ(buff, buffInd, nIterFrames, nOutChannels, currentDataFrame, scidata, scistart, scilength);
				if (scid > 0) {
					csf.requestChunk(scid, 1); // will already be loaded, but we'll make sure it's there
					csf.requestChunk(scid+1, 1);
					csf.requestChunk(scid+2, 1);
				}
				buffInd += nOutChannels*nIterFrames;
				currentLoopFrame += nIterFrames;
	 			nFramesOut += nIterFrames;
			} else {
				if (!isPlaying()) {
					break;
				}
			}
			if (currentLoopFrame >= ll) {
				int lsb = Bufferator.chunk4Frame((int) state.loopStart);
				csf.requestChunk(lsb, 1);
				csf.requestChunk(lsb+1, 1);
				if (state.loopCount > 0 && ++currentLoopCount >= state.loopCount) {
					stop(true);
				} else {
					currentLoopFrame = 0;
					reloopCPad(cPadPointer);
					/* TODO restart any pad that is in our sync list and is running */
				}
			}
		}
		/* for the moment, zero the buffer if we fall through ... when we add fx
		 * perhaps we'll need to put any empty bits out through the playChunk routine
		 * where dsp stuff is handled
		 */
		for (int i=buffInd; i<buff.length; i++) {
			buff[i] = 0;
		}
		
		return nFrames;
	}
	
	/**
	 * part of the old way of doing things
	 * @param buff
	 * @param buffInd
	 * @param nIterFrames
	 * @param nOutChannels
	 * @param currentDataFrame
	 * @param chunkData
	 * @param chunkStart
	 * @param chunkLen
	 * @return
	 */
	public int playChunkJ(
			float []buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen)
	{
		int nIterDataFrames = ((currentDataFrame + nIterFrames > nTotalFrames)?(nTotalFrames-currentDataFrame):nIterFrames);
		if (nIterDataFrames < 0) nIterDataFrames = 0;
		int coff = 0;
		if (chunkLen == 0) {
			coff = 0;
			nIterDataFrames = 0;
		} else if (nIterDataFrames > 0) {
			coff = currentDataFrame-chunkStart;
			if (coff < 0) {
				coff = 0;
			}
			if (coff + nIterDataFrames > chunkLen) {
				nIterDataFrames = chunkLen - coff;
				nIterFrames = nIterDataFrames;
			}
			coff = coff*nChannels;
		}
		int buffBnd;
		
		if (nOutChannels == 2) {
			float rAmp=(state.gain)*(1+state.pan)/2;
			float lAmp=(state.gain)*(1-state.pan)/2;
			buffBnd = buffInd+2*nIterDataFrames;
			if (nChannels == 2) {
				for (int i=coff; buffInd < buffBnd; i+=2, buffInd+=2) {
					buff[buffInd] = (float) (lAmp * chunkData[i]);
					buff[buffInd+1] = (float) (rAmp * chunkData[i+1]);
				}
			} else if (nChannels == 1) {
				for (int i=coff; buffInd < buffBnd; i++, buffInd+=2) {
					buff[buffInd] = (float) (lAmp * chunkData[i]);
					buff[buffInd+1] = (float) (rAmp * chunkData[i]);
				}
			}
			buffBnd += 2*(nIterFrames-nIterDataFrames);
			for (;buffInd<buffBnd; buffInd++) {
				buff[buffInd] = 0;
			}
		} else if (nOutChannels == 1) { // dull, dull, dull
			float amp = state.gain;
			buffBnd = buffInd+nIterDataFrames;
			if (nChannels == 2) {
				amp = amp/2;
				for (int i=coff; buffInd < buffBnd; i+=2,buffInd++) {
					buff[buffInd] = (float) (amp * (chunkData[i]+chunkData[i+1]));
				}
			} else if (nChannels == 1) {
				for (int i=coff; buffInd < buffBnd; i++,buffInd++) {
					buff[buffInd] = (float) (amp * chunkData[i]);
				}
			}
			buffBnd += (nIterFrames-nIterDataFrames);
			for (;buffInd<buffBnd; buffInd++) {
				buff[buffInd] = 0;
			}
		} else {
			// we would be exceedingly ambitious for a phone ;D
		}
		return nIterFrames;
	}
	
	/**
	 * mixes nFrames of nChannels to the given busMixer, returning the number of frames handled
	 * @param busMixPointer
	 * @param nFrames
	 * @param nOutChannels
	 */
	public int playCumulative(long busMixPointer, int nFrames, short nOutChannels)
	{
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
//			Log.d("player", "bail with null chunk");
			return 0;
		}
		if (!isPlaying()) {
//			Log.d("player", "tried to play, but not really playing");
			return 0;
		}
		
		int nFramesOut = 0;
		int ll = ((int)state.loopLength);
		if (ll < 2) ll = 2;
		int sf = ((int)state.loopStart);
		int buffInd = 0;
//		Log.d("player", String.format("pad %d to play %d %d", id, nFrames, nOutChannels));
		while (nFramesOut < nFrames) {
			int nFramesRemaining = nFrames-nFramesOut;
			int nIterFrames = (currentDirection >= 0)?
						( (nFramesRemaining > ll-currentLoopFrame)?(ll - currentLoopFrame): nFramesRemaining):
						( (nFramesRemaining > currentLoopFrame)?currentLoopFrame: nFramesRemaining);
//			Log.d("player", String.format("pad %d loop frame %d dir %d to make %d %d %d %d tune is %g", id, currentLoopFrame, currentDirection, nFrames, nFramesOut, nFramesRemaining, nIterFrames, state.tune));
			if (nIterFrames > 0) {
				int currentDataFrame = currentLoopFrame + sf;
				int scid = 0;
				int scistart = 0;
				int scilength = 0;
				float [] scidata = null;
				SampleChunkInfo sci = csf.getChunkFor(currentDataFrame);
				float nextL = 0;
				float nextR = 0;
				if (sci != null) {
					scid = sci.id;
					scilength = sci.nFrames;
					scidata = sci.data;
					scistart = sci.startFrame;
					if (state.tune != 0) { // setup buffer endpoints if we're interpolating TODO or we are tuning it with lfo!!!!
						if (currentDirection >= 0) {
							SampleChunkInfo scnxt = null;
							if (sci.nextChunkStartFrame() >= sf + ll) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid+1);
							}
							if (scnxt != null) {
								nextL = scnxt.data[0];
								if (nChannels > 1) {
									nextR = scnxt.data[1];
								}
							}
						} else { // backwards .
							SampleChunkInfo scnxt = null;
							if (sci.prevChunkEndFrame() < sf) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid-1);
							}
							if (scnxt != null) {
								if (nChannels > 1) {
									nextL = scnxt.data[scnxt.nFrames-2];
									nextR = scnxt.data[scnxt.nFrames-1];
								} else {
									nextL = scnxt.data[scnxt.nFrames-1];
								}
							}
						}
					}
//					Log.d("player", String.format("pad %d got %d chunk %d len %d @ %d data[0] %g path %d %s", id, currentDataFrame, scid, scilength, scistart, scidata[0], csf.refCount, csf.path));
				} else {
					scid = Bufferator.chunk4Frame(currentDataFrame);
//					Log.d("player", String.format("pad %d failed to find %d chunk %d path %s", id, currentDataFrame, scid, csf.path));
				}
				int nIterOutFrames = playChunkMixer(cPadPointer, busMixPointer, buffInd, nIterFrames, nOutChannels, currentDataFrame,
						scidata, scistart, scilength, nextL, nextR, currentDirection);
				if (scid >= 0) {
					csf.requestChunk(scid, 1); // will already be loaded, but we'll make sure it's there
					if (currentDirection >= 0) {
						csf.requestChunk(scid+1, 1);
						csf.requestChunk(scid+2, 1);
					} else {
						csf.requestChunk(scid-1, 1);
						csf.requestChunk(scid-2, 1);
					}
				}
				buffInd += nOutChannels*nIterOutFrames;
				currentLoopFrame = getNextDataFrame(cPadPointer)-sf;
	 			nFramesOut += nIterOutFrames;
//				Log.d("player", String.format("pad %d played %d %d requested %d loop %d data %d end %d", id, nIterOutFrames, nFramesOut, nIterFrames, currentLoopFrame, getNextDataFrame(cPadPointer), ll));
			} else {
				if (!isPlaying()) {
					break;
				}
			}
			if (currentLoopFrame >= ll) {
				if (currentDirection >= 0) {
					loopEnd();
				} else {
					currentLoopFrame = ll;
				}
			} else if (currentLoopFrame <= 0){
				if (currentDirection >= 0) {
					currentLoopFrame = 0;
				} else {
					loopEnd();
				}
			}
		}
		
		return nFrames;
	}
	
	protected void loopEnd() {
//		Log.d("player", "end of the loop "+currentLoopFrame + " ... "+ state.loopDirection);
		if (state.loopCount > 0 && ++currentLoopCount >= state.loopCount) {
			stop(true);
		}
		if (state.loopDirection == Direction.FWDBACK || state.loopDirection == Direction.BACKFWD) {
			currentDirection = -currentDirection;
		}
		if (currentDirection >= 0) {
			int lsb = Bufferator.chunk4Frame((int) state.loopStart);
			currentSampleInfo.requestChunk(lsb, 1);
			currentSampleInfo.requestChunk(lsb+1, 1);
			currentLoopFrame = 0;
		} else {
			int ll1 = (int) (state.loopLength-1);
			int lsb = Bufferator.chunk4Frame((int) (state.loopStart+ll1));
			currentSampleInfo.requestChunk(lsb, 1);
			currentSampleInfo.requestChunk(lsb-1, 1);
			currentLoopFrame = ll1;
		}
		reloopCPad(cPadPointer);
		doSync();
	}
	
	private void doSync() {
		int i=0;
		PadSample p;
		while ((p = ((PadSample)manager.getSamplePlayer(i++))) != null) {
			if (p != this && p.state.syncMasterId == id && (p.isPlaying() || p.isPaused())) {
				if (p.isPaused()) {
					manager.startSamplePlayer(p, false);
					if (listener != null) listener.playStarted(p);
				} else {
					manager.startSamplePlayer(p, true);
				}
			}
		}
	}
	
	public int playChunkCumulativeJ(
			float []buff, int buffInd, int nIterFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen)
	{
		int nIterDataFrames = ((currentDataFrame + nIterFrames > nTotalFrames)?(nTotalFrames-currentDataFrame):nIterFrames);
		if (nIterDataFrames < 0) nIterDataFrames = 0;
		int coff = 0;
		if (chunkLen == 0) {
			coff = 0;
			nIterDataFrames = 0;
		} else if (nIterDataFrames > 0) {
			coff = currentDataFrame-chunkStart;
			if (coff < 0) {
				coff = 0;
			}
			if (coff + nIterDataFrames > chunkLen) {
				nIterDataFrames = chunkLen - coff;
				nIterFrames = nIterDataFrames;
			}
			coff = coff*nChannels;
		}
		int buffBnd;
		
		if (nOutChannels == 2) {
			float rAmp=(state.gain)*(1+state.pan)/2;
			float lAmp=(state.gain)*(1-state.pan)/2;
			buffBnd = buffInd+2*nIterDataFrames;
			if (nChannels == 2) {
				for (int i=coff; buffInd < buffBnd; i+=2, buffInd+=2) {
					buff[buffInd] += (float) (lAmp * chunkData[i]);
					buff[buffInd+1] += (float) (rAmp * chunkData[i+1]);
				}
			} else if (nChannels == 1) {
				for (int i=coff; buffInd < buffBnd; i++, buffInd+=2) {
					buff[buffInd] += (float) (lAmp * chunkData[i]);
					buff[buffInd+1] += (float) (rAmp * chunkData[i]);
				}
			}
			buffBnd += 2*(nIterFrames-nIterDataFrames);
			buffInd = buffBnd;
//			for (;buffInd<buffBnd; buffInd++) {
//				buff[buffInd] = 0;
//			}
		} else if (nOutChannels == 1) { // dull, dull, dull
			float amp = state.gain;
			buffBnd = buffInd+nIterDataFrames;
			if (nChannels == 2) {
				amp = amp/2;
				for (int i=coff; buffInd < buffBnd; i+=2,buffInd++) {
					buff[buffInd] += (float) (amp * (chunkData[i]+chunkData[i+1]));
				}
			} else if (nChannels == 1) {
				for (int i=coff; buffInd < buffBnd; i++,buffInd++) {
					buff[buffInd] += (float) (amp * chunkData[i]);
				}
			}
			buffBnd += (nIterFrames-nIterDataFrames);
			buffInd = buffBnd;
//			for (;buffInd<buffBnd; buffInd++) {
//				buff[buffInd] = 0;
//			}
		} else {
			// we would be exceedingly ambitious for a phone ;D
		}
		return nIterFrames;
	}
	
	/**
	 * @return
	 */
	public boolean fire()
	{
		return fire(false);
	}
	
	public boolean fire(boolean fromSync)
	{
//		Log.d("padsample", String.format("pad %d fired", id));
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
			return false;
		}
		if (nTotalFrames == 0) return false;
		setDirection(state.loopDirection);
		currentLoopFrame = (int) ((currentDirection >= 0)?0:(state.loopLength-1));
		if (!fromSync) {
			currentLoopCount = 0;
		}
		playState = State.PLAYING;
		fireCPad(cPadPointer);
		doSync();
		return true;
	}
	
	public boolean firePaused()
	{
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
			return false;
		}
		if (nTotalFrames == 0) return false;
		currentLoopFrame = (int) ((currentDirection >= 0)?0:(state.loopLength-1));
		currentLoopCount = 0;
		playState = State.PAUSED;
		return true;
	}
	
	/**
	 * @return
	 */
	public boolean stop(boolean andTriggerEvent)
	{
//		Log.d("padsample", String.format("stop triggered"));
		if (andTriggerEvent && listener != null) listener.playComplete(this);
		playState = State.STOPPED;
		currentLoopCount = 0;
		stopCPad(cPadPointer);
		return true;
	}

	public boolean isPlayable()
	{
		if (!isAssigned()) {
			return false;
		}
		if (state.loopCount > 0 && currentLoopCount >= state.loopCount) {
			return false;
		}
		return currentLoopFrame+state.loopStart < nTotalFrames;
	}
	
	public boolean isPlaying()
	{
		return playState == State.PLAYING;
	}

	public boolean isPaused()
	{
		return playState == State.PAUSED;
	}

	public boolean isAssigned()
	{
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
			return false;
		}
		return nTotalFrames > 0;
	}

		/**
		 * @param pbs
		 */
	public void setState(PadSampleState pbs)
	{
		if (pbs == null) return;
		state = pbs;
		boolean ip = isPlaying();
		String cp = state.path;
		setSamplePath(pbs.path, false);
		setLoopStart(pbs.loopStart); // this will make the first chunk requests
		setLoopLength(pbs.loopLength); // and also perhaps more
		setDirection(pbs.loopDirection);
		setGain(pbs.gain);	// must set gain in c
		setPan(pbs.pan);
		setFilterParams(pbs.filterType, pbs.filterFrequency, pbs.filterResonance, pbs.filterEnvMod);
		setTune(pbs.tune);
		if (ip) {
			if(!isPlayable()) {
				stop(true);
			}
			if (state.path != null && cp != null && !state.path.equals(cp)) {
				stop(true);
			}
		}
		setLFOList(pbs.lfo);
		setEnvList(pbs.envelope);
		setSendList(pbs.send);
		if (interfaceTgtAdapter != null) interfaceTgtAdapter.refilter();
		if (lfoTgtAdapter != null) lfoTgtAdapter.refilter();
		if (envTgtAdapter != null) envTgtAdapter.refilter();
	}
	
	/**
	 * initial buffer requests are made inside the load routine in {@link com.mayaswell.audio.Bufferator}
	 * @see com.mayaswell.audio.Bufferator.SampleInfo#setSampleData()
	 * @param path
	 * @param setLoopBounds
	 * @return
	 */
	public boolean setSamplePath(String path, boolean setLoopBounds)
	{
//		Log.d("sample", "set path "+path+" "+setLoopBounds);
		if (path == null || path.equals("")) {
			Bufferator.free(currentSampleInfo);
			currentSampleInfo = null;
			state.path = null;
			
			setSampleInfo(0, (short) 0);
			if (listener != null) listener.setSampleData(this);
			return true;
		}

		state.path = path;
		if (currentSampleInfo != null && currentSampleInfo.path != null && currentSampleInfo.path.equals(path)) {
			return true;
		}
		
		Bufferator.free(currentSampleInfo);
		currentSampleInfo = null;
		SampleInfo inf = Bufferator.load(path);
		
		if (inf == null) {
			return false;
		}
		currentSampleInfo = inf;
		setSampleInfo(currentSampleInfo.nTotalFrames, currentSampleInfo.nChannels);
		
//		Log.d("sample", "got path "+path+" "+currentSampleInfo.nTotalFrames+", "+nTotalFrames);
		if (listener != null) listener.setSampleData(this);

		if (setLoopBounds) { // initial buffer requests already made in Bufferator.load
			state.loopStart = 0;
			state.loopLength = nTotalFrames;
//			Log.d("sample", "got path "+path+" "+state.loopLength+", "+nTotalFrames);
		}
		setDirection(state.loopDirection);
		return true;
	}
	
	public Button padButton = null;

	/**
	 * @param pb
	 */
	public void setPadButton(Button pb)
	{
		padButton = pb;
	}

	protected ControlsAdapter interfaceTgtAdapter = null;
	protected ControlsAdapter lfoTgtAdapter = null;
	protected ControlsAdapter envTgtAdapter = null;
	public static ArrayAdapter<Direction> directionAdapter = null;
	public static ArrayAdapter<Filter> filtersAdapter = null;
	public static ArrayAdapter<ResetMode> lfoRstModeAdapter=null; 
	public static ArrayAdapter<ResetMode> envRstModeAdapter=null; 
	public static ArrayAdapter<LFWave> lfoWaveAdapter=null; 
	public ArrayAdapter<PadSample.Info> syncMasterAdapter= null;
	
	
	@Override
	public ControlsAdapter getInterfaceTgtAdapter()
	{
		return interfaceTgtAdapter;
	}
	
	public ControlsAdapter getLFOTgtAdapter() {
		return lfoTgtAdapter;
	}

	public ControlsAdapter getEnvTgtAdapter() {
		return envTgtAdapter;
	}
	
	protected ControlsFilter inUseModulatorFilter = new ControlsFilter() {
		@Override
		public boolean isAvailable(Control c) {
			CControl cc = (CControl) c;
			if (cc.isEnvControl()) {
				if (state == null) return false;
				ArrayList<Envelope> el = state.envelope;
				int env = cc.env4Id();
				int param = cc.envParam4Id();
				if (env < state.envelope.size()) {
					if (el.get(env).target.size() == 0) return false;
					if (el.get(env).target.size() == 1 && el.get(env).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param >= 5) {
						if (param-5 >= el.get(env).target.size()) return false;
						if (el.get(env).target.get(param-5).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			} else if (cc.isLFOControl()) {
				if (state == null) return false;
				ArrayList<LFO> ll = state.lfo;
				int lfo = cc.lfo4Id();
				int param = cc.lfoParam4Id();
				if (lfo < ll.size()) {
					if (ll.get(lfo).target.size() == 0) return false;
					if (ll.get(lfo).target.size() == 1 && ll.get(lfo).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param == 1) {
						return false;
					} else if (param >= 2) {
						if (param-2 >= ll.get(lfo).target.size()) return false;
						if (ll.get(lfo).target.get(param-2).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			}
			return true;
		}
	};

	@Override
	public void setupControlsAdapter(Context c, int tvrid, int ddrid)
	{
		directionAdapter =  new ArrayAdapter<Direction>(c, tvrid);
		directionAdapter.setDropDownViewResource(ddrid);
		directionAdapter.add(Direction.FORWARD);
		directionAdapter.add(Direction.BACKWARD);
		directionAdapter.add(Direction.FWDBACK);
		directionAdapter.add(Direction.BACKFWD);
		
		interfaceTgtAdapter = new ControlsAdapter(c, tvrid);

		interfaceTgtAdapter.setDropDownViewResource(ddrid);
		interfaceTgtAdapter.add(new CControl(CControl.NOTHING));
		interfaceTgtAdapter.add(new CControl(CControl.GAIN));
		interfaceTgtAdapter.add(new CControl(CControl.PAN));
		interfaceTgtAdapter.add(new CControl(CControl.TUNE));
		interfaceTgtAdapter.add(new CControl(CControl.LOOP_START));
		interfaceTgtAdapter.add(new CControl(CControl.LOOP_LENGTH));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_ENVMOD));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		
		for (int i=0; i<CControl.maxBus; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.busSendId(i)));
		}
		
		for (int i=0; i<CControl.maxEnv; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.envAttackTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envDecayTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envSustainTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envSustainLevelId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envReleaseTimeId(i)));
			for (int j=0; j<CControl.maxEnvTgt; j++) {
				interfaceTgtAdapter.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<CControl.maxLFO; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
//				interfaceTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		interfaceTgtAdapter.setFilter(inUseModulatorFilter);
		interfaceTgtAdapter.refilter();
		
		lfoTgtAdapter = new ControlsAdapter(c, tvrid);
		lfoTgtAdapter.setDropDownViewResource(ddrid);
		lfoTgtAdapter.add(new CControl(CControl.NOTHING));
		lfoTgtAdapter.add(new CControl(CControl.GAIN));
		lfoTgtAdapter.add(new CControl(CControl.PAN));
		lfoTgtAdapter.add(new CControl(CControl.TUNE));
		lfoTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		lfoTgtAdapter.add(new CControl(CControl.FLT_ENVMOD));
		lfoTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		for (int i=0; i<CControl.maxLFO; i++) {
			lfoTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
//				lfoTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<CControl.maxEnv; i++) {
			for (int j=0; j<CControl.maxEnvTgt; j++) {
				lfoTgtAdapter.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		lfoTgtAdapter.setFilter(inUseModulatorFilter);
		lfoTgtAdapter.refilter();
		
		envTgtAdapter = new ControlsAdapter(c, tvrid);
		envTgtAdapter.setDropDownViewResource(ddrid);
		envTgtAdapter.add(new CControl(CControl.NOTHING));
		envTgtAdapter.add(new CControl(CControl.GAIN));
		envTgtAdapter.add(new CControl(CControl.PAN));
		envTgtAdapter.add(new CControl(CControl.TUNE));
		envTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		envTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		for (int i=0; i<CControl.maxLFO; i++) {
			envTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
				envTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		envTgtAdapter.setFilter(inUseModulatorFilter);
		envTgtAdapter.refilter();

		if (lfoRstModeAdapter == null) {
			lfoRstModeAdapter = new ArrayAdapter<ResetMode>(c, tvrid);
			lfoRstModeAdapter.setDropDownViewResource(ddrid);
			lfoRstModeAdapter.add(ResetMode.ONFIRE);
			lfoRstModeAdapter.add(ResetMode.ONLOOP);
		}
		if (envRstModeAdapter == null) {
			envRstModeAdapter = new ArrayAdapter<ResetMode>(c, tvrid);
			envRstModeAdapter.setDropDownViewResource(ddrid);
			envRstModeAdapter.add(ResetMode.ONFIRE);
			envRstModeAdapter.add(ResetMode.ONLOOP);		
		}
		if (lfoWaveAdapter == null) {
			lfoWaveAdapter = new ArrayAdapter<LFWave>(c, tvrid);
			lfoWaveAdapter.setDropDownViewResource(ddrid);
			lfoWaveAdapter.add(LFWave.SIN);
			lfoWaveAdapter.add(LFWave.SQUARE);
			lfoWaveAdapter.add(LFWave.SAW);
			lfoWaveAdapter.add(LFWave.EXP);
		}
	}

	public static void setupFiltersAdapter(Context c, int tvrid, int ddrid)
	{
		filtersAdapter  = new ArrayAdapter<Filter>(c, tvrid);
		filtersAdapter.setDropDownViewResource(ddrid);
		filtersAdapter.add(new Filter(Filter.UNITY, "Unity", "unity", "unity"));
		filtersAdapter.add(new Filter(Filter.SG_LOW, "SG L.Pass", "sg-lp", "SG Lo"));
		filtersAdapter.add(new Filter(Filter.SG_BAND, "SG B.Pass", "sg-bp", "SG Band"));
		filtersAdapter.add(new Filter(Filter.SG_HIGH, "SG H.Pass", "sg-hp", "SG Hi"));
		filtersAdapter.add(new Filter(Filter.MOOG1_LOW, "Moog Lo", "moog1-lp", "Moog Lo"));
		filtersAdapter.add(new Filter(Filter.MOOG1_BAND, "Moog Band", "moog1-bp", "Moog Band"));
		filtersAdapter.add(new Filter(Filter.MOOG1_HIGH, "Moog Hi", "moog1-hp", "Moog Hi"));
		filtersAdapter.add(new Filter(Filter.MOOG2_LOW, "VCF3 Lo", "moog2-lp", "VCF3 Lo"));
		filtersAdapter.add(new Filter(Filter.MOOG2_BAND, "VCF3 Band", "moog2-bp", "VCF3 Band"));
		filtersAdapter.add(new Filter(Filter.MOOG2_HIGH, "VCF3 Hi", "moog2-hp", "VCF3 Hi"));
		filtersAdapter.add(new Filter(Filter.FORMANT, "Formant", "formant", "Formant"));
	}
	
	public void setupSyncAdapter(Context c, int tvrid, int ddrid)
	{
		syncMasterAdapter  = new ArrayAdapter<PadSample.Info>(c, tvrid);
		syncMasterAdapter.setDropDownViewResource(ddrid);
		syncMasterAdapter.add(new PadSample.Info(null));
		int i=0;
		PadSample p;
		while ((p = ((PadSample)manager.getSamplePlayer(i++))) != null) {
			if (p.id != id) {
				syncMasterAdapter.add(new PadSample.Info(p));
			}
		}
	}
	
	
	public static Filter filter4type(int fid)
	{
		if (filtersAdapter == null) {
			return null;
		}
		for (int i=0; i<filtersAdapter.getCount(); i++) {
			Filter f = filtersAdapter.getItem(i);
			if (f.getId() == fid) {
				return f;
			}
			
		}
		return null;
	}
	
	public static Filter filter4name(String v)
	{
		if (filtersAdapter == null) {
			return null;
		}
		for (int i=0; i<filtersAdapter.getCount(); i++) {
			Filter f = filtersAdapter.getItem(i);
			if (f.getSaveName().equals(v)) {
				return f;
			}
			
		}
		return null;
	}

	@Override
	public int controllableId() {
		return id;
	}

	@Override
	public String controllableType()
	{
		return Controllable.PAD;
	}

	@Override
	public float floatValue(Control cc)
	{
		switch (cc.ccontrolId()) {
		case CControl.GAIN: return state.gain;
		case CControl.PAN: return state.pan;
		case CControl.TUNE: return state.tune;
		case CControl.LOOP_START: {
			return nTotalFrames == 0? 0: (((float)state.loopStart)/nTotalFrames);
		}
		case CControl.LOOP_LENGTH: {
			return nTotalFrames == 0? 0: (((float)state.loopLength)/nTotalFrames);
		}
		case CControl.FLT_CUTOFF: return state.filterFrequency;
		case CControl.FLT_ENVMOD: return state.filterEnvMod;
		case CControl.FLT_RESONANCE: return state.filterResonance;
		default: {
			if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(CControl.maxEnv)) {
				int env = CControl.env4Id(cc.ccontrolId());
				int param = CControl.envParam4Id(cc.ccontrolId());
				if (param == 0) {
					if (env<state.envelope.size()) { return state.envelope.get(env).attackT; }
				} else if (param == 1) { // decay
					if (env<state.envelope.size()) {	return state.envelope.get(env).decayT; }
				} else if (param == 2) {// sustain
					if (env<state.envelope.size()) {	return state.envelope.get(env).sustainT; } 
				} else if (param == 3) {// sustain level
					if (env<state.envelope.size()) {	return state.envelope.get(env).sustainL; } 
				} else if (param == 4) { // release
					if (env<state.envelope.size()) {	return state.envelope.get(env).releaseT; }
				} else {// depth(param-5);
					if (env<state.envelope.size() && (param-5)<state.envelope.get(env).target.size()) { 
						return state.envelope.get(env).target.get(param-5).amount; 
					}
				}
			} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(CControl.maxLFO)) {
				int lfo = CControl.lfo4Id(cc.ccontrolId());
				int param = CControl.lfoParam4Id(cc.ccontrolId());
				if (param == 0) { // rate
					if (lfo<state.lfo.size()) { return state.lfo.get(lfo).rate; }
				} else if (param == 1) { // phase
					if (lfo<state.lfo.size()) { return state.lfo.get(lfo).phase; } 
				} else {// depth(param-2);
					if (lfo<state.lfo.size() && (param-2) < state.lfo.get(lfo).target.size()) { 
						return state.lfo.get(lfo).target.get(param-2).amount;
					}
				}
			} else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(CControl.maxBus)) {
				int bus = CControl.bus4SendId(cc.ccontrolId());
				int param = CControl.busParam4Id(cc.ccontrolId());
				if (param == 0)	{
					Send bs = state.send4bus(bus);
					if (bs == null) {
						Bus b = BusMixer.getBus(bus);
						if (b != null) {
							bs = new Send(b, 1);
							state.addSend(bs);
							if (bus == 1) {
								bs.set(1.0f, false);
							} else {
								bs.set(0.0f, false);
							}
						}
					}
					return bs.gain;
				}
			}
		}
		}
		return 0;
	}

	@Override
	public void setValue(Control cc, float v)
	{
		switch (cc.ccontrolId()) {
		case CControl.GAIN: {
			setGain(v);
			break;
		}
		case CControl.PAN: {
			setPan(v);
			break;
		}
		case CControl.TUNE: {
			setTune(v);
			break;
		}
		case CControl.LOOP_START: {
			setLoopStart((long) (v * nTotalFrames));
			break;
		}
		case CControl.LOOP_LENGTH: {
			setLoopLength((long) (v * nTotalFrames));
			break;
		}
		case CControl.FLT_CUTOFF: {
			setFilterFrequency(v);
			break;
		}
		case CControl.FLT_ENVMOD: {
			setFilterEnvMod(v);
			break;
		}
		case CControl.FLT_RESONANCE: {
			setFilterResonance(v);
			break;
		}
		default:
			if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(CControl.maxEnv)) {
				int env = CControl.env4Id(cc.ccontrolId());
				int param = CControl.envParam4Id(cc.ccontrolId());
				if (param == 0) { setEnvelopeAttack(env, v); }
				else if (param == 1) { setEnvelopeDecay(env, v); } // decay
				else if (param == 2) { setEnvelopeSustain(env, v); } // sustain
				else if (param == 3) { setEnvelopeSustainLevel(env, v); } // sustain level
				else if (param == 4) { setEnvelopeRelease(env, v); } // sustain release
				else { setEnvelopeTargetAmt(env, param-5, v);} // depth(param-5);
			} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(CControl.maxLFO)) {
				int lfo = CControl.lfo4Id(cc.ccontrolId());
				int param = CControl.lfoParam4Id(cc.ccontrolId());
				if (param == 0) { setLFORate(lfo, v); } // rate
				if (param == 1) { setLFOPhase(lfo, v); } // phase
				else { setLFOTargetAmt(lfo, param-2, v);} // depth(param-2);
			} else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(CControl.maxBus)) {
				int bus = CControl.bus4SendId(cc.ccontrolId());
				int param = CControl.busParam4Id(cc.ccontrolId());
				if (param == 0)	{
					Send bs = state.send4bus(bus);
					if (bs == null) {
						Bus b = BusMixer.getBus(bus);
						if (b != null) {
							bs = new Send(b, 1);
							state.addSend(bs);
							if (bus == 1) {
								bs.set(1.0f, false);
							} else {
								bs.set(0.0f, false);
							}
							setSend(bs.bus, v, bs.mute);
						}
					}
					setSendGain(bs.bus, v);
				}
			}
		}
	}
	
	public void setGain(float v)
	{
		state.gain = v;
		setCPadGain(cPadPointer, v);
	}
	
	public void setPan(float v)
	{
		state.pan = v;
		setCPadPan(cPadPointer, v);
	}
	
	public void setTune(float v) {
		state.tune = v;
		setCPadTune(cPadPointer, v);
	}
	
	protected void setSampleInfo(int ntf, short nc)
	{
		nTotalFrames = ntf;
		nChannels = nc;
		setCPadSampleInfo(cPadPointer, ntf, nc);
	}
	
	@Override
	public String toString()
	{
		return "Pad"+Integer.toString(id);
	}

	@Override
	public String abbrevName()
	{
		return "P"+Integer.toString(id);
	}

	public void setLoopLength(long v)
	{
		state.loopLength = v;
		if (state.loopLength == 0) {
			currentLoopFrame = 0;
			requestChunk4frame(state.loopStart);
		} else if (currentLoopFrame >= state.loopLength) {
			currentLoopFrame = (int) (state.loopLength-1);
			requestChunk4frame(state.loopStart+state.loopLength);
		}
	}

	public void setLoopStart(long v)
	{
		state.loopStart = v;
		requestChunk4frame(state.loopStart+currentLoopFrame);
	}
	
	
	public void setFilterType(Filter flt)
	{
		state.filterType = flt.getId();
		setCPadFltType(cPadPointer, flt.getId());
	}
	
	public void setFilterFrequency(float f)
	{
		state.filterFrequency = f;
		setCPadFltFrequency(cPadPointer, f);
	}
	
	public void setFilterEnvMod(float f)
	{
		state.filterEnvMod = f;
		setCPadFltEnvMod(cPadPointer, f);
	}
	
	public void setFilterResonance(float r)
	{
		state.filterResonance = r;
		setCPadFltResonance(cPadPointer, r);
	}
	
	protected void setFilterParams(int t, float f, float r, float e)
	{
		state.filterType = t;
		state.filterFrequency = f;
		state.filterResonance = r;
		state.filterEnvMod = e;
		setCPadFlt(cPadPointer, t, f, r, e);
	}
	
	public void setBufsize(int bufsize)
	{
		setCPadBufsize(cPadPointer, bufsize);
	}
	
	public void setTempo(float t)
	{
//		Log.d("tempo set", String.format("pad id %d ... %g tempo", id, t));
		setCPadTempo(cPadPointer, t);
	}
	
	protected void requestChunk4frame(long fr)
	{
		SampleInfo csf = currentSampleInfo;
		if (csf != null && csf.sampleChunk != null && csf.nTotalFrames > 0) {
			int rchunk = Bufferator.chunk4Frame((int) fr);
			csf.requestChunk(rchunk, 2);
		}
	}

	public Observable<Path[]> getSamplePaths(final int w, final int h) {
		if (currentSampleInfo != null) {
			return currentSampleInfo.getSamplePaths(w, h);
		} else {
			return Observable.create(new Observable.OnSubscribe<Path[]>() {
				@Override
				public void call(Subscriber<? super Path[]> subscriber) {
					subscriber.onNext(null);
					subscriber.onCompleted();
				}
			});
		}
	}

	public String getSamplePath()
	{
		if (state != null && state.path != null) return state.path;
		return "(empty)";
	}

	public float proportion2secs(float tL)
	{
		if (nTotalFrames == 0) return 0f;
		return (tL * nTotalFrames)/manager.getSampleRate();
	}
	
	public float secs2proportion(float tS)
	{
		if (nTotalFrames == 0) return 0f;
		return (tS * manager.getSampleRate()) / nTotalFrames;
	}
	
	public SampleInfo getSampleInfo()
	{
		return currentSampleInfo;
	}
	
/*
 * 	send entry points
 */
	protected void setSendList(ArrayList<Send> send) {
		for (Send s: send) {
			if (!setSend(s.bus, s.gain, s.mute)) {
			}
		}
	}
	
	protected Send send(Bus b)
	{
		if (b == null) return null;
		for (Send s:state.send) {
			if (s.bus == b) return s;
		}
		return null;
	}
	
	public boolean setNSend(int n)
	{
		while (state.send.size() > n) {
			state.send.remove(state.send.size()-1);
		}
		return setCPadNSend(cPadPointer, n);
	}
	
	public boolean setSend(Bus b, float f, boolean m)
	{
		if (b == null) {
			Log.d("pad", "bus is null");
			return false;
		}
		Send xyci = send(b);
		if (xyci != null && f <= 0 && m) {
			state.send.remove(xyci);
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, f, m));
			} else {
				xyci.bus = b;
				xyci.gain = f;
				xyci.mute = m;
			}
		}
		return  setCPadSend(cPadPointer, b.getBusPointer(), f, m);
	}

	public boolean setSendGain(Bus b, float f)
	{
		if (b == null) {
			return false;
		}

		Send xyci = send(b);
		if (f <= 0) {
			if (xyci != null) {
				if(xyci.mute) state.send.remove(xyci);
				else xyci.gain = f;
			}
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, f, false));
			} else {
				xyci.bus = b;
				xyci.gain = f;
			}
		}
		return  setCPadSendGain(cPadPointer, b.getBusPointer(), f);
	}

	public boolean setSendMute(Bus b, boolean m)
	{
		if (b == null) {
			return false;
		}
		Send xyci = send(b);
		if (m) {
			if (xyci != null) {
				if (xyci.gain <= 0) state.send.remove(xyci);
				else xyci.mute = m;
			}
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, 1, m));
			} else {
				xyci.bus = b;
				xyci.mute = m;
			}
		}
		return  setCPadSendMute(cPadPointer, b.getBusPointer(), m);
	}

	/*
	 * 	lfo entry points
	 */
	public boolean setNLFO(int n)
	{
		return setCPadNLFO(cPadPointer, n);
	}

	public boolean setLFOList(ArrayList<LFO> lfo)
	{
		setCPadNLFO(cPadPointer, lfo.size());
		int i=0;
		for (LFO l: lfo) {
			if (l.countValidTargets() > 0) {
				setCPadLFO(cPadPointer, i, l.waveform.code(), l.trigger.code(), l.tempoLock, l.rate, l.phase);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) {
						setCPadLFOTgt(cPadPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setCPadNLFOTgt(cPadPointer, i, j);
				i++;
			}
		}
		return true;
	}
	
	public boolean setLFORate(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).rate = amt;
		return setCPadLFORate(cPadPointer, xyci, amt);
	}

	public boolean setLFOPhase(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).phase = amt;
		return setCPadLFOPhase(cPadPointer, xyci, amt);
	}

	public boolean setLFOLock(int xyci, boolean amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).tempoLock = amt;
		return setCPadLFOLock(cPadPointer, xyci, amt);
	}

	public boolean setLFOReset(int xyci, ResetMode amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).trigger = amt;
		return setCPadLFOReset(cPadPointer, xyci, amt.code());
	}

	public boolean setLFOWave(int xyci, LFWave amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).waveform = amt;
		return setCPadLFOWave(cPadPointer, xyci, amt.code());
	}
	
	public boolean setNLFOTarget(int xyci, int n)
	{
		return setCPadNLFOTgt(cPadPointer, xyci, n);
	}

	public boolean setLFOTarget(int xyci, int xycj, ICControl target, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
//			Log.d("padsample", String.format("setlfotgt %d %d %s %g", xyci, xycj, target.abbrevName(), amt));
			state.lfo.get(xyci).target.get(xycj).target = target;
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setCPadLFOTgt(cPadPointer, xyci, xycj, target != null ? target.ccontrolId() : CControl.NOTHING, amt);
	}

	public boolean setLFOTargetAmt(int xyci, int xycj, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setCPadLFOTgtAmt(cPadPointer, xyci, xycj, amt);
	}
	
	
	/*
	 * 	envelope entry points
	 */
	public boolean setNEnvelope(int n)
	{
		return setCPadNEnv(cPadPointer, n);
	}

	public boolean setEnvList(ArrayList<Envelope> envelope)
	{
		setCPadNEnv(cPadPointer, envelope.size());
		int i=0;
		for (Envelope l: envelope) {
			if (l.countValidTargets() > 0) {
				setCPadEnv(cPadPointer, i++, l.trigger.code(), l.tempoLock, l.attackT, l.decayT, l.sustainT, l.sustainL, l.releaseT);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) { // need to set the NOTHING value too
						setCPadEnvTgt(cPadPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setCPadNLFOTgt(cPadPointer, i, j);
				i++;
			}
		}
		return true;
	}
	
	public boolean setEnvelopeLock(int xyci, boolean amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).tempoLock = amt;
		return setCPadEnvLock(cPadPointer, xyci, amt);
	}

	public boolean setEnvelopeReset(int xyci, ResetMode amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).trigger = amt;
		return setCPadEnvReset(cPadPointer, xyci, amt.code());
	}

	public boolean setEnvelopeAttack(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).attackT = t;
		return setCPadEnvAtt(cPadPointer, xyci, t);
	}

	public boolean setEnvelopeDecay(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).decayT = t;
		return setCPadEnvDec(cPadPointer, xyci, t);
	}

	public boolean setEnvelopeSustain(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).sustainT = t;
		return setCPadEnvSus(cPadPointer, xyci, t);
	}

	public boolean setEnvelopeSustainLevel(int xyci, float l)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).sustainL = l;
		return setCPadEnvSusLevel(cPadPointer, xyci, l);
	}

	public boolean setEnvelopeRelease(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).releaseT = t;
		return setCPadEnvRel(cPadPointer, xyci, t);
	}

	public boolean setNEnvelopeTarget(int xyci, int n)
	{
		return setCPadNEnvTgt(cPadPointer, xyci, n);
	}

	public boolean setEnvelopeTarget(int xyci, int xycj, ICControl target, float amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size() && xycj >= 0 && xycj <state.envelope.get(xyci).target.size()) {
			state.envelope.get(xyci).target.get(xycj).target = target;
			state.envelope.get(xyci).target.get(xycj).amount = amt;
		}
		return setCPadEnvTgt(cPadPointer, xyci, xycj, target != null ? target.ccontrolId() : CControl.NOTHING, amt);
	}

	public boolean setEnvelopeTargetAmt(int xyci, int xycj, float amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size() && xycj >= 0 && xycj <state.envelope.get(xyci).target.size()) state.envelope.get(xyci).target.get(xycj).amount = amt;
		return setCPadEnvTgtAmt(cPadPointer, xyci, xycj, amt);
	}

	public int getSyncMaster() {
		return state.syncMasterId;
	}
	public void setSyncMaster(PadSample s) {
		if (s == this) {
			s = null;
		}
		if (s != null) {
			state.syncMasterId = s.id;
		} else {
			if (isPaused()) {
				stop(false);
			}
		}
	}
	public boolean shouldStartPaused() {
		return state != null && state.syncMasterId >= 0;
	}
	public void setDirection(Direction d) {
		state.loopDirection = d;
		switch (d) {
		case FWDBACK:
		case FORWARD: {
			if (currentSampleInfo != null) {
				int lsb = Bufferator.chunk4Frame((int) state.loopStart);
				currentSampleInfo.requestChunk(lsb, 1);
			}
			currentDirection = 1;
			break;
		}
		case BACKFWD:
		case BACKWARD: {
			if (currentSampleInfo != null) {
				int ll1 = (int) (state.loopLength-1);
				int lsb = Bufferator.chunk4Frame((int) (state.loopStart+ll1));
				currentSampleInfo.requestChunk(lsb, 1);
			}
			currentDirection = -1;
			break;
		}
		}
	}
	
	@Override
	public ArrayList<LFO> lfos() {
		return state.lfo;
	}
	@Override
	public ArrayList<Envelope> envs() {
		return state.envelope;
	}
	
	@Override
	public int maxLFO() {
		return CControl.maxLFO;
	}
	@Override
	public int maxLFOTgt() {
		return CControl.maxLFOTgt;
	}
	@Override
	public int maxEnv() {
		return CControl.maxEnv;
	}
	@Override
	public int maxEnvTgt() {
		return CControl.maxEnvTgt;
	}
	
	@Override
	public boolean alwaysLockedTempo()
	{
		return false;
	}

	@Override
	public int nActiveBus() {
		return activeBusCount(cPadPointer);
	}

	@Override
	public Send send4Bus(Bus b) {
		if (b == null || state.send == null) {
			return null;
		}
		for (Send bs: state.send) {
			if (bs.getBus() == b) {
				return bs;
			}
		}
		return null;
	}
}
