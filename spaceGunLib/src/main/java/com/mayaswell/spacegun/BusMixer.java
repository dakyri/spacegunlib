package com.mayaswell.spacegun;

import java.util.ArrayList;

import android.util.Log;

public class BusMixer
{
	protected ArrayList<Bus> bus = null;
	private static long bmxPointer = 0;
	private static BusMixer instance = null;
	
	private static native long allocBmx(int bs);
	private static native void deleteBmx(long ptr);
	private static native void setBmxBufsize(long ptr, int bufsize);
	public static native void globalBmxInit();
	public static native void globalBmxCleanup();
	
	private static native void setBmxTempo(long ptr, float tempo);
		
	private static native void zeroBmxBuffers(long ptr, int nFramePerBuf);
	private static native void bmxMix(long ptr, int nbus, short[] outBuffer, int nFramePerBuf, short nOutChannels);
	
	private BusMixer(int nb, int nFX, int nFXPar, int nLFO, int nLFOTgt, int bufSize)
	{
		Log.d("create", String.format("ing busmixer nb %d nfx %d %d %d %d %d ", nb,  nFX,  nFXPar,  nLFO,  nLFOTgt,  bufSize));
		bmxPointer = allocBmx(bufSize);
		for (int i=0; i<nb; i++) {
			Log.d("create", "ing buses and adding ... "+i);
			addBus(new Bus(i, bmxPointer, nFX, nFXPar, nLFO, nLFOTgt));
		}
	}
	
	public static BusMixer build(int nb, int nFX, int nFXPar, int nLFO, int nLFOTgt, int bufSize)
	{
		if (instance == null) {
			instance = new BusMixer(nb, nFX, nFXPar, nLFO, nLFOTgt, bufSize);
		}
		return instance;
	}
	
	public static BusMixer getInstance()
	{
		if (instance == null) {
			Log.d("busmix", "bus mixer unconstructed before access");
		}
		return instance;
	}
	
	public int addBus(Bus b)
	{
		if (bus==null) bus = new ArrayList<Bus>();
		int n = bus.size();
		bus.add(b);
		return n;
	}
	
	public static Bus getBus(int i)
	{
		if (instance == null) {
			return null;
		}
		if (i>=0 && i<instance.bus.size()) {
			return instance.bus.get(i);
		}
		return null;
	}
	
	public ArrayList<Bus> getBus()
	{
		return bus;
	}
	
	public ArrayList<BusState> cloneBusStates()
	{
		ArrayList<BusState> bal = new ArrayList<BusState>();
		for (Bus b: bus) {
			if (b.state != null) {
				bal.add(b.state.clone());
			}
		}
		return bal;
	}
	
	public void setBusStates(ArrayList<BusState> ebo)
	{
		for (int i=0; i<bus.size(); i++) {
			Bus b = bus.get(i);
			BusState bss = null;
			for (BusState bssi: ebo) {
				if (bssi.id == i) {
					bss = bssi;
				}
			}
			b.setState(bss);
		}
	}
	
	public static long getNativePointer()
	{
		return bmxPointer;
	}
	
	public void setBufsize(int bufsize)
	{
		setBmxBufsize(bmxPointer, bufsize);
	}
	
	public static void zeroBuffers(int nFramePerBuf)
	{
		zeroBmxBuffers(bmxPointer, nFramePerBuf);	
	}
	
	public static void mix(int nActiveBus, short[] outBuffer, int nFramePerBuf, short nOutChannels) {
		bmxMix(bmxPointer, nActiveBus, outBuffer, nFramePerBuf, nOutChannels);
	}
	
	public static void setTempo(float tempo)
	{
		setBmxTempo(bmxPointer, tempo);	
	}

}
