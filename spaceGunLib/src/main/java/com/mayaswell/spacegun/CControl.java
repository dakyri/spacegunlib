package com.mayaswell.spacegun;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;

public class CControl extends Control {
	public static int maxEnv;
	public static int maxEnvTgt;
	public static int maxLFO;
	public static int maxLFOTgt;
	public static int maxBus;
	public static int maxBusFX;
	public static int maxFXParam;
	
	public static void init(int _maxLFO, int _maxLFOTgt, int _maxEnv, int _maxEnvTgt, int _maxBus, int _maxBusFX, int _maxFXParam) {
		maxEnv = _maxEnv;
		maxEnvTgt = _maxEnvTgt;
		maxLFO = _maxLFO;
		maxLFOTgt = _maxLFOTgt;
		maxBus = _maxBus;
		maxBusFX = _maxBusFX;
		maxFXParam = _maxFXParam;
	}
	
	public static final int GAIN=1;
	public static final int PAN=2;
	public static final int LOOP_START=3;
	public static final int LOOP_LENGTH=4;
	public static final int FLT_CUTOFF=5;
	public static final int FLT_ENVMOD=6;
	public static final int FLT_RESONANCE=7;
	public static final int TUNE = 8;
	public static final int SEND_BASE=20;
	public static final int ENV_BASE=40;
	public static int busIdBase(int i) { return (SEND_BASE + i); }
	public static int busSendId(int i) { return (SEND_BASE + i); }
	public static int bus4SendId(int id) { return (id - SEND_BASE); }
	public static int busParam4Id(int id) { return (0); /* only gain */}
//SpaceGun.ENV_PER_PAD *
//	attackT 
//	decayT 
//	sustainT
//	sustainL
//	releaseT 
//		+ SpaceGun.MAX_ENV_TGT * target
	public static int envParam4Id(int i) { return (i-ENV_BASE)%(maxEnvTgt+5); }
	public static int env4Id(int i) { return (i-ENV_BASE)/(maxEnvTgt+5); }
	public static int envIdBase(int i) { return ENV_BASE+i*(maxEnvTgt+5); }
	public static int envAttackTimeId(int i) { return 0+envIdBase(i); }
	public static int envDecayTimeId(int i) { return 1+envIdBase(i); }
	public static int envSustainTimeId(int i) { return 2+envIdBase(i); }
	public static int envSustainLevelId(int i) { return 3+envIdBase(i); }
	public static int envReleaseTimeId(int i) { return 4+envIdBase(i); }
	public static int envTargetDepthId(int i, int j) { return j+5+envIdBase(i); }
//SpaceGun.LFO_PER_PAD *
//	rate 
//	phase 
//		+ SpaceGun.MAX_LFO_TGT * target
	public static int lfoParam4Id(int i) { return (i-envIdBase(maxEnv))%(maxLFOTgt+2); }
	public static int lfo4Id(int i) { return (i-envIdBase(maxEnv))/(maxLFOTgt+2); }
	public static int lfoIdBase(int i) { return envIdBase(maxEnv)+i*(maxLFOTgt+2); }
	public static int lfoRateId(int i) { return 0+lfoIdBase(i); }
	public static int lfoPhaseId(int i) { return 1+lfoIdBase(i); }
	public static int lfoTargetDepthId(int i, int j) { return j+2+lfoIdBase(i); }
//SpaceGun.MAX_BUS_FX *
//	enable 
//		+ SpaceGun.MAX_FX_PARAM * param
//params are 0 enable, 1-6 floating point parameters
	public static int fxIdBase(int i) { return lfoIdBase(maxLFO)+i*(maxFXParam+1); }
	public static int fxEnableId(int i) { return 0+fxIdBase(i); }
	public static int fxParamId(int i, int j) { return 1+j+fxIdBase(i); }
	public static int fxParam4Id(int i) { return ((i-lfoIdBase(maxLFO))%(maxFXParam+1)); }
	public static int fx4Id(int i) { return (i-lfoIdBase(maxLFO))/(maxFXParam+1); }
	
	public CControl(int i)
	{
		typeCode = i;
		id = 0;
	}
	
	public CControl(String s)
	{
		typeCode = string2code(s);
		id = 0;
	}
	
	/**
	 */
	@Override
	public String fullName()
	{
		switch (typeCode) {
		case GAIN: return "gain";
		case PAN: return "pan";
		case LOOP_START: return "loop start";
		case LOOP_LENGTH: return "loop length";
		case FLT_CUTOFF: return "cutoff";
		case FLT_ENVMOD: return "env mod";
		case FLT_RESONANCE: return "resonance";
		case TUNE: return "tune";
		default: {
			if (typeCode >= envIdBase(0) && typeCode < envIdBase(maxEnv)) {
				int env = env4Id(typeCode)+1;
				int param = envParam4Id(typeCode);
				if (param == 0) return "env"+Integer.toString(env)+" attack";
				if (param == 1) return "env"+Integer.toString(env)+" decay";
				if (param == 2) return "env"+Integer.toString(env)+" sustain";
				if (param == 3) return "env"+Integer.toString(env)+" sustain level";
				if (param == 4) return "env"+Integer.toString(env)+" sustain release";
				return "env"+Integer.toString(env)+" depth"+Integer.toString(param-5+1);
			} else if (typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(maxLFO)) {
				int lfo = lfo4Id(typeCode)+1;
				int param = lfoParam4Id(typeCode);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phase";
				return "lfo"+Integer.toString(lfo)+" depth"+Integer.toString(param-2+1);
			} else if (typeCode >= fxIdBase(0) && typeCode < fxIdBase(maxBusFX)) {
				int fx = fx4Id(typeCode)+1;
				int param = fxParam4Id(typeCode); // param 0 is enable
				if (param == 0) return "fx"+Integer.toString(fx)+" enable";
				return "fx"+Integer.toString(fx)+" param"+Integer.toString(param);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(maxBus)) {
				int bus = bus4SendId(typeCode)+1;
				return "bus"+Integer.toString(bus)+" send";
			}
			break;
		}
		}
		return "none";
	}
	
	/**
	 */
	@Override
	public String abbrevName() {
		switch (typeCode) {
		case GAIN: return "gain";
		case PAN: return "pan";
		case LOOP_START: return "start";
		case LOOP_LENGTH: return "length";
		case FLT_CUTOFF: return "cutoff";
		case FLT_ENVMOD: return "envmod";
		case FLT_RESONANCE: return "reson";
		case TUNE: return "tune";
		default: {
			if (typeCode >= envIdBase(0) && typeCode < envIdBase(maxEnv)) {
				int env = env4Id(typeCode)+1;
				int param = envParam4Id(typeCode);
				if (param == 0) return "env"+Integer.toString(env)+" att";
				if (param == 1) return "env"+Integer.toString(env)+" dec";
				if (param == 2) return "env"+Integer.toString(env)+" sus";
				if (param == 3) return "env"+Integer.toString(env)+" susL";
				if (param == 4) return "env"+Integer.toString(env)+" rel";
				return "env"+Integer.toString(env)+" amt"+Integer.toString(param-5+1);
			} else if (typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(maxLFO)) {
				int lfo = lfo4Id(typeCode)+1;
				int param = lfoParam4Id(typeCode);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phs";
				return "lfo"+Integer.toString(lfo)+" amt"+Integer.toString(param-2+1);
			} else if (typeCode >= fxIdBase(0) && typeCode < fxIdBase(maxBusFX)) {
				int fx = fx4Id(typeCode)+1;
				int param = fxParam4Id(typeCode); 
				if (param == 0) return "fx"+Integer.toString(fx)+" en";
				return "fx"+Integer.toString(fx)+" p"+Integer.toString(param);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(maxBus)) {
				int bus = bus4SendId(typeCode)+1;
				return "send"+Integer.toString(bus);
			}
			break;
		}
		}
		return "none";
	}

	public String toString()
	{
		return fullName();
	}
	
	/**
	 *  converts a float param value with an arbitrary range into a normalized parameter, from [0,1]
	 * perhaps this should be refactored into PadSample, as some params (ie sample ranges) can't be arbitrarily normalized out of context
	 * from the parent audio object ... perhaps most more interesting parameters ...
	 * @param v
	 * @return the normalized value from [0,1]
	 */
	public float normalize(float v)
	{
		float nv=0;
		switch (typeCode) {
		case PAN: {
			nv = (v+1)/2;
			break;
		}
		case TUNE: {
			nv = (v+Controllable.TUNE_RANGE_SEMITONES)/(2*Controllable.TUNE_RANGE_SEMITONES);
			break;
		}
		default: {
			if (typeCode >= CControl.envIdBase(0) && typeCode < CControl.envIdBase(maxEnv)) {
				int env = CControl.env4Id(typeCode);
				int param = CControl.envParam4Id(typeCode);
				if (param == 0) { nv = v/Envelope.NormalizedBeatPerSegment; } // attack
				else if (param == 1) { nv = v/Envelope.NormalizedBeatPerSegment; } // decay
				else if (param == 2) { nv = v/Envelope.NormalizedBeatPerSegment; } // sustain
				else if (param == 3) { nv = v; } // sustain level
				else if (param == 4) { nv = v/Envelope.NormalizedBeatPerSegment; } // release
				else { nv = (v+1)/2; } // depth(param-5);
			} else if (typeCode >= CControl.lfoIdBase(0) && typeCode < CControl.lfoIdBase(maxLFO)) {
				int lfo = CControl.lfo4Id(typeCode);
				int param = CControl.lfoParam4Id(typeCode);
				if (param == 0) { // rate
					float minl = (float) Math.log(LFO.NormalizedMinRate);
					float maxl = (float) Math.log(LFO.NormalizedMaxRate);
					nv = ((float) Math.log(v)-minl)/(maxl-minl);
				} else if (param == 1) { nv = v; } // phase
				else { nv = (v+1)/2; } // depth(param-2);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(maxBus)) {
				int bus = bus4SendId(typeCode);
				nv = v/2; // bus send is over unity ... maybe do same for main gain or not TODO ro
			} else {
				nv = v;
				break;
			}
		}
		}
		if (nv < 0) nv = 0;
		else if (nv > 1) nv = 1;
		return nv;
	}
	
	/**
	 *  converts a float normalized param value from [0,1] into an ordinary arbitrary range float. 
	 * @param nv
	 * @return the regular param
	 */
	public float denormalize(float nv)
	{
		float v=0;
		if (nv < 0) nv = 0;
		else if (nv > 1) nv = 1;
		switch (typeCode) {
		case PAN: {
			v = 2*nv -1;
			break;
		}
		case TUNE: {
			v = Controllable.TUNE_RANGE_SEMITONES*(2*nv -1);
			break;
		}
		default: {
			if (typeCode >= CControl.envIdBase(0) && typeCode < CControl.envIdBase(maxEnv)) {
				int env = CControl.env4Id(typeCode);
				int param = CControl.envParam4Id(typeCode);
				if (param == 0) { v = nv*Envelope.NormalizedBeatPerSegment; } // attack
				else if (param == 1) { v = nv*Envelope.NormalizedBeatPerSegment;  } // decay
				else if (param == 2) { v = nv*Envelope.NormalizedBeatPerSegment;  } // sustain
				else if (param == 3) { v = nv; } // sustain level
				else if (param == 4) { v = nv*Envelope.NormalizedBeatPerSegment; } // release
				else { v = 2*nv -1;} // depth(param-5);
			} else if (typeCode >= CControl.lfoIdBase(0) && typeCode < CControl.lfoIdBase(maxLFO)) {
				int lfo = CControl.lfo4Id(typeCode);
				int param = CControl.lfoParam4Id(typeCode);
				if (param == 0) { // rate
					float minl = (float) Math.log(LFO.NormalizedMinRate);
					float maxl = (float) Math.log(LFO.NormalizedMaxRate);
					float logv = minl + (maxl-minl)*nv;
					v = (float) Math.exp(logv);
				} else if (param == 1) { v = nv; } // phase 
				else { v = 2*nv -1; } // depth(param-2);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(maxBus)) {
				int bus = bus4SendId(typeCode);
				v = 2*nv; // bus send is over unity ... maybe do same for main gain or not TODO ro
			} else {
				v = nv;
				break;
			}
		}
		}
		return v;
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.spacegun.ICControl#ccontrolId()
	 */
	@Override
	public int ccontrolId()
	{
		return typeCode;
	}

	/* (non-Javadoc)
	* @see com.mayaswell.spacegun.ICControl#ccontrolName()
	*/
	@Override
	public String ccontrolName()
	{
		return code2string(typeCode);
	}

	public int instanceId()
	{
		return 0;
	}
	
	public static String code2string(int tc)
	{
		switch (tc) {
		case GAIN: return "gain";
		case PAN: return "pan";
		case LOOP_START: return "loopStart";
		case LOOP_LENGTH: return "loopLength";
		case FLT_CUTOFF: return "fltCutoff";
		case FLT_ENVMOD: return "fltEnvmod";
		case FLT_RESONANCE: return "fltResonance";
		case TUNE: return "tune";
		default: {
			if (tc >= envIdBase(0) && tc < envIdBase(maxEnv)) {
				int env = env4Id(tc);
				int param = envParam4Id(tc);
				if (param == 0) return "env"+Integer.toString(env)+" attack";
				if (param == 1) return "env"+Integer.toString(env)+" decay";
				if (param == 2) return "env"+Integer.toString(env)+" sustain";
				if (param == 3) return "env"+Integer.toString(env)+" sustainLevel";
				if (param == 4) return "env"+Integer.toString(env)+" release";
				return "env"+Integer.toString(env)+" depth"+Integer.toString(param-5);
			} else if (tc >= lfoIdBase(0) && tc < lfoIdBase(maxLFO)) {
				int lfo = lfo4Id(tc);
				int param = lfoParam4Id(tc);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phase";
				return "lfo"+Integer.toString(lfo)+" depth"+Integer.toString(param-2);
			} else if (tc >= fxIdBase(0) && tc < fxIdBase(maxBusFX)) {
				int fx = fx4Id(tc);
				int param = fxParam4Id(tc);
				if (param == 0) return "fx"+Integer.toString(fx)+" enable";
				return "fx"+Integer.toString(fx)+" param"+Integer.toString(param-1);
			} else if (tc >= busIdBase(0) && tc < busIdBase(maxBus)) {
				int bus = bus4SendId(tc);
				return "bus"+Integer.toString(bus)+" send";
			}
			break;
		}
		}
		return "nothing";
	}

	public static int string2code(String type)
	{
		if (type.equals("gain")) {
			return GAIN;
		} else if (type.equals("pan")) {
			return PAN;
		} else if (type.equals("loopStart")) {
			return LOOP_START;
		} else if (type.equals("loopLength")) {
			return LOOP_LENGTH;
		} else if (type.equals("fltCutoff")) {
			return FLT_CUTOFF;
		} else if (type.equals("fltEnvmod")) {
			return FLT_ENVMOD;
		} else if (type.equals("fltResonance")) {
			return FLT_RESONANCE;
		} else if (type.equals("tune")) {
			return TUNE;
		} else {
			String [] scw = type.split(" ");
			if (scw.length == 2) {
				if (scw[0].startsWith("lfo")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("rate")) {
						return lfoRateId(li);
					} else if (scw[1].equals("phase")) {							
						return lfoPhaseId(li);
					} else if (scw[1].startsWith("depth")) {
						int di = Integer.parseInt(scw[1].substring(5));
						return lfoTargetDepthId(li, di);
					} else {
					}
				} else if (scw[0].startsWith("env")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("attack")) {
						return envAttackTimeId(li);
					} else if (scw[1].equals("decay")) {
						return envDecayTimeId(li);
					} else if (scw[1].equals("sustain")) {
						return envSustainTimeId(li);
					} else if (scw[1].equals("sustainLevel")) {
						return envSustainLevelId(li);
					} else if (scw[1].equals("release")) {						
						return envReleaseTimeId(li);
					} else if (scw[1].startsWith("depth")) {
						int di = Integer.parseInt(scw[1].substring(5));
						return envTargetDepthId(li, di);
					} else {
					}
				} else if (scw[0].startsWith("fx")) {
					int li = Integer.parseInt(scw[0].substring(2));
					if (scw[1].startsWith("param")) {
						int di = Integer.parseInt(scw[1].substring(5));
						return fxParamId(li, di);
					} else if (scw[1].startsWith("enable")) {
						return fxEnableId(li);
					} else {
						
					}
				} else if (scw[0].startsWith("bus")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("send")) {
						return busSendId(li);
					} else {
						
					}
				}
			}
		}
		return NOTHING;
	}
	public boolean isEnvControl() {
		return typeCode >= envIdBase(0) && typeCode < envIdBase(maxEnv);
	}
	public int env4Id() {
		return env4Id(typeCode);
	}
	public int envParam4Id() {
		return envParam4Id(typeCode);
	}
	public boolean isLFOControl() {
		return typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(maxLFO);
	}
	public int lfo4Id() {
		return lfo4Id(typeCode);
	}
	public int lfoParam4Id() {
		return lfoParam4Id(typeCode);
	}
	public int fx4Id() {
		return fx4Id(typeCode);
	}
	public int fxParam4Id() {
		return fxParam4Id(typeCode);
	}
	public boolean isFXControl() {
		return typeCode >= fxIdBase(0) && typeCode < fxIdBase(maxBusFX);
	}
	public boolean isBusControl() {
		return typeCode >= busIdBase(0) && typeCode < busIdBase(maxBus);
	}
	@Override
	public float defaultValue() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public int valueType() {
		return TYPE_FLOAT;
	}
}
