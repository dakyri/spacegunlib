package com.mayaswell.spacegun;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;

import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.audio.file.AudioFileException;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.file.AudioFile.Type;
import com.mayaswell.audio.file.WavFile;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.subjects.PublishSubject;
import rx.subscriptions.Subscriptions;

public class DBRXAudioMixer {


	private static final int MAX_HOLDING_QUEUE = 10;
		// number of buffers we keep to allow for interface lags and stuff ... on Nexus7 that's around 800ms
	private Thread producer = null;
	private Thread consumer = null;
	private Thread player = null;

	public float getSampleRate() {
		return sampleRate;
	}

	public double getRecordTime() {
		if (!isRecording()) return 0;
		return (System.currentTimeMillis() - recordStartTimestamp)/1000.0;
	}

	public class Buffer {
		Buffer(long timestamp, short[] data, int dataLength) {
			this.timestamp = timestamp;
			this.data = new short[dataLength];
			System.arraycopy(data, 0, this.data, 0, dataLength);
		}
		long timestamp;
		short[] data;
	}

	public static final int dfltSampleRate = 44100;
	public static int sampleRate = dfltSampleRate;
	protected boolean isRunning = false;
	protected int outBufSize = 4096;
	protected int inBufSize = 4096;
	protected int outChannelFormat = AudioFormat.CHANNEL_OUT_STEREO;
	protected int inChannels = AudioFormat.CHANNEL_IN_MONO;
	protected int nOutChannels = 2;
	protected int nInChannels = 1;

	protected LinkedBlockingQueue<Buffer> inputQueue = new LinkedBlockingQueue<>();
			// synchronized this is for sending buffers from producer to consumer
	protected LinkedList<Buffer> holdingQueue = new LinkedList<Buffer>();
			// local to the handleRecord componsent. not thread safe
	private boolean isRecording = false;
	private boolean findInitialBuffers = false;
	private boolean recordEnabled = true;
	private long recordEndTimestamp = 0;
	private long recordStartTimestamp = 0; // trigger to start recording

	private PublishSubject<Float> vuSubject = null;
				// vu is hot, and won't complete until end of activity, but maybe unsubsscribed
	private Subscriber<? super AudioFile> recordSubscriber=null; // our loop is hot, but recording isn't

	private AudioFile outAudioFile = null;

	private PadSample activePad[]=null;
	private int nActivePad = 0;

	public DBRXAudioMixer(int n) {
		outBufSize = AudioTrack.getMinBufferSize(sampleRate, outChannelFormat, AudioFormat.ENCODING_PCM_16BIT);
		inBufSize  = AudioRecord.getMinBufferSize(sampleRate, inChannels, AudioFormat.ENCODING_PCM_16BIT);
		if (outBufSize == AudioTrack.ERROR_BAD_VALUE) {
			// cry
		}
		if (inBufSize == AudioRecord.ERROR_BAD_VALUE) {
//			Log.d("sampler", "recording disabled :(");
			enableRecord(false);
		}
		Log.d("DBRXAudioMixer", "Got sizes " + inBufSize + ", " + outBufSize);
		vuSubject = PublishSubject.create();
//		recordSubject = PublishSubject.create();
		activePad = new PadSample[n];
		nActivePad = 0;
	}

	public int getOutBufsize()
	{
		return outBufSize;
	}
	
	public boolean startExport(SamplePlayer s, long frameOffset, File to, Type t) {
		return false;
	}
	
	public boolean stopExport(SamplePlayer s, long frameOffset) {
		return false;
	}

	protected void enableRecord(boolean b) {
		recordEnabled = false;
	}

	public boolean run() {
		if (isRunning) {
			return false;
		}

		isRunning = true;
		// buffer producer
		(producer = new Thread() {
			@Override
			public void run() {
				AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate, inChannels,
						AudioFormat.ENCODING_PCM_16BIT, inBufSize);
				short inBuffer[] = new short[inBufSize];
				Log.d("DBRXAudioMixer", "Got recordSubject "+recorder.getAudioSource());
				recorder.startRecording();
				while (isRunning) {
					long timeStamp = System.currentTimeMillis();
					int nSamplesRead = recorder.read(inBuffer, 0, inBufSize);
					inputQueue.add(new Buffer(timeStamp, inBuffer, nSamplesRead));
				}
				recorder.stop();
				recorder.release();
			}
		}).start();

		// buffer consumer
		(consumer = new Thread() {
			@Override
			public void run() {
				while (isRunning) {
					try {
						Buffer currentBuffer = inputQueue.take();
//						Log.d("DBRXAudioMixer", "got buffer timestamp "+currentBuffer.timestamp+" on thread "+Thread.currentThread().getId());
						handleMeterDisplay(currentBuffer);
						handleRecording(currentBuffer);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				vuSubject.onCompleted();
			}
		}).start();

		(player = new Thread() {
			public void run() {
//				setPriority(Thread.MAX_PRIORITY);

				AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, dfltSampleRate,
						outChannelFormat,
						AudioFormat.ENCODING_PCM_16BIT,
						outBufSize,
						AudioTrack.MODE_STREAM);

				int nFramePerBuf = outBufSize/nOutChannels;
//				float busBuffer[] = new float[outBufSize];
				short outBuffer[] = new short[outBufSize];

				audioTrack.play();

				int nActiveBus = 1;
				while(isRunning){
					BusMixer.zeroBuffers(nFramePerBuf);
					int j = 0;
					int ac = 0;
					while (j<activePad.length && ac<nActivePad) {
						PadSample p = activePad[j];
						if (p != null) {
							p.playCumulative(BusMixer.getNativePointer()/*busBuffer*/, nFramePerBuf, (short) 2);
							int pab = p.nActiveBus();
							if (pab > nActiveBus) {
								nActiveBus = pab;
							} else {
							}
							ac++;
						}
						j++;
					}
					/*
					for(int i=0; i < outBufSize; i++) {
						float b=busBuffer[i];
						outBuffer[i] = (short)(Short.MAX_VALUE*(b<-1.0f?-1.0f:(b>1.0f?1.0f:b)));
					}*/
					BusMixer.mix(nActiveBus, outBuffer, nFramePerBuf, (short)nOutChannels);
					audioTrack.write(outBuffer, 0, outBufSize);
				}
				audioTrack.stop();
				audioTrack.release();
			}
		}).start();

		return true;
	}

	public void stop() {
		isRunning = false;
		consumer.interrupt(); // in case we are blocked on inputQueue
	}

	private AudioFile startRecording(final File file) {
		return startRecording(file, System.currentTimeMillis());
	}

	private AudioFile startRecording(final File file, long ts) {
		Log.d("mixer", isRecording+" start rec "+file.getAbsoluteFile());
		if (!recordEnabled) {
			return null;
		}
		recordEndTimestamp = 0;
		recordStartTimestamp = ts;
		AudioFile of = new WavFile();
		try {
			of.create(file, nInChannels, Long.MAX_VALUE, 16, sampleRate);
		} catch (IOException e) {
			recorderError("Catastrophe! Some kind of IO exception, creating record file " + outAudioFile.getPath());
			return null;
		} catch (AudioFileException e) {
			recorderError("How Bizzare! An audio file exception, creating record file " + outAudioFile.getPath());
			return null;
		}
		findInitialBuffers = true;
		isRecording = true;
		return of;
	}

	/**
	 * handle recording.
	 * we have about 1 seconds worth of buffers stacked up, we only use 1 buffer per iteration
	 * @param inputBuffer
	 */
	private void handleRecording(Buffer inputBuffer) {
// stash the buffers ... even if we're not recording they're still keepers to allow for the interface processing lag
		holdingQueue.add(inputBuffer);
		if (holdingQueue.size() > MAX_HOLDING_QUEUE) {
			holdingQueue.poll();
		}
		if (isRecording) {
			boolean finalizeRecording = false;
			int offset = 0;
			int nValidFrames = inputBuffer.data.length / nInChannels;
			Buffer currentBuffer;
			if (findInitialBuffers) {
				long startOffsetT = 0;
				int nOffsetFrames = 0;

				// find the starting buffer in our holding queue
				while ((currentBuffer = holdingQueue.poll()) != null) {
					long sot = recordStartTimestamp - currentBuffer.timestamp;
					long nof = millisToFrames(sot);
					if (nof <= currentBuffer.data.length/nInChannels) {
						startOffsetT = sot;
						nOffsetFrames = (int) nof;
						break;
					}
//					Log.d("audio mixer", "skip holding buffer "+recordStartTimestamp+", at "+currentBuffer.timestamp);
				}
				if (currentBuffer == null) {
					currentBuffer = inputBuffer;
				}

				if (startOffsetT > 0) {
					if (nOffsetFrames < nValidFrames) {
//						Log.d("audio mixer", "recording on time "+recordStartTimestamp+", in buf "+currentBuffer.timestamp);
						offset = nOffsetFrames * nInChannels;
						nValidFrames = (inputBuffer.data.length / nInChannels) - nOffsetFrames;
						findInitialBuffers = false;
					} else {
						// still haven't got to the buffer for our start time .. this would be wierd and should never happen
//						Log.d("audio mixer", "recording early "+recordStartTimestamp+", in buf "+currentBuffer.timestamp);
					}
				} else { // better late than never, just start from the start of whatever last buffer we have
//					Log.d("audio mixer", "recording late "+recordStartTimestamp+", in buf "+currentBuffer.timestamp);
					recordStartTimestamp = currentBuffer.timestamp;
					findInitialBuffers = false;
				}
			} else {  // regular cycle grab the next buffer in the queue
				currentBuffer = holdingQueue.poll();
				nValidFrames = currentBuffer.data.length / nInChannels;
			}
			if (recordEndTimestamp > 0) {
					// recording ends when this gets set non zero. we'll still keep processing buffers until
					// we hit the buffer corresponding to this timestamp
				long endOffsetT = recordEndTimestamp - currentBuffer.timestamp;
				int endOffsetFrames = (int) millisToFrames(endOffsetT);
				if (endOffsetFrames*nInChannels < currentBuffer.data.length) {
					nValidFrames = endOffsetFrames - offset*nInChannels;
					finalizeRecording = true;
				} else { // havent reached endpoint yet

				}
			}

			try {
				if (offset > 0) {
					outAudioFile.writeFrames(currentBuffer.data, offset, nValidFrames);
				} else if (offset == 0){
					outAudioFile.writeFrames(currentBuffer.data, nValidFrames);
				}
			} catch (IOException e) {
				recorderError("Catastrophe! Some kind of IO exception, writing record file " + outAudioFile.getPath());
			} catch (AudioFileException e) {
				recorderError("How Bizzare! An audio file exception, writing record file " + outAudioFile.getPath());
			}
			if (finalizeRecording) {
				Log.d("audio mixer", "finalizing recording");
				try {
					outAudioFile.close();
				} catch (IOException e) {
					recorderError("Catastrophe! Some kind of IO exception, finalizing record file " + outAudioFile.getPath());
				}
				recordSubscriber.onNext(outAudioFile);
				outAudioFile = null;
				isRecording = false;
				recordSubscriber.onCompleted();
				recordSubscriber = null;
			}
		} else {
		}
	}

	/**
	 * we just mark the end timestamp we want, and the filehandler will handle the closing etc.
	 *
	 * ?? time base for stopping and starting is in ms ... would it be better in frames?
	 * @param ts timestamp in ms to halt recording at
	 * @param syncMaster pointer to pad state of the master sample used for lenght and for sync'ing
	 */
	public void stopRecording(long ts, PadSampleState syncMaster)
	{
		if (syncMaster != null) {
			long mstMsDur = (long) Math.floor((((double)syncMaster.loopLength) * 1000.0 )/ sampleRate);
			long schMsDur = ts - recordStartTimestamp;
			long nMstMultiples = (schMsDur / mstMsDur);
			if (nMstMultiples == 0 || (schMsDur % mstMsDur) != 0) {
				nMstMultiples++;
			}
			schMsDur = mstMsDur * nMstMultiples;
			ts = recordStartTimestamp + schMsDur;
		}
		stopRecording(ts);
	}

	public void stopRecording(long ts) {
		recordEndTimestamp = ts;
	}

	/**
	 * halt recording immediately, and clean up the mess
	 */
	public void cancelRecording()
	{
		isRecording = false;
		findInitialBuffers = false;
		recordStartTimestamp = 0;
		try {
			outAudioFile.close();
		} catch (IOException e) {
			// we don't give a hoot about this
		}
		outAudioFile.getFile().delete();
		if (recordSubscriber != null) {
			recordSubscriber.onCompleted();
		}
	}

	public boolean isRecording()
	{
		return isRecording;
	}

	private void handleMeterDisplay(Buffer currentBuffer) {
		double rmsTotal = 0;
		for (int i=0; i<currentBuffer.data.length; i++) {
			float d = ((float)currentBuffer.data[i])/Short.MAX_VALUE; // normalized vu
			rmsTotal += d * d;
		}
//		Log.d("DBRXAudioMixer", "got "+rmsTotal+", "+currentBuffer.data.length);
		double vuVal = Math.sqrt(rmsTotal/currentBuffer.data.length);
		vuSubject.onNext((float) vuVal);
	}

	private long millisToFrames(long mSecs) {
		return (long)(mSecs * sampleRate / 1000.0);
	}

	public Observable<Float> getVU() {
		return vuSubject.onBackpressureDrop().observeOn(AndroidSchedulers.mainThread());
	}

	private void recorderError(String s) {
		isRecording = false;
		if (recordSubscriber != null) {
			recordSubscriber.onError(new Exception(s));
		}
	}

	public Observable<AudioFile> doRecord(final File file, final long ts) {
		return Observable.create(new Observable.OnSubscribe<AudioFile>() {
			@Override
			public void call(Subscriber<? super AudioFile> subscriber) {
				if (isRecording) {
// todo our fix might need to be more comprehensive ... but should never happen anyeway ... may also be synch issues
					subscriber.onError(new Exception("already recording"));
					return;
				}
				outAudioFile = startRecording(file, ts);
				recordSubscriber = subscriber;
				subscriber.add(Subscriptions.create(new Action0(){
					@Override
					public void call() { // clean up local mess
						recordSubscriber = null;
						isRecording = false;
					}
				}));
			}
		}).observeOn(AndroidSchedulers.mainThread());
	}

	public synchronized boolean startPad(PadSample p, boolean fromSync)
	{
		if (p == null) {
			return false;
		}
		if (!p.isAssigned()) {
			return false;
		}
		int j = 0;
		int ac = 0;
		int ah = -1;
		while (j<activePad.length && ac < nActivePad) {
			if (activePad[j] == p) {
				p.fire();
				return true;
			}
			if (activePad[j] != null) {
				ac++;
			} else {
				if (ah < 0) ah = j;
			}
			j++;
		}
		if (nActivePad < activePad.length) {
			if (ah >= 0) {
				activePad[ah] = p;
			} else {
				activePad[nActivePad] = p;
			}
			nActivePad++;
			p.fire(fromSync); // NB this may fire off other pads now!!!! order of these statements is importan increment nActive first
		}
		return true;
	}

	public synchronized void stopPad(PadSample p)
	{
		int j = 0;
		int ac = 0;
		while (j<activePad.length && ac<nActivePad) {
			if (activePad[j] == p) {
				activePad[j] = null;
				nActivePad--;
				return;
			}
			if (activePad[j] != null) {
				ac++;
			}
			j++;
		}
	}


}