package com.mayaswell.spacegun.fragment;

import java.util.ArrayList;
import java.util.Arrays;

import com.mayaswell.fragment.MWFragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ViewAnimator;

public class FragmentHolder extends RelativeLayout {
	
	public class SGFragmentInfo {
		SGFragmentInfo(MWFragment f, String t) {
			fragment = f;
			tag = t;
		}
		
		SGFragmentInfo(MWFragment f) {
			this(f, null);
		}
		
		public MWFragment fragment = null;
		public String tag = null;
		public boolean addedToHolder = false;
	}

	private static final int ADJUST_CHANGED_ROWS = 1;

	FragmentManager fm = null;
	ArrayList<SGFragmentInfo> fragments = null;

	public FragmentHolder(Context context) {
		super(context);
		setup(context, null);
	}

	public FragmentHolder(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs);
	}

	private void setup(Context context, AttributeSet attrs) {
		fragments = new ArrayList<SGFragmentInfo>();
		fm = ((Activity) context).getFragmentManager();
		setOnHierarchyChangeListener(new OnHierarchyChangeListener() {

			@Override
			public void onChildViewAdded(View parent, View child) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildViewRemoved(View parent, View child) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	

	
	/**
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 * 
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public float dp2px(float dp){
		Resources resources = getContext().getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}
	
	/**
	 * This method converts device specific pixels to density independent pixels.
	 * 
	 * @param px A value in px (pixels) unit. Which we need to convert into db
	 * @return A float value to represent dp equivalent to px value
	 */
	public float px2dp(float px){
		Resources resources = getContext().getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
//		Log.d("fragger", String.format(">>> done on measure"));
	}
	
	@Override
	protected void onLayout (boolean changed, int l, int t, int r, int b)
	{
//		Log.d("fragger", String.format("onLayout %b, %d %d %d %d", b, l, t, r, b));
		super.onLayout(changed, l, t, r, b);
		if (changed) layoutChildren(false, true);
	}
	
	protected SGFragmentInfo info4Fragment(Fragment f) {
		for (SGFragmentInfo fi: fragments) {
			if (fi.fragment == f) {
				return fi;
			}
		}
		return null;
	}
	
	protected SGFragmentInfo info4View(View v) {
		for (SGFragmentInfo fi: fragments) {
			if (fi.fragment.getView() == v) {
				return fi;
			}
		}
		return null;
	}
	
	protected SGFragmentInfo addFragment(MWFragment f, String t) {
		SGFragmentInfo fi = new SGFragmentInfo(f, t);
		fragments.add(fi);
		return fi;
	}

	public boolean isShowing(MWFragment f) {
		return false;
	}

	private int currentFragMainViewId = 1;
	
	public void showFragment(MWFragment f) {
		if (f == null) {
			return;
		}
		SGFragmentInfo fi = info4Fragment(f);
		if (fi == null) {
			fi = addFragment(f, null);
		}
		
		View v = f.getView();
		if (v == null) {
			fm.beginTransaction().add(f, "fragment").commit();
			fm.executePendingTransactions();
			v = f.getView();
			v.setId(currentFragMainViewId++);
		}
		if (v != null && v.getParent() == null) {
			addView(v);
			fi.addedToHolder  = true;
			layoutChildren(true, false);
		}
	}
	
	public void hideFragment(Fragment f)
	{
		SGFragmentInfo fi = info4Fragment(f);
		View v = null;
		if (fi == null || !fi.addedToHolder || (v=f.getView()) == null) {
			return;
		}
		removeView(v);
		fi.addedToHolder = false;
		layoutChildren(true, false);
	}

/*
	LayoutParams omrlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
 		Log.d("fragger", String.format(">>> onMeaure %s %s", View.MeasureSpec.toString(widthMeasureSpec), View.MeasureSpec.toString(heightMeasureSpec)));
		int myWidth = MeasureSpec.getSize(widthMeasureSpec);
		int myHeight = MeasureSpec.getSize(heightMeasureSpec);
		for (int i=0; i<getChildCount(); i++) {
			View v = getChildAt(i);
			SGFragmentInfo f = info4View(v);
			v.measure(MeasureSpec.makeMeasureSpec(myWidth, MeasureSpec.AT_MOST),
					MeasureSpec.makeMeasureSpec(myHeight, MeasureSpec.AT_MOST));
			Log.d("fragger", String.format("onMeaure start child %d, meas %g %g, %d %d", i, px2dp(v.getMeasuredWidth()), px2dp(v.getMeasuredHeight()), v.getMeasuredWidth(), v.getMeasuredHeight()));
		}
		
		for (int i=0; i<getChildCount(); i++) {
			View v = getChildAt(i);
			Log.d("fragger", String.format("onMeaure end child %d, meas %d %d, %d %d", i, v.getMeasuredWidth(), v.getMeasuredHeight(), v.getWidth(), v.getHeight()));
		}
		
 */
	public void layout(ArrayList<Row> rows)
	{
		for (Row r: rows) {
			if (r != null && r.views.size() > 0) {
				int rightOfId = -1;
				int belowId = r.belowId;
				for (View v: r.views) {
					SGFragmentInfo fi = info4View(v);
					LayoutParams mlp = new LayoutParams(LayoutParams.WRAP_CONTENT, (fi!=null&&fi.fragment.isGreedy())?r.height:LayoutParams.WRAP_CONTENT);
	
					if (rightOfId < 0) {
						mlp.addRule(/*r.views.size() == 1?RelativeLayout.CENTER_HORIZONTAL:*/RelativeLayout.ALIGN_PARENT_LEFT);
					} else {
						mlp.addRule(RelativeLayout.RIGHT_OF, rightOfId);
					}
					if (belowId < 0) {
						mlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
					} else {
						mlp.addRule(RelativeLayout.BELOW, belowId);
					}
					rightOfId = v.getId();
					v.setLayoutParams(mlp);
				}
			}
		}
	}
	
	public Row addView(Row[] row, View v, int tw, int th, int chooseBy, boolean setMaxHeight)
	{
		int vh = v.getMeasuredHeight();
		int vw = v.getMeasuredWidth();
		int i = 0;
		Row chooseRow = null;
		int chooseRowI = -1;
		int chooseA = 0;
		int chooseW = 0;
		int acch = 0;
		for (Row r: row) {
			if (chooseBy == Row.FIRST_FIT && r.width+vw <= tw) {
				chooseRow = r;
				chooseRowI  = i;
				break;
			} else if (chooseBy == Row.MAX_AREA) {
				int rh = r.height;
				if (rh == 0 && r.views.size() == 0) {
					rh = th-acch;
					r.height = rh;
				}
				int ra = (tw-r.width)*r.height;
//				Log.d("max area", String.format("%d %d %d %d %d", i, ra, (vw-r.width), r.height, chooseA));
				if (ra > chooseA) {
					chooseRow = r;
					chooseRowI  = i;
					chooseA = ra;
				}
			} else if (chooseBy == Row.MAX_WIDTH) {
				if (tw-r.width > chooseW) {
					chooseRow = r;
					chooseRowI  = i;
					chooseW = tw-r.width;
				}
			}
			acch += r.height;
			i++;
		}
		if (chooseRow != null) {
			Log.d("Row.addView", String.format("add %d -> %d %d @ %d", v.getId(), vw, vh, chooseRowI));
			chooseRow.width += vw;
			chooseRow.views.add(v);
			if (chooseRow.height < vh) {
				if (!setMaxHeight) {
					if (i < row.length-1) {
						row[chooseRowI+1].belowId = v.getId();
					}
					chooseRow.height = vh;
				}
			}
		}
		return chooseRow;
	}

	public ArrayList<Row> differingRows(Row[] oldRow, Row[] newRow) {
		ArrayList<Row> changed = new ArrayList<Row>();
		if (newRow == null) {
			return changed;
		}
		if (oldRow == null) {
			for (Row nr: newRow) {
				changed.add(nr);
			}
			return changed;
		}
		int i=0;
		int l = newRow.length;
		if (oldRow.length < l) l = oldRow.length;
		for (; i<l; i++) {
			if (oldRow[i].views.size() != newRow[i].views.size()) {
				changed.add(newRow[i]);
			} else {
				if (oldRow[i].belowId != newRow[i].belowId) {
					Log.d("Row.differingRows", String.format("row %d: below id %d -> %d", i, oldRow[i].belowId, newRow[i].belowId));
					changed.add(newRow[i]);
				} else if (oldRow[i].height != newRow[i].height) {
					Log.d("Row.differingRows", String.format("row %d: height id %d -> %d", i, oldRow[i].height, newRow[i].height));
					changed.add(newRow[i]);
				} else {
					for (int j=0; j<oldRow[i].views.size(); j++) {
						if (oldRow[i].views.get(j) != newRow[i].views.get(j)) {
							changed.add(newRow[i]);
							break;
						}
					}
				}
			}
		}
		for (i=l; i<newRow.length; i++) {
			changed.add(newRow[i]);
		}
		return changed;
	}

	private static class Row
	{
		public static final int FIRST_FIT = 0;
		public static final int MAX_AREA = 1;
		public static final int MAX_WIDTH = 2;
		public int belowId = -1;
		public int width = 0;
		public int height = 0;
		
		public ArrayList<View> views = new ArrayList<View>();
		
		public static Row[] build(int n) {
			Row[] rows = new Row[n];
			for (int i=0; i<rows.length; i++) {
				rows[i] = new Row();
			}
			return rows;
		}

	}
	
	private Row[] rows = null;
	private Handler layoutHandler = new Handler() {
		public void handleMessage(Message msg)
		{
			switch (msg.what) {
			case ADJUST_CHANGED_ROWS: {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<Row> changed = (ArrayList<Row>) msg.obj;
					if (changed != null && changed.size() > 0) {
						Log.d("fraggerlayout", String.format("layoutHandler: performing layout changes %d", changed.size()));
						layout(changed);
					}
				} catch (ClassCastException e) {
					
				}
				break;
			}
			}
		}
	};
	private void layoutChildren(boolean forceMeasure, boolean isInLayout) {
		int myWidth = getMeasuredWidth();
		int myHeight = getMeasuredWidth();
		Row [] oldRows = null;
		if (rows != null) {
			oldRows = rows.clone();
		}
		rows= Row.build(getChildCount());
		
		Log.d("fraggerlayout", String.format("1st pass: layout measure %b dims %d %d", forceMeasure, myWidth, myHeight));
		for (int i=0; i<getChildCount(); i++) {
			View v = getChildAt(i);
			SGFragmentInfo fi = info4View(v);
			if (fi == null) {
				Log.d("fraggerlayout", String.format("fragment info not found"));
			} else if (!fi.fragment.isGreedy()){ // give the not so greedy ones what they want first
				if (forceMeasure) v.measure(MeasureSpec.makeMeasureSpec(myWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(myHeight, MeasureSpec.AT_MOST));
				Row r = addView(rows, v, myWidth, myHeight, Row.FIRST_FIT, false);
			} 
		}
		Log.d("fraggerlayout", String.format("2nd pass"));
		for (int i=0; i<getChildCount(); i++) {
			View v = getChildAt(i);
			SGFragmentInfo fi = info4View(v);
			if (fi == null) {
				Log.d("fraggerlayout", String.format("fragment info not found"));
			} else if (fi.fragment.isGreedy()){
				if (forceMeasure) v.measure(MeasureSpec.makeMeasureSpec(myWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(myHeight, MeasureSpec.AT_MOST));
				Row r = addView(rows, v, myWidth, myHeight, fi.fragment.preferMaxArea()?Row.MAX_AREA:fi.fragment.preferMaxWidth()?Row.MAX_WIDTH:Row.FIRST_FIT, true);
			} 
		}
		ArrayList<Row> changed = differingRows(oldRows, rows);
		if (changed.size() > 0) {
			if (isInLayout) {
				Message msg = Message.obtain(layoutHandler, ADJUST_CHANGED_ROWS, changed);
				msg.sendToTarget();
			} else {
				Log.d("fraggerlayout", String.format("layoutChildren: implementing layout changes %d", changed.size()));
				layout(changed);
			}
		} else {
			Log.d("fraggerlayout", String.format("layoutChildren: unchanged layout", changed.size()));
		}
	}
}
