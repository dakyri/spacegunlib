package com.mayaswell.spacegun.fragment;

import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.CControl;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.spacegunlib.R;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.Filter;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.audio.PadMode;
import com.mayaswell.fragment.MWFragment;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.StringControl;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.MenuControl.MenuControlListener;
import com.mayaswell.widget.StringControl.StringControlListener;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.TeaPot.TeaPotListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainControlsFragment extends MWFragment {

	protected TeaPot gainControl = null;
	protected TeaPot panControl = null;
	protected StringControl padLabelControl = null;
	protected MenuControl padModeControl = null;
	protected MenuControl filterSelect = null;
	protected TeaPot filterCutoffControl = null;
	protected TeaPot filterResonControl = null;
	protected TeaPot filterEnvmodControl = null;
	protected InfinIntControl loopCountControl = null;
	protected MenuControl syncMasterSelect = null;
	protected TeaPot tuneControl = null;
	protected MenuControl sampleDirectionSelect = null;
	private int layoutResId;
	protected MWBPPadActivity<?,?,?> mwbp = null;
		
	public MainControlsFragment() {
		layoutResId = R.layout.main_controls_layout;
	}

	public void setLayoutResource(int res) {
		layoutResId = res;
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		mwbp = (MWBPPadActivity<?, ?, ?>) activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(layoutResId, container, true);
		
		padModeControl = (MenuControl) v.findViewById(R.id.padModeControl);
		padLabelControl = (StringControl) v.findViewById(R.id.padLabelControl);
		gainControl =  (TeaPot) v.findViewById(R.id.gainControl);
		panControl =  (TeaPot) v.findViewById(R.id.panControl);
		filterSelect = (MenuControl) v.findViewById(R.id.filterSelect);
		filterEnvmodControl = (TeaPot) v.findViewById(R.id.filterEnvmodControl);
		filterCutoffControl = (TeaPot) v.findViewById(R.id.filterCutoffControl);
		filterResonControl = (TeaPot) v.findViewById(R.id.filterResonControl);
		loopCountControl = (InfinIntControl) v.findViewById(R.id.loopCountControl);
		syncMasterSelect = (MenuControl) v.findViewById(R.id.syncMasterSelect);
		tuneControl  = (TeaPot) v.findViewById(R.id.tuneControl);
		sampleDirectionSelect = (MenuControl) v.findViewById(R.id.sampleDirectionSelect);

		setListeners();
		
		return v;
	}

	protected void setListeners() {
		if (padLabelControl != null) {
			padLabelControl.setValueListener(new StringControlListener() {
				@Override
				public void onValueChanged(String v) {
					PadSample ps;
					if ((ps = mwbp.getSelectedPad()) != null) {
						ps.state.name = v;
						if (ps.padButton != null) ps.padButton.invalidate();
					}
				}
			});
		}

		if (padModeControl != null) {
			padModeControl.setAdapter(mwbp.getPadModeAdapter());
			padModeControl.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					PadSample ps;
					if ((ps = mwbp.getSelectedPad()) != null) {
						MenuData<PadMode> d = mwbp.getPadModeAdapter().getItem(position);
						ps.state.padMode = d.getMode();
					}
				}
			});
		}

		if (gainControl != null) gainControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				PadSample ps;
				if ((ps=mwbp.getSelectedPad()) != null) {
					ps.setGain(v);
					mwbp.ccontrolUpdate(ps, CControl.GAIN, v, Controllable.Controller.INTERFACE);
				}
			}

		});

		if (panControl != null) panControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				PadSample ps;
				if ((ps=mwbp.getSelectedPad()) != null) {
					ps.setPan(v);
					mwbp.ccontrolUpdate(ps, CControl.PAN, v, Controllable.Controller.INTERFACE);
				}
			}

		});

		if (loopCountControl != null) loopCountControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				PadSample ps;
				if ((ps=mwbp.getSelectedPad()) != null) {
					ps.state.loopCount = (int)v;
				}
			}
		});

		if (sampleDirectionSelect != null) {
			sampleDirectionSelect.setAdapter(PadSample.directionAdapter);
			sampleDirectionSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					Log.d("main", "set sample direction " + position);
					PadSample ps;
					if ((ps = mwbp.getSelectedPad()) != null) {
						Direction d = PadSample.directionAdapter.getItem(position);
						ps.setDirection(d);
						Log.d("main", "set sample direction " + d.toString());
					}
				}
			});
		}


		if (tuneControl != null) {
			tuneControl.setValueBounds(-Controllable.TUNE_RANGE_SEMITONES, Controllable.TUNE_RANGE_SEMITONES);
			tuneControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					PadSample ps;
					if ((ps = mwbp.getSelectedPad()) != null) {
						ps.setTune(v);
						mwbp.ccontrolUpdate(ps, CControl.TUNE, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		if (syncMasterSelect != null) {
			syncMasterSelect.setAdapter(null);
			syncMasterSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					PadSample ps;
					if ((ps=mwbp.getSelectedPad()) != null) {
						Object o=syncMasterSelect.getAdapter().getItem(position);
						Log.d("setup", String.format("%s adapter item %d", o.toString(), position));
						try {
							PadSample.Info s = (PadSample.Info) syncMasterSelect.getAdapter().getItem(position);
							ps.setSyncMaster(s.padSample);
						} catch (ClassCastException e) {
							syncMasterSelect.setSelection(0);
							ps.setSyncMaster(null);
						}
					}
				}
			});
		}

		if (filterSelect != null) {
			filterSelect.setAdapter(PadSample.filtersAdapter);
			filterSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					PadSample ps;
					if ((ps=mwbp.getSelectedPad()) != null) {
						Filter d = PadSample.filtersAdapter.getItem(position);
						ps.setFilterType(d);
					}
				}
			});
		}

		if (filterCutoffControl != null) {
			filterCutoffControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					PadSample ps;
					if ((ps = mwbp.getSelectedPad()) != null) {
						ps.setFilterFrequency(v);
						mwbp.ccontrolUpdate(ps, CControl.FLT_CUTOFF, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		if (filterEnvmodControl != null) {
			filterEnvmodControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					PadSample ps;
					if ((ps=mwbp.getSelectedPad()) != null) {
						ps.setFilterEnvMod(v);
						mwbp.ccontrolUpdate(ps, CControl.FLT_ENVMOD, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		if (filterResonControl != null) {
			filterResonControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					PadSample ps;
					if ((ps=mwbp.getSelectedPad()) != null) {
						ps.setFilterResonance(v);
						mwbp.ccontrolUpdate(ps, CControl.FLT_RESONANCE, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}
	}

	@Override
	 public void onInflate (Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);
	 }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		setToPad(mwbp.getSelectedPad());
	}

	@SuppressLint("NewApi")
	public void setToPad(PadSample pbs) {
		if (pbs == null || getView() == null) return;
		PadSampleState s = pbs.getState();
		if (loopCountControl != null) loopCountControl.setValue(s.loopCount);
		if (padLabelControl != null) padLabelControl.setValue(s.name);


		if (padModeControl != null && mwbp.getPadModeAdapter() != null) {
			for (int i=0; i<mwbp.getPadModeAdapter().getCount(); i++) {
				MenuData<PadMode> d = mwbp.getPadModeAdapter().getItem(i);
				if (d.getMode() == s.padMode) {
					padModeControl.setSelection(i);
					break;
				}
			}
		}
		if (gainControl != null) gainControl.setValue(s.gain);
		if (panControl != null) panControl.setValue(s.pan);
		if (tuneControl != null) tuneControl.setValue(s.tune);
		if (filterEnvmodControl != null) filterEnvmodControl.setValue(s.filterEnvMod);
		if (filterCutoffControl != null) filterCutoffControl.setValue(s.filterFrequency);
		if (filterResonControl != null) filterResonControl.setValue(s.filterResonance);
		if (filterSelect != null) {
			for (int i = 0; i < PadSample.filtersAdapter.getCount(); i++) {
				Filter d = PadSample.filtersAdapter.getItem(i);
				if (d.getId() == s.filterType) {
					filterSelect.setSelection(i);
					break;
				}
			}
		}
		if (syncMasterSelect != null && pbs.syncMasterAdapter != null) {
			boolean found = false;
			syncMasterSelect.setAdapter(pbs.syncMasterAdapter);
			for (int i=0; i<pbs.syncMasterAdapter.getCount(); i++) {
				PadSample.Info d = pbs.syncMasterAdapter.getItem(i);
				if (d != null && d.padSample != null && d.padSample.id == s.syncMasterId) {
					syncMasterSelect.setSelection(i);
					found = true;
					break;
				}
			}
			if (!found) {
				syncMasterSelect.setSelection(0);
			}
		}
		if (sampleDirectionSelect != null) {
			if (PadSample.directionAdapter != null) {
				for (int i = 0; i < PadSample.directionAdapter.getCount(); i++) {
					Direction d = PadSample.directionAdapter.getItem(i);
					if (d == s.loopDirection) {
						sampleDirectionSelect.setSelection(i);
						break;
					}
				}
			}
		}
	}

	public void setGain(float v) {
		if (gainControl != null) gainControl.setValue(v);
	}

	public void setPan(float v) {
		if (panControl != null) panControl.setValue(v);
	}

	public void setTune(float v) {
		if (tuneControl != null) tuneControl.setValue(v);
	}

	public void setCutoff(float v) {
		if (filterCutoffControl != null) filterCutoffControl.setValue(v);
	}

	public void setEnvMod(float v) {
		if (filterEnvmodControl != null) filterEnvmodControl.setValue(v);
	}

	public void setResonance(float v) {
		if (filterResonControl != null) filterResonControl.setValue(v);
	}

}
