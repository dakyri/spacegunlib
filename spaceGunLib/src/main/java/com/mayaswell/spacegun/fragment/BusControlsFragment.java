package com.mayaswell.spacegun.fragment;

import java.util.ArrayList;

import com.mayaswell.spacegun.CControl;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXState;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.spacegun.Send;
import com.mayaswell.spacegun.Bus;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.spacegunlib.R;
import com.mayaswell.spacegun.widget.BusSendController;
import com.mayaswell.widget.FXControl;
import com.mayaswell.spacegun.widget.BusSendController.BusSendListener;
import com.mayaswell.widget.FXControl.FXControlListener;
import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.DisplayMode;
import com.mayaswell.widget.LFOControl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

public class BusControlsFragment extends SubPanelFragment {

	private BusSendController busSendController = null;
	
	private ArrayList<FXControl> fxControl = null;
	private ArrayList<LFOControl> buslfoControl = null;
	private RelativeLayout subPanelHolder = null;
	
	protected MWBPPadActivity<?,?,?> mwbp = null;
	
	public BusControlsFragment() {
		fxControl = new ArrayList<FXControl>();
		buslfoControl = new ArrayList<LFOControl>();
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		mwbp = (MWBPPadActivity<?, ?, ?>) activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.bus_controls_layout, container, true);
		
		busSendController  = (BusSendController) v.findViewById(R.id.busSendController);
		subPanelHolder = (RelativeLayout) v.findViewById(R.id.subPanelHolder);
		
		busSendController.setListener(new BusSendListener() {

			@Override
			public void onBusSelected(Bus b) {
				if (b != null && b.state != null) {
					PadSample ps;
					Log.d("select", String.format("bus %s", b.state.name));
					setFXEditor(b.state.fx);
					if ((ps=mwbp.getSelectedPad()) != null) {
						setBusSend(ps, b);
					}
				} else {
					Log.d("select", String.format("null bus selected"));
				}
			}

			@Override
			public void onGainChanged(Bus b, float amt) {
				PadSample ps;
				if ((ps=mwbp.getSelectedPad()) != null) {
					ps.setSendGain(b, amt);
					mwbp.ccontrolUpdate(ps, CControl.busSendId(b.controllableId()), amt, Controllable.Controller.INTERFACE);
				}
			}

			@Override
			public void onEnableChanged(Bus b, boolean m) {
				PadSample ps;
				if ((ps=mwbp.getSelectedPad()) != null) {
					ps.setSendMute(b, !m);
				}
			}
			
		});
		busSendController.setValueBounds(0, 2);
		busSendController.setCenterValue(1);

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if (mwbp.getBusMixer() != null) {
			setBusList(mwbp.getBusMixer().getBus());
		}
		FXControl fc = addFXControl(fxControl.size(), mwbp.getFXAdapter(null), fxControl);
		setToPad(mwbp.getSelectedPad());
		busSendController.setToBus(0);
		layoutEditorControls();
	}
	
	@Override
	public void layoutEditorControls() {
		layoutEditorControls(subPanelHolder, 1, busSendController.getId());
	}
	
	private void setFXListener(FXControl p, final int xyci)
	{
		p.setListener(new FXControlListener() {
			@Override
			public void onEnableChanged(FXControl fpc, int fxId, boolean isChecked)
			{
				Bus fxBus = busSendController.getSelectedBus();
				if (fxBus != null) {
					fxBus.setFXenable(fxId, isChecked);
				}
			}

			@Override
			public void onParamChanged(FXControl fpc, int fxId, int paramId, float amt)
			{
				Bus fxBus = busSendController.getSelectedBus();
				if (fxBus != null) {
					fxBus.setFXparam(fxId, paramId, amt);
					Log.d("BusControl", "param " + fxId + ", " + paramId + ", " + amt);
					mwbp.ccontrolUpdate(fxBus, CControl.fxParamId(fxId, paramId), amt, Controllable.Controller.INTERFACE);
				}
			}

			@Override
			public void onFXChanged(FXControl fpc, int fxId, FX fx)
			{
				Bus fxBus = busSendController.getSelectedBus();
				if (fxBus != null) {
					fxBus.setFX(fxId,  fx);
					fpc.setDisplay(DisplayMode.SHOW_PARAM_CTLS);
					boolean hasFree=false;
					if (fxControl.size() == 0) {
						hasFree = false;
					} else {
						FX f2 = fxControl.get(fxControl.size()-1).selectedFX();
						Log.d("onFXChanged", String.format("last has fx %s", f2.toString()));
						if (f2.getId() == FX.UNITY) {
							hasFree = true;
						}
					}
					Log.d("onFXChanged", String.format("fx is %s has free slot %b", fx.toString(), hasFree));

					if (!hasFree && fxControl.size() <CControl.maxBusFX) {
						FXControl fc = addFXControl(fxControl.size(), mwbp.getFXAdapter(null), fxControl);
						layoutEditorControls();
					}
				}
			}

		});
	}
	
	public FXControl addFXControl(int fxInd, ArrayAdapter<FX> erma, ArrayList<FXControl> list)
	{
		int mid = 1;
		BaseControl sxy0 = null;
//		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		
		if (subPanelHolder.getChildCount() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			sxy0 = (BaseControl) subPanelHolder.getChildAt((subPanelHolder.getChildCount()-1));
			mid = sxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, sxy0.getId());
		}
		FXControl snsx = new FXControl(mwbp);
		snsx.setName("FX"+Integer.toString(fxInd+1));
		snsx.setAdapter(erma);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		snsx.setFxId(fxInd);
		int pxpd = mwbp.getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setId(mid);
		snsx.setDisplay(DisplayMode.SHOW_PARAM_CTLS);
		
		list.add(snsx);
		subPanelHolder.addView(snsx, relativeParams);
		
		setFXListener(snsx, fxInd);
		
		return snsx;
	}
	
	public void setFXEditor(ArrayList<FXState> fx) 
	{
		Log.d("setFXEditor", String.format("%d fx", fx.size()));
		int pxi = 0;
		for (FXState xsp: fx) {
			if (pxi >= CControl.maxBusFX) {
				break;
			}
			Log.d("setFXEditor", String.format("pxi %d, %d fxcontrol %s", pxi, fxControl.size(), xsp.getFx() != null?xsp.getFx().toString():""));
			if (pxi >= fxControl.size()) {
				Log.d("setFXEditor", String.format("add fx control here"));
				addFXControl(fxControl.size(), mwbp.getFXAdapter(null), fxControl);
				layoutEditorControls();
			}
			FXControl lc = fxControl.get(pxi);
			lc.setFXState(xsp);
			pxi++;
		}
		for (int i=pxi; i<fxControl.size(); i++) {
			FXControl lc = fxControl.get(pxi);
			lc.setFXState(null);
		}
	}
	
	@SuppressWarnings("unused")
	private void delFXControl(FXControl p)
	{
		int pxi = fxControl.indexOf(p);
		fxControl.remove(pxi);
//		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		subPanelHolder.removeView(p);
	}

	private FXControl getFXControl(int env) {
		return env >= 0 && env < fxControl.size()? fxControl.get(env): null;
	}

	public void setBusList(ArrayList<Bus> bus) {
		if (busSendController != null) {
			busSendController.setBusList(bus);	
		}
	}

	@SuppressLint("NewApi")
	public void setToPad(Send.Host sps) {
		if (sps == null || getView() == null) return;
		Bus bs = busSendController.getSelectedBus();
		setBusSend(sps, bs);
	}

	private void setBusSend(Send.Host sps, Bus bs) {
		if (busSendController != null && busSendController.showingBus(bs)) {
			Send si = sps.send4Bus(bs);
			if (si != null) {
				busSendController.setSendGain(si.getGain());
				busSendController.setsendEnable(!si.getMute());
			} else if (busSendController.showingBus(0)){
				busSendController.setSendGain(1);
				busSendController.setsendEnable(true);
			} else {
				busSendController.setSendGain(0);
				busSendController.setsendEnable(false);
			}
		}
	}

	public void setTargetGain(int lfo, int i, float v) {
		// TODO Auto-generated method stub
		
	}

	public void setPhase(int lfo, float v) {
		// TODO Auto-generated method stub
		
	}

	public void setRate(int lfo, float v) {
		// TODO Auto-generated method stub
		
	}

	public void setFXParam(int fx, int i, float v) {
		FXControl l = getFXControl(fx);
		if (l != null) l.setFXParamValue(i, v);
	}

	public void setSendGain(int bus, float v) {
		if (busSendController != null && busSendController.showingBus(bus)) {
			busSendController.setSendGain(v);
		}
	}

	public int getSelectedId() {
		Bus b =  busSendController.getSelectedBus();
		if (b == null)  {
			return -1;
		}
		return b.getId();
	}

	public void refreshDisplayedBusState() {
		if (busSendController != null) {
			busSendController.refreshDisplayedBusState();
		}
	}
	

}
