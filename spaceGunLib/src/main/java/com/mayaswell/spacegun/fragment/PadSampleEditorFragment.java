package com.mayaswell.spacegun.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mayaswell.fragment.MWFragment;
import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.spacegunlib.R;
import com.mayaswell.spacegun.widget.SGSampleView;
import com.mayaswell.spacegun.widget.SampleFrameControl;
import com.mayaswell.util.EventID;
import com.mayaswell.widget.FileChooserView;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.SampleView.SampleViewListener;

public class PadSampleEditorFragment extends MWFragment {

	protected SGSampleView sampleView = null;
	protected SampleFrameControl loopStartControl = null;
	protected SampleFrameControl loopLengthControl = null;
	protected FileChooserView fileChooser = null;

	public PadSampleEditorFragment() {
		super();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setToPad(getPadActivity().getSelectedPad());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sample_editor_layout, container, true);
		sampleView  =  (SGSampleView) v.findViewById(R.id.sampleView);
		loopStartControl =  (SampleFrameControl) v.findViewById(R.id.loopStartControl);
		loopLengthControl =  (SampleFrameControl) v.findViewById(R.id.loopLengthControl);
		fileChooser = (FileChooserView) v.findViewById(R.id.sampleSelector);
		fileChooser.setListener(new FileChooserView.Listener() {

			@Override
			public void chooseFile(FileChooserView self) {
				getPadActivity().launchSampleFinder(".*\\.wav", EventID.CHOOSE_FINDER);

			}

			@Override
			public void unchooseFile(FileChooserView self) {
				PadSample s = getPadActivity().getSelectedPad();
				if (s != null) {
					s.setSamplePath(null, true);
					setToPad(s);
					getPadActivity().stopSamplePlayer(s);
				}

			}
		});
		
		registerForContextMenu(loopStartControl);
		registerForContextMenu(loopLengthControl);
	
		sampleView.setListener(new SampleViewListener() {
			@Override
			public void onRegionStartChanged(long v)
			{
				if (getPadActivity().getSelectedPad() != null) {
					getPadActivity().getSelectedPad().setLoopStart(v);
				}
				loopStartControl.setValue(v);
			}
	
			@Override
			public void onRegionLengthChanged(long v)
			{
				if (getPadActivity().getSelectedPad() != null) {
					getPadActivity().getSelectedPad().setLoopLength(v);
				}
				loopLengthControl.setValue(v);
			}
	
			@Override
			public void onMarkerChanged(int type, Object m, int ind, long fr,
					float v) {
				// TODO Auto-generated method stub
				
			}
	
			@Override
			public void onMarkerSelected(int type, Object m, int ind) {
				// TODO Auto-generated method stub
				
			}
	
			@Override
			public void onMarkerRegionSelected(int type, Object m, int ind) {
				// TODO Auto-generated method stub
				
			}
		});
		loopStartControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (getPadActivity().getSelectedPad() != null) {
					getPadActivity().getSelectedPad().setLoopStart(v);
				}
				sampleView.setRegionStart(v);
			}
		});
		
		loopLengthControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (getPadActivity().getSelectedPad() != null) {
					getPadActivity().getSelectedPad().setLoopLength(v);
				}
				sampleView.setRegionLength(v);
			}
		});
		
		return v;
	}

	protected MWBPPadActivity getPadActivity() {
		MWBPPadActivity a = (MWBPPadActivity)getActivity();
		return a;
	}

	public void setToPad(PadSample pbs) {
		if (pbs == null || getView() == null) return;
		PadSampleState s = pbs.getState();
		Log.d("sample", "loop state");
		if (s != null) {
			Log.d("sample", "loop state "+s.loopLength);
			fileChooser.setPath(s.path);
			loopLengthControl.setValue(s.loopLength);
			loopStartControl.setValue(s.loopStart);
			loopLengthControl.setCenterValue(pbs.nTotalFrames);
			loopStartControl.setCenterValue(pbs.nTotalFrames);
		}
		sampleView.setToPlayer(pbs);
	}

	public void setBeatMode(boolean b) {
		loopStartControl.setBeatMode(b);
		loopLengthControl.setBeatMode(b);
	}

	public void setSampleGfx4Pad(PadSample pbs) {
		fileChooser.setPath(pbs.state.path);
		sampleView.setToPlayer(pbs);
	}

	public void setLoopStart(long l) {
		if (loopStartControl != null) loopStartControl.setValue(l);
		if (sampleView != null) sampleView.setRegionStart(l);
	}

	public void setLoopLength(long l) {
		if (loopStartControl != null) loopLengthControl.setValue(l);
		if (sampleView != null) sampleView.setRegionLength(l);
	}

	public void updateSampleCursor() {
		if (sampleView != null) sampleView.updateCursor();
	}

}