package com.mayaswell.spacegun;

import java.util.ArrayList;

import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.LFO;

public class AudioNodeState {
	public String name = "Empty";
	public float gain = 1;
	public float pan = 0;
	public float tune = 0;
	
	public ArrayList<LFO> lfo = new ArrayList<LFO>();
	public ArrayList<Envelope> envelope = new ArrayList<Envelope>();

	public AudioNodeState() {
		super();
	}

	public void addEnvelope(Envelope env) {
		if (envelope == null) envelope = new ArrayList<Envelope>();
		envelope.add(env);
	}

	public void addLFO(LFO l) {
		if (lfo == null) lfo = new ArrayList<LFO>();
		lfo.add(l);
	}

}